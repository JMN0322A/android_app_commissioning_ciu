////////
//////////////////////////////////[ ssid_procs.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"
#include <string.h>




//////// - MAC-ADDRESS  length = 6-bytes (48-bits)
uint8_t *get_mac_6B_48b()
{
	return &(m_mac_addr[0]);
}




void generate_ssid_R128bits(uint8_t *ptr)
{
	brascii64b_encode(ptr, 16, m_APmode_SSID + 4);
	m_APmode_SSID[26] = ('\0');
	printf("******ssid==%s ********\r\n", m_APmode_SSID);
}







////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////// K-Key  length = 26-bytes  (208-bits)                                   ////////
//////// Here, it is a 32-byte sequence of random-ish bytes                     ////////
//////// ***** THIS ARRAY IS SHARED WITH THE MOBILE APP ********                ////////
//////// *****                                                                  ////////
//////// ***** DO * NOT * EDIT * THIS ********                                  ////////
//////// ***** DO * NOT * EDIT * THIS ********                                  ////////
//////// ***** DO * NOT * EDIT * THIS ********                                  ////////
////////                                                                        ////////
uint8_t *get_kkey_26B_208b()                                                    ////////
{                                                                               ////////
	static uint8_t x[] =  { 0x1f, 0x68, 0x48, 0xcc,   0x59, 0xcf, 0x8a, 0x9c,   ////////
							0x4d, 0xe7, 0xc4, 0xf2,   0x0d, 0x5a, 0xa0, 0x93,	////////
							0x3d, 0xd0, 0x56, 0x8f,   0x6c, 0x06, 0xd3, 0x37,	////////
							0x0d, 0x6b, 0x50, 0x91,   0x35, 0x9b, 0xbd, 0xfd };	////////
																				////////
	return &(x[0]);	                                                            ////////
}                                                                               ////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////







//////// - Combined to produce DECODE-ABLE ... length = 16-bytes (128-bits)
uint8_t *combineXORY_16B_128b( uint8_t* rand_128b, uint8_t* kkey_208b, uint8_t* mac_48b )
{
	#define SIZEOF_RNDX (16)
	#define SIZEOF_MACX (6)
	static uint8_t x[SIZEOF_RNDX];

	for (int i=(0); i<SIZEOF_MACX; i++)
	{
		x[i] = (rand_128b[i] ^ mac_48b[i]);
	}
	for (int i=(SIZEOF_MACX), j=(0);  i<SIZEOF_RNDX;  ++i,++j)
	{
		x[i] = (rand_128b[i] ^ kkey_208b[j]);
	}
	printf("Combined--");
	for (int i=0; i<16; i++) {
		printf("-%02x", x[i]);
	}
	printf("\r\n");
	return x;

	#undef SIZEOF_MACX
	#undef SIZEOF_RNDX
}








//////// - produces a 16-byte random number, which determines the wifi PASSWORD
uint8_t *create_random_16B_128b()
{
	#define SIZEOF_RNDX (16)
	static uint8_t x[SIZEOF_RNDX];

	for (int i=0; i<SIZEOF_RNDX; ++i) {
		x[i] = (uint8_t)(rand() & 0x0ff);
	}

	printf("Rand128 ==> ");
	for (int i=0; i<SIZEOF_RNDX; i+=4) {
		for (int j=0; j<4; j++) {
			printf((0==j)?("%02x"):("-%02x"), x[i+j]);
		}
		if (i < (SIZEOF_RNDX - 4)) printf("~~~");
	}
	printf("\r\n");

	return x;
	
	#undef SIZEOF_RNDX
}






char *get_password(uint8_t *pRand16bytes)
{
	#define SIZEOF_PASSW (28)
	static char x[SIZEOF_PASSW];

	brascii64b_encode(pRand16bytes, 16,  x);
	x[22] = ('\0');
	printf("Wifi-Password==> %s\r\n", (x));

	return x;

	#undef SIZEOF_PASSW
}



char *get_ssid(uint8_t *pRndKkeyMac)
{
	#define SSID_PREFIX_LENGTH (4)
	#define SSID_PREFIX        ("SAT_")
	#define SIZEOF_SSID        (28)
	static char x[SIZEOF_SSID];

	strcpy(x, SSID_PREFIX);
	brascii64b_encode(pRndKkeyMac, 16,  (x + SSID_PREFIX_LENGTH));
	x[26] = ('\0');
	printf("Wifi-SSID==> %s\r\n", (x));

	return x;

	#undef SIZEOF_SSID
	#undef SSID_PREFIX
	#undef SSID_PREFIX_LENGTH
}








void get_wifi_settings()
{
	uint8_t *ptr_rnd;
	uint8_t *ptr_kkey;
	uint8_t *ptr_mac;
	uint8_t *ptr_comb;
	char    *ptr_pswd;
	char    *ptr_ssid;

	for(;;) {
		ptr_rnd  = create_random_16B_128b();
		ptr_kkey = get_kkey_26B_208b();
		ptr_mac  = get_mac_6B_48b();

		ptr_pswd = get_password(ptr_rnd);

		ptr_comb  = combineXORY_16B_128b(ptr_rnd, ptr_kkey, ptr_mac);
		ptr_ssid  = get_ssid(ptr_comb);

		//////// ensure the first random-character is a capitol-Letter
		if ((('A') <= ptr_ssid[4]) && (ptr_ssid[4] <= ('Z'))) {
			break;
		}
		printf("--------again\r\n");
	}

	strcpy(m_APmode_SSID, ptr_ssid);
	strcpy(m_APmode_PSWD, ptr_pswd);
}






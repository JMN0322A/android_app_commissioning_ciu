////////
//////////////////////////////////[ main.h ]/////////////////////////////
////////


#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#ifdef __cplusplus
extern "C" {
	#endif


	#include "asf.h"
	#include "socket/include/socket.h"




	extern  void           brascii64b_encode( unsigned char *input_ptr, int input_len, char *pOutput );
	extern  int            brascii64b_decode( char *input_ptr, int input_len, unsigned char *pOutput );
	extern  char           brascii64b_bin_to_char( unsigned char x );
	extern  unsigned char  brascii64b_char_to_bin( char x );


	extern	void      uartConsole_init();
	extern  void      generate_ssid();
	extern  uint8_t  *get_kkey_26B_208b();
	extern  uint8_t  *get_mac_6B_48b();
	extern  uint8_t  *combineXORY_16B_128b( uint8_t* rand_128b, uint8_t* kkey_208b, uint8_t* mac_48b );
	extern  uint8_t  *create_random_16B_128b();
	extern  char     *get_password(uint8_t *pRand16bytes);
	extern  char     *get_ssid(uint8_t *pRndKkeyMac);
	extern  void      get_wifi_settings();

	extern  uint8_t   buttonSW0_isActive();
	extern  uint32_t  buttonSW0_getRandomSeed();
	extern  void      buttonSW0_loopSlice();

	extern  void      wifiProcs_init();
	extern  void      wifiProcs_connectRouter();
	extern  void      wifiProcs_disconnectRouter();
	extern  void      wifiProcs_AP_mode();
	extern  void      wifiProcs_STA_mode();

	extern	void      commissionProcs_init();
	extern	void      commissionProcs_loopSlice();
	extern	void      commissionProcs_exitCommnMode();

	extern	void      tcpClient_init();
	extern	void      tcpClient_loopSlice();

	extern  void      tcpServer_setup();
	extern  void      tcpServer_loopSlice();
	extern	uint8_t   tcpServer_checkActivity();

	extern  void      proc_recv(SOCKET sock, tstrSocketRecvMsg *pstrRecv);

	extern	void      socket_server_callback(SOCKET sock,  uint8_t u8Msg,  void *pvMsg);




	#define  m_random_128bits_LEN  (16)

	#define  m_rxBuffer_SIZE       (1460)




	extern	uint32_t  m_Count_main_loop;

	extern  uint8_t   buttonSW0_isTriggered;

	extern  uint32_t  m_random_128bits[ ];      //// m_random_128bits_LEN;

	extern  uint8_t   m_mac_addr[ ];            //// M2M_MAC_ADDRES_LEN;
	extern  char      m_mac_addr_str[ ];		//// 24

	extern  char      m_APmode_SSID[ ];         //// 32
	extern  char      m_APmode_PSWD[ ];			//// 32

	extern  uint8_t   m_rxBuffer[ ];			//// 1460

	extern  uint8_t   m_state_wifi_router_enabled;
	extern  uint8_t   m_state_wifi_router_connected;
	extern	uint8_t   m_state_wifi_app_connected;
	extern  uint8_t   m_state_announced;
	extern  uint8_t   m_state_AP_mode;
	extern	uint8_t   m_triggering_end_tcpServer;
	extern	bool      m_state_wifi_connected;
	extern	uint8_t   m_APP_is_concluded;
	extern	uint8_t   m_flag_reconnect_to_APP;


	////////////////////////////////////////// the IP-# & Port-# of CIU, when connecting to APP
	#define TCPIP_SERVER_PORT       (9090)
	//
	#define TCPIP_SERVER_IP_0       (172)
	#define TCPIP_SERVER_IP_1       (29)
	#define TCPIP_SERVER_IP_2       (188)
	#define TCPIP_SERVER_IP_3       (1)


	#define   SIZEOF_AP_STR			(34)

	extern  char      m_configuration_Router_ssid[ ];
	extern  char      m_configuration_Router_pswd[ ];
	extern  uint32_t  m_configuration_tm_connect_Wifi;
	extern  uint32_t  m_configuration_tm_connect_Cloud;





	#ifdef __cplusplus
}
#endif

#endif /* MAIN_H_INCLUDED */

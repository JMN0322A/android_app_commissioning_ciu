////////
//////////////////////////////////[ commission_procs.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>


static uint8_t  m_commissioning_mode = (0);
static uint8_t  m_state_setup_ssid = (0);
static uint8_t  m_state_disconnecting_router = (0);
static uint8_t  m_zulu = (0);


//////////////////////////////////////////########-----Value provides for about 2-minutes fifteen-seconds
#define          m_timeout_MAX           (30000000)
static uint32_t  m_timeout_no_activity = m_timeout_MAX;





///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

uint8_t  m_state_AP_mode = (0);








void commissionProcs_init()
{
///////////////////////////////////////////////// cant because wifi connect call follows this in main()
////////	m_state_router_enabled = (0);

	m_commissioning_mode = (0);
	m_state_setup_ssid = (0);
    m_state_disconnecting_router = (0);
	m_state_AP_mode = (0);
	m_triggering_end_tcpServer = (0);
	m_APP_is_concluded = (0);
}


void commissionProcs_exitCommnMode()
{
	printf("commissionProcs_exitCommnMode()- Clear mem.\r\n");
	commissionProcs_init();

	printf("commissioningProcs_exitCommnMode()- Disable AP-Mode.\r\n");
	wifiProcs_STA_mode();

	printf("commissioningProcs_exitCommnMode()- Restore wifi-->Router\r\n");
	wifiProcs_connectRouter();

	printf("commissioningProcs_exitCommnMode()- Start NORMAL-MODE\r\n");
}


void commissionProcs_loopSlice()
{
	//////// determine if triggered INTO Commissioning-Mode
	if (! m_commissioning_mode) {
		if ((buttonSW0_isTriggered) && (! buttonSW0_isActive())) {
			//////// Triggered: Commissioning-mode
			m_commissioning_mode = (1);
			printf("commissioning_loop()- ---Start-COMMISSION-MODE----\r\n");
		}
		else {
			return;
			////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////// Otherwise, go no further.
		}
	}



    ////////-- COMMISSIONING-MODE --////////////////////////////////
	////////
    //////// ensure trigger is cleared
	buttonSW0_isTriggered = (0);



    //////// IF ROUTER CONNECTED, WIFI DISCONNECT
	if (m_state_wifi_router_connected) {
		if (! m_state_disconnecting_router) {
			m_state_disconnecting_router = (1);
			wifiProcs_disconnectRouter();
		}
		//////// go no further until router disconnected
		return;
	}
	//////// clear the disconnecting flag
	m_state_disconnecting_router = (0);



	//////// IF CIU is NOT connected to ROUTER, such as, FAILING to connect
	//////// (Note)- the CIU could be in a retry-loop, to connect to a router...
	if (m_state_wifi_router_enabled) {
		printf("commissioning_loop()- m_state_wifi_router_enabled == TRUE\r\n");
		m_state_wifi_router_enabled = (0);
	}



    //////// CERATE NEW SSID AND PASSWORD
    if (! m_state_setup_ssid) {
		m_state_setup_ssid = (1);
		//////// calculate the new random ssid
		srand(m_Count_main_loop);
		//
		printf("commissioning_loop()- rand(): ");
		for (int i=0; i<6; i++) {
			printf("%08x ", (rand()));
		}
		printf("\r\n");

	    //////// generate new wifi SSID/PASSW using the random value above
		get_wifi_settings();
		printf("commissioning_loop()- --%s--%s--\r\n", m_APmode_SSID, m_APmode_PSWD);
		return;
	}



	//////// STARTUP THE AP_MODE
	if (! m_state_AP_mode) {
		m_state_AP_mode = (1);

		wifiProcs_AP_mode();
		return;
	}


//	//////// if AP-Mode started, look for button switch and go back to normal.
//	if (m_state_AP_mode) {
//		if (buttonSW0_isActive()) {
//			commissionProcs_exitCommnMode();
//			return;
//		}
//	}

	//////// if AP-Mode has been UNCONNECTED for a LONG TIME, go BACK to NORMAL MODE
	//
	if (tcpServer_checkActivity()) {
		m_timeout_no_activity = m_timeout_MAX;
	}
	if (m_timeout_no_activity != 0) {
		if ((0) == (-- m_timeout_no_activity)) {
			for (uint8_t i=0; i<10; i++) {
				printf("commissioning_loop() - (TIMEOUT---EXIT)\r\n");
			}
			commissionProcs_exitCommnMode();
		}
//		if (0 == (m_timeout_no_activity & 0xfffff)) {
//			printf("...(%ld)\r\n", m_timeout_no_activity);
//		}
	}

}





////////
//////////////////////////////////[ main.c ]/////////////////////////////
////////


#include "asf.h"
#include "pseudo_ciu.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>


#define STRING_HEADER	"\r\n\r\n\r\n"										\
						"-- ===================================--\r\n"		\
						"-- Novosel's  Pseudo-CIU              --\r\n"		\
						"-- Reference Design for Commissioning --\r\n"		\
						"-- "BOARD_NAME "  &  Winc15x0   --\r\n"			\
						"-- Compiled:  "__DATE__ "  "__TIME__ "   --\r\n--\r\n"



///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

uint32_t m_Count_main_loop;


uint8_t  m_rxBuff[ m_rxBuff_SIZE ];
uint8_t  m_txBuff[ m_rxBuff_SIZE ];


uint32_t m_random_128bits[ m_random_128bits_LEN ];

uint8_t  m_mac_addr[ M2M_MAC_ADDRES_LEN ];
char     m_mac_addr_str[24] = "00:00:00:00:00:00";



uint8_t  m_APP_is_concluded  = (0);


//char     m_configuration_Router_ssid[SIZEOF_AP_STR] = ("SOPHI");
//char     m_configuration_Router_pswd[SIZEOF_AP_STR] = ("3342010865");
//char     m_configuration_Router_ssid[SIZEOF_AP_STR] = ("da-robotics");
//char     m_configuration_Router_pswd[SIZEOF_AP_STR] = ("darkhelmetget$jammed");
char     m_configuration_Router_ssid[SIZEOF_AP_STR] = ("Androgen");
char     m_configuration_Router_pswd[SIZEOF_AP_STR] = ("3343321243");
uint32_t m_configuration_tm_connect_Wifi            = (0); ///////////(1562227200);
uint32_t m_configuration_tm_connect_Cloud           = (0); ///////////(1563609600);
char     m_configuration_APP_ssid[SIZEOF_AP_STR]    = ("SAT_Xxxxxxxxxxxxxxxxxxxxx");
char     m_configuration_APP_pswd[SIZEOF_AP_STR]    = ("0000000000");




int main(void)
{
	int8_t  ret;


	//////// Initialize the board
	sysclk_init();
	board_init();

	//////// Initialize the UART console
	uartConsole_init();
	printf(STRING_HEADER);

	//////// Initialize the BSP
	nm_bsp_init();

	//////// Initialize Wifi Connectivity
	wifiProcs_init();

	//////// Initialize socket module
	socketInit();

	//////// CIU STARTS in Connect-to-ROUTER mode FIRST.
	wifiProcs_setConnectionMode(MODE_CONN_ROUTER);


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	//////////////// LOOP-FOREVER
	while(1)
	{
		//////// Dispatch pending events from network controller
		m2m_wifi_handle_events(NULL);


		//////// Global main-loop Counter
		++ m_Count_main_loop;


		//////// Loop-Slices for each module

		buttonSW0_loopSlice();

		modeAPP_loopSlice();

		modeRouter_loopSlice();

		tcpServer_loopSlice();

		tcpClient_loopSlice();
	}
	///////////////////////////////////////////////////////////////////

	return (0);
}






void uartConsole_init(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	//// Configure UART console
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


///////////////////// TODO: want more decorations around SSID and PSWD generation
void print_div()
{
	for (uint8_t i = 4; (0 != i); --i) {
		for (uint8_t j = 32; (0 != j); --j) {
			printf("<>");
		}
		printf("\r\n");
	}
}









/*-
------------------------------------------------------------------------------
[_]- timeout after long time with no APP connection, while in APP mode
[_]- when flag set, PseudoCIU should return to APP-mode, after some time elapsed
[_]- when wifi-Connected, CIU should time-out after 60-seconds of no APP get-status() calls
[_]- no keep-alive implemented in the APP yet
[_]- APP debug screen overlay option
[_]- add check-box to APP, for RETURN to APP after some time
[_]- re-sync GIT to NEW REPOSITORY
[_]- place values into NVM
------------------------------------------------------------------------------
-*/


















////////
//////////////////////////////////[ pseudo_ciu.h ]/////////////////////////////
////////


#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#ifdef __cplusplus
extern "C" {
	#endif


	#include "asf.h"
	#include "socket/include/socket.h"




	extern  void           brascii64b_encode( unsigned char *input_ptr, int input_len, char *pOutput );
	extern  int            brascii64b_decode( char *input_ptr, int input_len, unsigned char *pOutput );
	extern  char           brascii64b_bin_to_char( unsigned char x );
	extern  unsigned char  brascii64b_char_to_bin( char x );


	extern	void      uartConsole_init();
	extern	void      print_div();

	extern  void      generate_ssid();
	extern  uint8_t  *get_kkey_26B_208b();
	extern  uint8_t  *get_mac_6B_48b();
	extern  uint8_t  *combineXORY_16B_128b( uint8_t* rand_128b, uint8_t* kkey_208b, uint8_t* mac_48b );
	extern  uint8_t  *create_random_16B_128b();
	extern  char     *get_password(uint8_t *pRand16bytes);
	extern  char     *get_ssid(uint8_t *pRndKkeyMac);
	extern  void      get_wifi_settings();

	extern  uint8_t   buttonSW0_isActive();
	extern  void      buttonSW0_loopSlice();

	extern	void      tcpClient_init();
	extern	void      tcpClient_loopSlice();

	extern  void      tcpServer_setup();
	extern  void      tcpServer_loopSlice();

	extern  int       seconds_since_1970_Jan_01(uint16_t year, uint8_t month, uint8_t day, uint8_t hh, uint8_t mm, uint8_t ss);

	extern	void      wifiProcs_init();
	extern	void      wifiProcs_setConnectionMode(int m);

	extern	void      modeAPP_loopSlice();

	extern	void      modeRouter_loopSlice();


	#define  m_random_128bits_LEN  (16)

	#define  m_rxBuff_SIZE       (1460)




	enum { ////- wifi_mode -//////////////////////
		MODE_DISCONNECT,
		MODE_CONN_ROUTER,
		MODE_CONN_APP
	};

	enum { ////- wifi_status -////////////////////
		WIFI_DISCONNECTED,
		WIFI_CONNECTING_ROUTER,
		WIFI_CONNECTED_ROUTER,
		WIFI_CONNECTING_APP,
		WIFI_CONNECTED_APP
	};



	extern	uint32_t  m_Count_main_loop;

	extern  uint8_t   buttonSW0_isTriggered;

	extern  uint32_t  m_random_128bits[ ];      //// m_random_128bits_LEN

	extern  uint8_t   m_mac_addr[ ];            //// M2M_MAC_ADDRES_LEN
	extern  char      m_mac_addr_str[ ];		//// 24

	extern	uint8_t   m_ip_number[ ];			//// 4

	extern	uint8_t   m_rxBuff[ ];				//// 1460
	extern	uint8_t   m_txBuff[ ];				//// 1460

	extern	bool      m_state_wifi_connected;
	extern	uint8_t   m_APP_is_concluded;


	////////////////////////////////////////// the IP-# & Port-# of CIU, when connecting to APP
	#define TCPIP_SERVER_PORT       (9090)
	//
	#define TCPIP_SERVER_IP_0       (172)
	#define TCPIP_SERVER_IP_1       (29)
	#define TCPIP_SERVER_IP_2       (188)
	#define TCPIP_SERVER_IP_3       (1)



	#define   SIZEOF_AP_STR			(34)

	extern  char      m_configuration_Router_ssid[ ];		//// 34
	extern  char      m_configuration_Router_pswd[ ];		//// 34
	extern  uint32_t  m_configuration_tm_connect_Wifi;
	extern  uint32_t  m_configuration_tm_connect_Cloud;
	extern  char      m_configuration_APP_ssid[ ];			//// 34
	extern  char      m_configuration_APP_pswd[ ];			//// 34






	#ifdef __cplusplus
}
#endif

#endif /* MAIN_H_INCLUDED */

////////
//////////////////////////////////[ wifi_procs.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>



static	tstrWifiInitParam  m_paramWifi;




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


//////// current IP number
uint8_t  m_ip_number[4] = {0,0,0,0};

//////// Wifi connection state
uint8_t  m_state_wifi_router_enabled = (1);
uint8_t  m_state_wifi_router_connected = (0);


#define SIZEOF_AP_STR   (34)
/*-
static char      m_router_ssid[SIZEOF_AP_STR] = ("da-robotics");
static char      m_router_pswd[SIZEOF_AP_STR] = ("darkhelmetget$jammed");
-*/
static char      m_router_ssid[SIZEOF_AP_STR] = ("SOPHI");
static char      m_router_pswd[SIZEOF_AP_STR] = ("3342010865");
//*--*/






/**
 * \brief Callback to get the Wi-Fi status update.
 *
 * \param[in] u8MsgType type of Wi-Fi notification. Possible types are:
 *  - [M2M_WIFI_RESP_CURRENT_RSSI](@ref M2M_WIFI_RESP_CURRENT_RSSI)
 *  - [M2M_WIFI_RESP_CON_STATE_CHANGED](@ref M2M_WIFI_RESP_CON_STATE_CHANGED)
 *  - [M2M_WIFI_RESP_CONNTION_STATE](@ref M2M_WIFI_RESP_CONNTION_STATE)
 *  - [M2M_WIFI_RESP_SCAN_DONE](@ref M2M_WIFI_RESP_SCAN_DONE)
 *  - [M2M_WIFI_RESP_SCAN_RESULT](@ref M2M_WIFI_RESP_SCAN_RESULT)
 *  - [M2M_WIFI_REQ_WPS](@ref M2M_WIFI_REQ_WPS)
 *  - [M2M_WIFI_RESP_IP_CONFIGURED](@ref M2M_WIFI_RESP_IP_CONFIGURED)
 *  - [M2M_WIFI_RESP_IP_CONFLICT](@ref M2M_WIFI_RESP_IP_CONFLICT)
 *  - [M2M_WIFI_RESP_P2P](@ref M2M_WIFI_RESP_P2P)
 *  - [M2M_WIFI_RESP_AP](@ref M2M_WIFI_RESP_AP)
 *  - [M2M_WIFI_RESP_CLIENT_INFO](@ref M2M_WIFI_RESP_CLIENT_INFO)
 * \param[in] pvMsg A pointer to a buffer containing the notification parameters
 * (if any). It should be casted to the correct data type corresponding to the
 * notification type. Existing types are:
 *  - tstrM2mWifiStateChanged
 *  - tstrM2MWPSInfo
 *  - tstrM2MP2pResp
 *  - tstrM2MAPResp
 *  - tstrM2mScanDone
 *  - tstrM2mWifiscanResult
 */
void callback_wifi_connection(uint8_t u8MsgType, void *pvMsg)
{
	switch (u8MsgType)
	{
	case M2M_WIFI_RESP_CON_STATE_CHANGED:
		{
			tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
			if (M2M_WIFI_CONNECTED == pstrWifiState->u8CurrState) {
				printf("CB_wifi: StateX==> CONNECTED\r\n");
				m2m_wifi_request_dhcp_client();
				////
				//////// Assumed the tcp-server, which was Listening,
				//////// or connected, gracefully exits.
			}
			else if (M2M_WIFI_DISCONNECTED == pstrWifiState->u8CurrState) {
				m_state_wifi_router_connected = (0);
				printf("CB_wifi: StateX==> DISCONNECTED\r\n");
				//////// only retry connection if enabled
				if (m_state_wifi_router_enabled) {
					wifiProcs_connectRouter();
				}
				else if (m_triggering_end_tcpServer) {
					printf("CB_wifi: (disconnected)--TRIGGERING\r\n");
					printf("CB_wifi: (disconnected)--Exit-Commission-Mode\r\n");
					m_triggering_end_tcpServer = (0);
					commissionProcs_exitCommnMode();
					m_state_wifi_router_enabled = (1);
					m_APP_is_concluded = (1); //////// flag ///////////////----
				}
			}
		}
		break;


	case M2M_WIFI_REQ_DHCP_CONF:
		{
			uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
			m_ip_number[0] = pu8IPAddress[0];
			m_ip_number[1] = pu8IPAddress[1];
			m_ip_number[2] = pu8IPAddress[2];
			m_ip_number[3] = pu8IPAddress[3];
			printf("CB_wifi: M2M_WIFI_REQ_DHCP_CONF: IP is %u.%u.%u.%u\r\n",
					m_ip_number[0], m_ip_number[1], m_ip_number[2], m_ip_number[3] );

			if (m_state_wifi_router_enabled) {
				m_state_wifi_router_connected = (1);
			}
			if (m_state_AP_mode) {
				tcpServer_setup();
			}
		}
		break;


	case M2M_WIFI_RESP_GET_SYS_TIME:
		{
			tstrSystemTime *strSysTime_now = (tstrSystemTime *)pvMsg;

			for (int8_t i=0; i<16; i++) {
				if (8==i) {
					printf("-TIME--- Winc-GMT== %u:%02u:%02u\r\n",
					strSysTime_now->u8Hour,           // hour (86400 equals secs per day)
					strSysTime_now->u8Minute,         // minute (3600 equals secs per minute)
					strSysTime_now->u8Second);        // second
				}
				for(int8_t j=0; j<8; j++) {
					printf("-TIME");
				}
				printf("\r\n");
			}
			break;
		}


	default:
		{
			printf("CB_wifi: UNKNOWN (%d)\r\n", u8MsgType);
		}
		break;
	}

}



void wifiProcs_init()
{
	int8_t  ret;


	//////// default - enable wifi RE-Connect
	m_state_wifi_router_enabled = (1);

	//////// Initialize Wifi parameters structure. CLEAR-ZERO
	memset((uint8_t *)(& m_paramWifi), (0), sizeof(tstrWifiInitParam));

	//////// Initialize Wifi driver with data and status callbacks
	m_paramWifi.pfAppWifiCb = callback_wifi_connection;
	ret = m2m_wifi_init(& m_paramWifi);
	if (M2M_SUCCESS != ret) {
		printf("wifiProcs_init(): m2m_wifi_init call error!(%d)\r\n", ret);

		while (1) {
			;////////--infinite-loop
		}
	}

	////////////////////////////// THIS STUCK, WHEN AT THE TOP OF THIS FUNCTION ////////////
	////////////////////////////// THIS STUCK, WHEN AT THE TOP OF THIS FUNCTION ////////////
	////////////////////////////// THIS STUCK, WHEN AT THE TOP OF THIS FUNCTION ////////////
	////////////////////////////// THIS STUCK, WHEN AT THE TOP OF THIS FUNCTION ////////////
	//////// get the MAC address of the Winc
	m2m_wifi_get_mac_address(m_mac_addr);
	sprintf(m_mac_addr_str, "%02X:%02X:%02X:%02X:%02X:%02X",
							m_mac_addr[0], m_mac_addr[1], m_mac_addr[2],
							m_mac_addr[3], m_mac_addr[4], m_mac_addr[5]  );
	printf("WifiProcs_init() wifi-MAC: %s\r\n", m_mac_addr_str );
}


/*-
void wifiProcs_setReconnectEnable(uint8_t  b) {
	m_state_wifi_router_enabled = (b);
	if (! m_state_wifi_router_enabled) {
		printf("Wifi-Router-Connect-DISNABLED (0)\r\n");
	}
	else {
		printf("Wifi-Router-Connect-ENABLED (1)\r\n");

		if (! m_state_wifi_router_connected) {
			wifiProcs_connectRouter();
		}
	}
}
-*/


void wifiProcs_connectRouter()
{
	printf("wifiProcs_connectRouter(): AP(%s)...\r\n", m_router_ssid);

    m_state_wifi_router_enabled = (1);

	m2m_wifi_connect(	m_router_ssid,
						strlen(m_router_ssid),
						M2M_WIFI_SEC_WPA_PSK,
						m_router_pswd,
						M2M_WIFI_CH_ALL			);
	
}


void wifiProcs_disconnectRouter()
{
	printf("wifiProcs_disconnectRouter(): AP(%s)...\r\n", m_router_ssid);
	m_state_wifi_router_enabled = (0);
	m2m_wifi_disconnect();
}








/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#define SIZEOF_AP_STR   (34)
char      m_conf_AP_ssid[SIZEOF_AP_STR] = ("STA_000000000000000000");
char      m_conf_AP_pswd[SIZEOF_AP_STR] = ("0000000000");

uint32_t  m_conf_tm_connect_Wifi        = (0);///////////(1562227200);
uint32_t  m_conf_tm_connect_Cloud       = (1563609600);

#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define MAIN_WLAN_CHANNEL        (6)

static tstrM2MAPConfig  m_strucM2MAPConfig;


void wifiProcs_AP_mode()
{
    uint8_t  ret;

	//////// Initialize AP mode parameters structure with SSID, channel and OPEN security type.
	memset(&m_strucM2MAPConfig, 0x00, sizeof(tstrM2MAPConfig));


	//////// setup the AP mode parameters with SSID/Password/...
	strcpy((char *)&m_strucM2MAPConfig.au8SSID, m_APmode_SSID);
	strcpy((char *)&m_strucM2MAPConfig.au8Key, m_APmode_PSWD);
	m_strucM2MAPConfig.u8KeySz = strlen(m_APmode_PSWD);
	m_strucM2MAPConfig.u8KeyIndx = MAIN_WLAN_WEP_KEY_INDEX;
	m_strucM2MAPConfig.u8SecType = M2M_WIFI_SEC_WPA_PSK;
	m_strucM2MAPConfig.u8ListenChannel = MAIN_WLAN_CHANNEL;

	//////// setup the AP mode parameters with initial IP-# values
	m_strucM2MAPConfig.au8DHCPServerIP[0] = 172; //172; //192;
	m_strucM2MAPConfig.au8DHCPServerIP[1] =  29; //16;  //168;
	m_strucM2MAPConfig.au8DHCPServerIP[2] = 188; //188; //1;
	m_strucM2MAPConfig.au8DHCPServerIP[3] =   1; //1;   //1;


	//////// start AP mode
	printf("wifiProcs_AP_mode()- starting AP mode\r\n");
	ret = m2m_wifi_enable_ap(&m_strucM2MAPConfig);
	if (M2M_SUCCESS != ret) {
		printf("wifiProcs_AP_mode: m2m_wifi_enable_ap call error!\r\n");
		while (1) {
		}
	}

	printf("wifiProcs_AP_mode()- Started. (WPA2) SSID == %s\r\n", m_strucM2MAPConfig.au8SSID);
	printf("wifiProcs_AP_mode()- Started. PASSWORD    == %s\r\n", m_strucM2MAPConfig.au8Key);
}



void wifiProcs_STA_mode()
{
	uint8_t  ret;

	ret = m2m_wifi_disable_ap();
	if (M2M_SUCCESS != ret) {
		printf("wifiProcs_STA_mode: m2m_wifi_disable_ap call error!\r\n");
		while (1) {
		}
	}

	printf("wifiProcs_STA_mode()- Started\r\n");
}








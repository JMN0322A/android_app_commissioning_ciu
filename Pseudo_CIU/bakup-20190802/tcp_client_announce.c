////////
//////////////////////////////////[ tcp_client_announce.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"
#include <string.h>




#define TCP_SERVER_IP           ((172<<24)|(217<<16)|(7<<8)|(180))
#define TCP_SERVER_PORT         (80)

#define STRING_HTTP_GET_MSG_0 (	"GET /histov0?w=1&msg=" )

#define STRING_HTTP_GET_MSG_1 (	" HTTP/1.1\r\n"								\
								"Host: wifi-state-jonovos.appspot.com\r\n"	\
								"User-Agent: Atmel Winc15x0 Custom\r\n"		\
								"Accept: text/html\r\n"						\
								"Referer: none\r\n"							\
								"Accept-Language: en-US,en,q=0.9\r\n"		\
								"\r\n" )


#define SIZEOF_tx_text  (256)
static char    m_msg_tx_text[ SIZEOF_tx_text ];


static SOCKET  m_socket_tcp_client = (-1);


//////// ADDRESS for tcp-ip socket
static struct sockaddr_in  m_tcpClient_addr;




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


uint8_t  m_state_announced = (0);





/**
 * \brief Callback to get the Data from socket.
 *
 * \param[in] sock socket handler.
 * \param[in] u8Msg socket event type. Possible values are:
 *  - SOCKET_MSG_BIND
 *  - SOCKET_MSG_LISTEN
 *  - SOCKET_MSG_ACCEPT
 *  - SOCKET_MSG_CONNECT
 *  - SOCKET_MSG_RECV
 *  - SOCKET_MSG_SEND
 *  - SOCKET_MSG_SENDTO
 *  - SOCKET_MSG_RECVFROM
 * \param[in] pvMsg is a pointer to message structure. Existing types are:
 *  - tstrSocketBindMsg
 *  - tstrSocketListenMsg
 *  - tstrSocketAcceptMsg
 *  - tstrSocketConnectMsg
 *  - tstrSocketRecvMsg
 */
static void tcpClient_socket_callback(SOCKET sock, uint8_t u8Msg, void *pvMsg)
{
	switch (u8Msg)
	{
	case SOCKET_MSG_CONNECT:
		{
			tstrSocketConnectMsg *pstrConnect = (tstrSocketConnectMsg *)pvMsg;
			if (pstrConnect && pstrConnect->s8Error >= 0) {
				printf("CB_socket(): Connect success\r\n");
				printf("CB_socket(): Send:\"%s\" ... ... ...\r\n", m_msg_tx_text);
				send(m_socket_tcp_client, m_msg_tx_text, strlen(m_msg_tx_text), (0));
			} else {
				printf("CB_socket(): connect error\r\n");
				close(m_socket_tcp_client);
				m_socket_tcp_client = (-1);
			}
		}
		break;


	case SOCKET_MSG_SEND:
		{
			printf("CB_socket(): Send success.  Recv...\r\n");
			recv(m_socket_tcp_client, m_rxBuffer, m_rxBuffer_SIZE, 0);
		}
		break;


	case SOCKET_MSG_RECV:
		{
			tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
			if (pstrRecv && pstrRecv->s16BufferSize > 0) {
				printf("CB_socket(): Recv success\r\n");
				printf("CB_socket(): Recv:\"%s\"\r\n", pstrRecv->pu8Buffer);
				m_state_announced = (1);
			} else {
				printf("CB_socket(): recv error\r\n");
			}
			close(m_socket_tcp_client);
			m_socket_tcp_client = (-1);
		}
		break;


	default:
		printf("CB_socket(): UNKNOWN -- (%d)\r\n", u8Msg);
		break;
	}

}



void tcpClient_init()
{
	//////// Initialize socket address structure
	m_tcpClient_addr.sin_family      = AF_INET;
	m_tcpClient_addr.sin_port        = _htons(TCP_SERVER_PORT);
	m_tcpClient_addr.sin_addr.s_addr = _htonl(TCP_SERVER_IP);


	//////// setup the payload to send
	strcpy (m_msg_tx_text,  STRING_HTTP_GET_MSG_0);
	strcat (m_msg_tx_text,  "(");
	strcat (m_msg_tx_text,  m_mac_addr_str);
	strcat (m_msg_tx_text,  ")(");
	strcat (m_msg_tx_text,  m_configuration_AP_ssid);
	strcat (m_msg_tx_text,  ")");
	strcat (m_msg_tx_text,  STRING_HTTP_GET_MSG_1);


	//////// Register the Callback, for the Socket module
	registerSocketCallback(tcpClient_socket_callback, NULL);
}


void tcpClient_loopSlice()
{
	int8_t ret;


	//////// IF Wifi is CONNECTED, then TCP Comm's are possible
	if (m_state_wifi_router_connected)
	{
		//////// IF NOT Announced yet, attempt comms
		if (! m_state_announced)
		{
			//////// IF the Client-Socket is INVALID, attempt connection
			if (m_socket_tcp_client < 0)
			{
				printf("LPC: Connected-Loop: socket < 0\r\n");

				//////// Attempt connect.
				if ((m_socket_tcp_client = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
					printf("LPC: failed to create TCP client socket error!\r\n");
					return;
				}
				printf("LPC: socket()- success\r\n");

				ret = connect(m_socket_tcp_client,
				(struct sockaddr *)(& m_tcpClient_addr),
				sizeof(struct sockaddr_in));

				if (ret < 0) {
					printf("LPC: ERROR- connect()==>(%d)\r\n", ret);
					close(m_socket_tcp_client);
					m_socket_tcp_client = (-1);
				}
				printf("LPC: Connect()-->(%d) ...\r\n", ret);

			}////////-end-if-socket<0
		}////////-end-if-NOT-announced
	}////////-end-if-router-connected


	//////// RETURN TO MAIN-LOOPER
	return;
}





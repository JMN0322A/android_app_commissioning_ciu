////////
//////////////////////////////////[ brascii64b.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"




char brascii64b_bin_to_char( unsigned char x )
{
	char r;

	if( x < (26) )  {
		r = ((x) + ('A'));
	}
	else
	if( x < (26+26) )  {
		r = ((x - 26) + ('a'));
	}
	else
	if( x < (26+26+10) )  {
		r = ((x - 26 - 26) + ('0'));
	}
	else
	if( (0x3e) == x )  {
		r = ('-');
	}
	else
	if( (0x3f) == x )  {
		r = ('~');
	}
	else  {
		r =('\xff');
	}

	return (r);
}


void brascii64b_encode( unsigned char *input_ptr, int input_len, char *pOutput )
{
	unsigned char bin[4];
	unsigned char num[4];
	char          b64[4];
	unsigned char xByte;
	int i, len;

	for(;;)
	{
		//// prepare
		b64[0] = b64[1] = b64[2] = b64[3] = ('\0');
		bin[0] = bin[1] = bin[2]          = (unsigned char)(0);
		num[0] = num[1] = num[2]          = (unsigned char)(0);
		len = 0;

		//// process the next three input bytes
		for( i=0; i<3; i++ )
		{
			//// if no more, stop
			if( input_len <= 0)
			break;

			//// get the next data byte
			xByte = (int)(*input_ptr++);
			--input_len;

			//// save it into array of three bytes
			bin[i] = xByte;
			//// count it
			++len;
		}
		if ((0)==len)
		return;

		//// process into four values(0-63)
		num[0] = ((bin[0] >> 2) & 0x3f);
		num[1] = ((bin[0] << 4) & 0x30) | ((bin[1] >> 4) & 0x0f);
		num[2] = ((bin[1] << 2) & 0x3c) | ((bin[2] >> 6) & 0x03);
		num[3] = ((bin[2]     ) & 0x3f);

		//// translate four values into characters
		b64[0] = brascii64b_bin_to_char( num[0] );
		b64[1] = brascii64b_bin_to_char( num[1] );
		b64[2] = brascii64b_bin_to_char( num[2] );
		b64[3] = brascii64b_bin_to_char( num[3] );

		//// output it
		if( len == 1 )  {
			*pOutput++ = b64[0];
			*pOutput++ = b64[1];
			*pOutput++ = ('_');
			*pOutput++ = ('_');
		}
		else
		if( len == 2 )  {
			*pOutput++ = b64[0];
			*pOutput++ = b64[1];
			*pOutput++ = b64[2];
			*pOutput++ = ('_');
		}
		else
		if( len == 3 )  {
			*pOutput++ = b64[0];
			*pOutput++ = b64[1];
			*pOutput++ = b64[2];
			*pOutput++ = b64[3];
		}

	}
	*pOutput = ('\0');
}


unsigned char brascii64b_char_to_bin( char x )
{
	unsigned char r;

	if( ('A') <= x  &&  x <= 'Z' )  {
		r = x - ('A');
	}
	else
	if( ('a') <= x  &&  x <= 'z' )  {
		r  = x - ('a');
		r += (26);
	}
	else
	if( ('0') <= x  &&  x <= '9' )  {
		r = x - ('0');
		r += (26+26);
	}
	else
	if( ('-') == x )  {
		r = (0x3e);
	}
	else
	if( ('~') == x )  {
		r = (0x3f);
	}
	else
	if( ('_') == x )  {
		r = (0x00);
	}
	else  {
		r = (0x7f);
	}
	return (r);
}


int brascii64b_decode( char *input_ptr, int input_len, unsigned char *pOutput )
{
	char b64[4];
	unsigned char bin[4];
	char x,xbin;
	int i, len, nCount;

	nCount = (0);
	while( input_len > 0 )
	{
		//// prepare
		b64[0] = b64[1] = b64[2] = b64[3] =          (char)(0);
		bin[0] = bin[1] = bin[2]          = (unsigned char)(0);
		len = 0;

		//// process the next four input characters
		for( i=0; i<4; i++ )
		{
			//// if no more, stop
			if( input_len <= 0 )
			break;

			//// get the next char
			x = *input_ptr++;
			--input_len;
			//// ignore white space
			//// printf( "%c", x );
			if( (' ')==x || ('\r')==x || ('\n')==x )
			break;
			//// if padding-space, stop
			if( ('=') == x )
			break;

			//// convert ascii-char to the binary 0..63
			xbin = brascii64b_char_to_bin( x );
			//// if error, stop
			if( xbin & 0xc0 )  {
				printf( "brascii64b_decode()- ERROR-->(%02x(%02x))\n", xbin, x );
				i=999;
				break;
			}
			////////printf("*b64_decode(): \'%c\' : %02x => %02x\n", x, x, xbin);

			//// save the binary-ified binary 0..63
			b64[i] = (xbin);
			//// count the piece
			++len;
		}
		if (i>=999)
		break;

		//// process into three binary data bytes
		bin[0] = ((b64[0] << 2) & 0xfc) | ((b64[1] >> 4) & 0x03);
		bin[1] = ((b64[1] << 4) & 0xf0) | ((b64[2] >> 2) & 0x0f);
		bin[2] = ((b64[2] << 6) & 0xc0) | ((b64[3])      & 0x3f);

		////      --> three Bytes  len==4
		////   =  --> two   Bytes  len==3
		////  ==  --> one   Byte   len==2
		if( len >= 2 )  {
			*pOutput++ = bin[0];
			++nCount;
			if( len >= 3 )  {
				*pOutput++ = bin[1];
				++nCount;
				if( len >= 4 )  {
					*pOutput++ = bin[2];
					++nCount;
				}
			}
		}


	}

	return nCount;
}





////////
//////////////////////////////////[ main.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>


#define STRING_HEADER	"\r\n\r\n"													\
						"-- ============================================== --\r\n"	\
						"-- Novosel SAMG55-Xplained + WINC1500 Development --\r\n"	\
						"-- WINC1500 TCP client example --\r\n"						\
						"-- "BOARD_NAME " --\r\n"									\
						"-- Compiled: "__DATE__ " "__TIME__ " --\r\n"




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

//////// Receive buffer
uint8_t  m_rxBuffer[ m_rxBuffer_SIZE ];


uint32_t m_random_128bits[ m_random_128bits_LEN ];

uint8_t  m_mac_addr[ M2M_MAC_ADDRES_LEN ];
char     m_mac_addr_str[24] = "00:00:00:00:00:00";

char     m_APmode_SSID[34] = "SAT_";
char     m_APmode_PSWD[34] = "0000000000";

bool     m_state_wifi_connected = false;

uint8_t  m_triggering_end_tcpServer = (0);
uint8_t  m_APP_is_concluded  = (0);


char     m_configuration_AP_ssid[SIZEOF_AP_STR] = ("SOPHI");//("da-robotics");
char     m_configuration_AP_pswd[SIZEOF_AP_STR] = ("3342010865");//("darkhelmetget$jammed");
uint32_t m_configuration_tm_connect_Wifi        = (0);///////////(1562227200);
uint32_t m_configuration_tm_connect_Cloud       = (1563609600);






int main(void)
{
	int8_t  ret;



	//////// Initialize the board
	sysclk_init();
	board_init();

	//////// Initialize the UART console
	uartConsole_init();
	printf(STRING_HEADER);

	//////// Initialize the BSP
	nm_bsp_init();

	//////// Initialize Wifi Connectivity
    wifiProcs_init();

	//////// Initialize socket module
	socketInit();



	//////// Attempt to connect to Wifi Router
    wifiProcs_connectRouter();


	//////// Note: this points SOCKET-Callback to tcp-client-announce
    tcpClient_init();

    commissionProcs_init();



	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	//////////////// LOOP-FOREVER
	while(1)
	{
		//////// Dispatch pending events from network controller
		m2m_wifi_handle_events(NULL);


		//////// Loop-SLices for each module

		tcpClient_loopSlice();

        buttonSW0_loopSlice();

		commissionProcs_loopSlice();

		tcpServer_loopSlice();
	}
	///////////////////////////////////////////////////////////////////
	return (0);
}




void uartConsole_init(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	//// Configure UART console
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}






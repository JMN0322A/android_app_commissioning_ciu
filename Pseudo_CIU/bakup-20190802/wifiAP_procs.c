

#include "asf.h"

/*-
#include "mainext.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>

static	tstrWifiInitParam  m_paramWifi;

//////// current IP number
//uint8_t  m_ip_number[4] = {0,0,0,0};

//////// Wifi connection state
static  m_state_wifi_AP_enabled = (1);
uint8_t m_state_wifi_AP_connected = (0);
-*/




/*-
#define SIZEOF_AP_STR   (34)
char      m_conf_AP_ssid[SIZEOF_AP_STR] = ("STA_000000000000000000");
char      m_conf_AP_pswd[SIZEOF_AP_STR] = ("0000000000");

uint32_t  m_conf_tm_connect_Wifi        = (0);///////////(1562227200);
uint32_t  m_conf_tm_connect_Cloud       = (1563609600);
-*/







/**
 * \brief Callback to get the Wi-Fi status update.
 *
 * \param[in] u8MsgType type of Wi-Fi notification. Possible types are:
 *  - [M2M_WIFI_RESP_CURRENT_RSSI](@ref M2M_WIFI_RESP_CURRENT_RSSI)
 *  - [M2M_WIFI_RESP_CON_STATE_CHANGED](@ref M2M_WIFI_RESP_CON_STATE_CHANGED)
 *  - [M2M_WIFI_RESP_CONNTION_STATE](@ref M2M_WIFI_RESP_CONNTION_STATE)
 *  - [M2M_WIFI_RESP_SCAN_DONE](@ref M2M_WIFI_RESP_SCAN_DONE)
 *  - [M2M_WIFI_RESP_SCAN_RESULT](@ref M2M_WIFI_RESP_SCAN_RESULT)
 *  - [M2M_WIFI_REQ_WPS](@ref M2M_WIFI_REQ_WPS)
 *  - [M2M_WIFI_RESP_IP_CONFIGURED](@ref M2M_WIFI_RESP_IP_CONFIGURED)
 *  - [M2M_WIFI_RESP_IP_CONFLICT](@ref M2M_WIFI_RESP_IP_CONFLICT)
 *  - [M2M_WIFI_RESP_P2P](@ref M2M_WIFI_RESP_P2P)
 *  - [M2M_WIFI_RESP_AP](@ref M2M_WIFI_RESP_AP)
 *  - [M2M_WIFI_RESP_CLIENT_INFO](@ref M2M_WIFI_RESP_CLIENT_INFO)
 * \param[in] pvMsg A pointer to a buffer containing the notification parameters
 * (if any). It should be casted to the correct data type corresponding to the
 * notification type. Existing types are:
 *  - tstrM2mWifiStateChanged
 *  - tstrM2MWPSInfo
 *  - tstrM2MP2pResp
 *  - tstrM2MAPResp
 *  - tstrM2mScanDone
 *  - tstrM2mWifiscanResult
 */
/*-
void callback_wifi_ap_connection(uint8_t u8MsgType, void *pvMsg)
{
	switch (u8MsgType)
	{
	case M2M_WIFI_RESP_CON_STATE_CHANGED:
		{
			tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
			if (M2M_WIFI_CONNECTED == pstrWifiState->u8CurrState) {
				printf("CB_wifi: StateX==> CONNECTED\r\n");
				m2m_wifi_request_dhcp_client();
			}
			else if (M2M_WIFI_DISCONNECTED == pstrWifiState->u8CurrState) {
				m_state_wifi_router_connected = (0);
				printf("CB_wifi: StateX==> DISCONNECTED\r\n");
				//////// only retry connection if enabled
				if (m_state_wifi_router_enabled) {
					wifiProcs_connectRouter();
				}
			}
		}
		break;


	case M2M_WIFI_REQ_DHCP_CONF:
		{
			uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
			m_ip_number[0] = pu8IPAddress[0];
			m_ip_number[1] = pu8IPAddress[1];
			m_ip_number[2] = pu8IPAddress[2];
			m_ip_number[3] = pu8IPAddress[3];
			printf("CB_wifi: M2M_WIFI_REQ_DHCP_CONF: IP is %u.%u.%u.%u\r\n",
					m_ip_number[0], m_ip_number[1], m_ip_number[2], m_ip_number[3] );

			if (m_state_wifi_router_enabled) {
				m_state_wifi_router_connected = (1);
			}
		}
		break;


	case M2M_WIFI_RESP_GET_SYS_TIME:
		{
			tstrSystemTime *strSysTime_now = (tstrSystemTime *)pvMsg;

			for (int8_t i=0; i<16; i++) {
				if (8==i) {
					printf("-TIME--- Winc-GMT== %u:%02u:%02u\r\n",
					strSysTime_now->u8Hour,           // hour (86400 equals secs per day)
					strSysTime_now->u8Minute,         // minute (3600 equals secs per minute)
					strSysTime_now->u8Second);        // second
				}
				for(int8_t j=0; j<8; j++) {
					printf("-TIME");
				}
				printf("\r\n");
			}
			break;
		}


	default:
		{
			printf("CB_wifi: UNKNOWN (%d)\r\n", u8MsgType);
		}
		break;
	}

}



void wifiAPProcs_init()
{
	int8_t  ret;


	//////// default - enable wifi RE-Connect
	m_state_wifi_router_enabled = (1);

	//////// Initialize Wifi parameters structure. CLEAR-ZERO
	memset((uint8_t *)(& m_paramWifi), (0), sizeof(tstrWifiInitParam));

	//////// Initialize Wifi driver with data and status callbacks
	m_paramWifi.pfAppWifiCb = callback_wifi_ap_connection;/////////////////////callback_wifi_connection;
	ret = m2m_wifi_init(& m_paramWifi);
	if (M2M_SUCCESS != ret) {
		printf("wifiAPProcs_init(): m2m_wifi_init call error!(%d)\r\n", ret);

		while (1) {
			;////////--infinite-loop
		}
	}
}
-*/





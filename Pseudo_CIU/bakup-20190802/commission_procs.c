////////
//////////////////////////////////[ commission_procs.c ]/////////////////////////////
////////


#include "asf.h"
#include "main.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>


static uint8_t  m_commissioning_mode = (0);
static uint8_t  m_state_setup_ssid = (0);
static uint8_t  m_state_disconnecting_router = (0);
static uint8_t  m_zulu = (0);


///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

uint8_t  m_state_AP_mode = (0);




void commissionProcs_init()
{
	m_commissioning_mode = (0);
	m_state_setup_ssid = (0);
    m_state_disconnecting_router = (0);
	m_state_AP_mode = (0);
	m_triggering_end_tcpServer = (0);
	m_APP_is_concluded = (0);
}


void commissionProcs_exitCommnMode()
{
	printf("commProcs_exitCommnMode()- Clear mem.\r\n");
	commissionProcs_init();

	printf("commProcs_exitCommnMode()- Disable AP-Mode.\r\n");
	wifiProcs_STA_mode();

	printf("commProcs_exitCommnMode()- Restore wifi-->Router\r\n");
	wifiProcs_connectRouter();

	printf("commProcs_exitCommnMode()- Start NORMAL-MODE\r\n");
}


void commissionProcs_loopSlice()
{
	//////// determine if triggered INTO Commissioning-Mode
	if (! m_commissioning_mode) {
		if ((buttonSW0_isTriggered) && (! buttonSW0_isActive())) {
			//////// Triggered: Commissioning-mode
			m_commissioning_mode = (1);
			printf("commP_loop()- ---Start-COMMISSION-MODE----\r\n");
		}
		else {
			//////// otherwise go no further.
			return;
		}
	}

    //////// COMMISSIONING-MODE /////////////
    //////// ensure triggers are cleared
	buttonSW0_isTriggered = (0);


    //////// IF ROUTER CONNECTED, WIFI DISCONNECT
	if (m_state_wifi_router_connected) {
		if (! m_state_disconnecting_router) {
			m_state_disconnecting_router = (1);
			wifiProcs_disconnectRouter();
		}
		//////// go no further until router disconnected
		return;
	}
	//////// clear the disconnecting flag
	m_state_disconnecting_router = (0);



    //////// ENSURE NEW SSID IS CREATED
    if (! m_state_setup_ssid) {
		m_state_setup_ssid = (1);
		//////// calculate the new random ssid
		srand(buttonSW0_getRandomSeed());
		//
		printf("commp_loop()- rand(): ");
		for (int i=0; i<6; i++) {
			printf("%08x ", (rand()));
		}
		printf("\r\n");

	    //////// generate new wifi SSID/PASSW using the random value above
		get_wifi_settings();
		printf("commp_loop()- --%s--%s--\r\n", m_APmode_SSID, m_APmode_PSWD);
		return;
	}



	//////// STARTUP THE AP_MODE
	if (! m_state_AP_mode) {
		m_state_AP_mode = (1);

		wifiProcs_AP_mode();
		return;
	}


	//////// if AP-Mode started, look for button switch and go back to normal.
	if (m_state_AP_mode) {
		if (buttonSW0_isActive()) {
			commissionProcs_exitCommnMode();
			return;
		}
	}


}





////////
//////////////////////////////////[ tcp_server.c ]/////////////////////////////
////////


#include "asf.h"
#include "pseudo_ciu.h"
#include <string.h>



void  proc_recv(SOCKET sock, tstrSocketRecvMsg *pstrRecv);



struct sockaddr_in  m_addr;
static SOCKET   m_socket_client = (-1);
static SOCKET   m_socket_listen = (-1);


//////// ADDRESS for tcp-ip socket
static struct sockaddr_in  m_tcpClient_addr;


//////// FLAG, for sensing if activity is overdue
static m_flag_has_activity = (0);



void socket_server_callback(SOCKET sock,  uint8_t u8Msg,  void *pvMsg)
{
	switch (u8Msg)
	{
	case SOCKET_MSG_BIND:
	{
		tstrSocketBindMsg *pstrBind = (tstrSocketBindMsg *)pvMsg;
		if (pstrBind && pstrBind->status == 0) {
			printf("socketCB()- Bind success. => next: Listen-()\r\n");
			listen(m_socket_listen, 1);
		} else {
			printf("socketCB()- Bind() ERROR!\r\n");
			m_socket_listen = -1;
		}
		break;
	}

	case SOCKET_MSG_LISTEN:
	{
		tstrSocketBindMsg *pstrListen = (tstrSocketListenMsg *)pvMsg;
		if (pstrListen && pstrListen->status == 0) {
			printf("socketCB()- Listen success. => next: Accept-()\r\n");
			accept(m_socket_listen, NULL, NULL);
		} else {
			printf("socketCB()- Listen() ERROR!\r\n");
			close(m_socket_listen);
			m_socket_listen = -1;
		}
		break;
	}

	case SOCKET_MSG_ACCEPT:
	{
		tstrSocketAcceptMsg *pstrAccept = (tstrSocketAcceptMsg *)pvMsg;
		if (pstrAccept) {
			printf("socketCB()- Accept success. => next: recv-()\r\n");
			accept(m_socket_listen, NULL, NULL);
			m_socket_client = pstrAccept->sock;
			recv(m_socket_client, m_rxBuff, (m_rxBuff_SIZE), 0);
		} else {
			printf("socketCB()- Accept() ERROR!\r\n");
			close(m_socket_listen);
			m_socket_listen = -1;
		}
		break;
	}

	case SOCKET_MSG_SEND:
	{
		printf("socketCB()- send() success.\r\n");
		printf("socketCB()- End.\r\n");
		printf("socketCB()- close socket\r\n");
		close(m_socket_listen);
		close(m_socket_client);
		m_socket_client = -1;
		m_socket_listen = -1;
		//
		printf("socketCB()- closed. => next: Setup-tcpServer-()\r\n");
		tcpServer_setup();
		//
		break;
	}

	case SOCKET_MSG_RECV:
	{
		tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
		if (pstrRecv && pstrRecv->s16BufferSize > 0) {
			proc_recv(sock, pstrRecv);
		}
		else {
			printf("socketCB()- Recv() ERROR!\r\n");
			close(m_socket_client);
			m_socket_client = -1;
			close(m_socket_listen);
			m_socket_listen = -1;
		}
		break;
	}

	default:
		printf("socketCB()- unknown case %d\r\n", u8Msg);
		break;
	}

}



static uint8_t calc_msg_checksum(uint8_t * ptr, uint8_t len) {
	uint16_t csum = 0;
	for (int8_t i=0; (i < (int8_t)len); i++) {
		csum += (uint16_t)(*ptr++);
	}
	csum %= 0x00fe;
	csum ^= 0x00e5;
	return (csum);
}



void proc_recv(SOCKET sock, tstrSocketRecvMsg *pstrRecv)
{
	extern void proc_recv_respondTx_error(SOCKET sock);///////////////////////////////////////////----
	char     *pch = pstrRecv->pu8Buffer;
	int16_t  len  = pstrRecv->s16BufferSize;
	int8_t   bValid;
	int16_t  i,j;


	//////// Set the HAS-ACTIVITY flag. This will be cleared when tested in "tcp_server_check_activity()"
	m_flag_has_activity = (1);


	printf("proc_recv()-RX(%d-bytes)-", len);
	for (i=0; i<len; i++) {
		printf((0==i)?("%02x"):("-%02x"), (uint8_t)pch[i] );
	}

	uint8_t dlen = (2 + pch[1]);
	uint8_t chk = calc_msg_checksum(pch, dlen);
	printf("---chk(%02x)-Checksum-",  chk  );
	if (chk == pch[dlen])
	{
		bValid = (1);
		printf("OK\r\n");

		if (('?')==pch[0]) {
			extern void proc_recv_respondTx_query(SOCKET);//////////////////////////////////////----
			proc_recv_respondTx_query(sock);
			return;
		}

		if (('*')==pch[0]) {
			extern void proc_recv_respondTx_save(SOCKET, uint8_t *pch);////////////////////////----
			proc_recv_respondTx_save(sock, pch);
			return;
		}

		if (('.')==pch[0]) {
			extern void proc_recv_respondTx_exit(SOCKET, uint8_t *pch);////////////////////////----
			proc_recv_respondTx_exit(sock, pch);
			return;
		}

	}

	printf("proc_recv()-ERROR\r\n");
	proc_recv_respondTx_error(sock);
}



void tcpServer_setup()
{
	m_addr.sin_family      = AF_INET;
	m_addr.sin_port        = _htons(TCPIP_SERVER_PORT);
	m_addr.sin_addr.s_addr = _htonl(  (TCPIP_SERVER_IP_0 << 24)    \
									| (TCPIP_SERVER_IP_1 << 16)    \
									| (TCPIP_SERVER_IP_2 <<  8)    \
									| (TCPIP_SERVER_IP_3      ) );

	registerSocketCallback(socket_server_callback, NULL);


	if (m_socket_listen < 0) {
		if ((m_socket_listen = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			printf("tcpServer_setup()- ERROR - create tcp server socket\r\n");
		}
		printf("tcpServer_setup()- create socket (%d) => next: Bind-()\r\n", (int)(m_socket_listen) );
	}
	else {
		printf("tcpServer_setup()- reusing socket (%d) => next: Bind-()\r\n", (int)(m_socket_listen) );
	}

	bind(m_socket_listen, (struct sockaddr *)(& m_addr), sizeof(struct sockaddr_in));
}



void tcpServer_close()
{
	if (m_socket_listen >= 0) {
		close(m_socket_listen);
		m_socket_listen = (-1);
		printf("tcpServer_close()- close Listen-socket\r\n");
	} else {
		printf("tcpServer_close()- Listen-socket already Closed\r\n");
	}

	if (m_socket_client >= 0) {
		close(m_socket_client);
		m_socket_client = (-1);
		printf("tcpServer_close()- close Client-socket\r\n");
	} else {
		printf("tcpServer_close()- Client-socket already Closed\r\n");
	}

}











void proc_recv_respondTx_error(SOCKET sock)
{
	uint8_t csum;

	m_txBuff[0] = ('!');
	m_txBuff[1] = (0x01);
	m_txBuff[2] = (0x00);
	csum = calc_msg_checksum(m_txBuff, 3);
	m_txBuff[3] = (csum);
	m_txBuff[4] = ('\n');
	m_txBuff[5] = ('\n');
	m_txBuff[6] = ('\n');
	m_txBuff[7] = ('\n');///////////////////////////////////////////////////////
	printf("proc_recv_responseTx_error()-[%d]-TX: 21-01-00-(%02x)-10-10-10-10\r\n", sock, csum);
	send(sock, m_txBuff, (8), 0);
}




// This is needed because, of connection recovery.
// If the tcp-server drops or fails, it needs to listen again for additional connections.
// However, the tcp-server needs to know when the conversation with APP is completed,
// in order to know when to transition back to "normal" mode
//
// A second flag is needed to trigger CIU to know when it needs to
// automatically return to AP-mode, for subsequent APP connection 
// OTherwise, the CIU would have to assume it is always needing to return.

void proc_recv_respondTx_exit(SOCKET sock, uint8_t *pch)
{
	extern uint8_t  m_APP_session_concluded;
	//////// FIRST, examine the payload for the FLAG that DIRECTs the CIU to RE-CONNECT
	//m_flag_reconnect_to_APP = pch[2];
	//printf("proc_recv_responseTx_exit()-  RE-CONNECT FLAG == %d\r\n", m_flag_reconnect_to_APP);
	m_APP_session_concluded = pch[2];
	printf("proc_recv_responseTx_exit()- APP_session_concluded == %d\r\n", m_APP_session_concluded);



	uint8_t csum;

	m_txBuff[0] = ('.');
	m_txBuff[1] = (0x01);
	m_txBuff[2] = (0x00);
	csum = calc_msg_checksum(m_txBuff, 3);
	m_txBuff[3] = (csum);
	m_txBuff[4] = ('\n');
	m_txBuff[5] = ('\n');
	m_txBuff[6] = ('\n');
	m_txBuff[7] = ('\n');///////////////////////////////////////////////////////
	printf("proc_recv_responseTx_exit()-[%d]-TX: 2e-01-00-(%02x)-10-10-10-10\r\n", sock, csum);
	send(sock, m_txBuff, (8), 0);

	m_APP_session_concluded = (1);
	/////////////////////// TODO: --- THE ANNOUNCE REQUIRES tcpClient_init() ////////----
}








void proc_recv_respondTx_query(SOCKET sock)
{
	uint8_t  csum;
	int16_t  offs, i;

    //////// Query result BEGINS with Colon(:)
	m_txBuff[0] = (':');

    //////// the position to START saving the response
	offs = (2);

    //////// Append the Wifi AP-SSID, INCLUDING the terminating NULL-('\0')
    for( i=0; (i < SIZEOF_AP_STR); i++) {
	    uint8_t ch = m_configuration_Router_ssid[i];
	    m_txBuff[offs++] = (ch);
	    if (('\0')==ch) {
		    break;
	    }
    }

    //////// Append the Wifi AP-PASSWORD, INCLUDING the terminating NULL-('\0')
    for( i=0; (i < SIZEOF_AP_STR); i++) {
	    uint8_t ch = m_configuration_Router_pswd[i];
	    m_txBuff[offs++] = (ch);
	    if (('\0')==ch) {
		    break;
	    }
    }

    //////// append the 32-bit-integer Timestamp of last Wifi-connect
	////*((uint32_t *)(m_tmpSvr + ndx)) = (m_configuration_tm_connect_Wifi);
	////ndx += (sizeof(uint32_t));
	sprintf(m_txBuff + offs, "%d", m_configuration_tm_connect_Wifi);
	while(('\0') != m_txBuff[offs++]);

	//////// append the 32-bit-integer Timestamp of last Cloud-connect
	////*((uint32_t *)(m_tmpSvr + ndx)) = (m_configuration_tm_connect_Cloud);
	////ndx += (sizeof(uint32_t));
	sprintf(m_txBuff + offs, "%d", m_configuration_tm_connect_Cloud);
	while(('\0') != m_txBuff[offs++]);

	//////// Save the number of bytes in this payload
	m_txBuff[1] = (offs - 2);

    //////// calculate the Checksum
	csum = calc_msg_checksum(m_txBuff, offs);
	m_txBuff[offs++] = (csum);

    //////// append three nulls
	m_txBuff[offs++] = ('\0');
	m_txBuff[offs++] = ('\0');
	m_txBuff[offs++] = ('\0');

	printf("proc_recv_respondTx_query()-[%d]-TX:", sock);
	for (i=0; i<offs; i++) {
		printf("-%02x", m_txBuff[i]);
	}
	printf("\r\n");
	send(sock, m_txBuff, (uint16_t)offs, 0);
}






// Need to save these config values over into THE SSID / PASSWORD


void proc_recv_respondTx_save(SOCKET sock, uint8_t *pch)
{
	int16_t  dlen, offs, i;
	uint8_t  ch;
	
	//////// obtain the length of the data payload
	dlen = (((uint16_t)(pch[1]) & 0x00ff) + 2);

	//////// initial position in the data payload to begin copying from
	offs = 2;

	//////// SAVE THE SSID, including the terminating NULL-('\0')
	for (i=0; i < (SIZEOF_AP_STR - 1); i++) {
		ch = pch[offs++];
		m_configuration_Router_ssid[i] = ch;
		if ((('\0')==ch) || (i >= dlen)) {
			break;
		}
	}
	printf("proc_recv_respondTx_save()- ssid=\"%s\"\r\n", m_configuration_Router_ssid);

	//////// SAVE THE PASSWORD, including the terminating NULL-('\0')
	for (i=0; i < (SIZEOF_AP_STR - 1); i++) {
		ch = pch[offs++];
		m_configuration_Router_pswd[i] = ch;
		if ((('\0')==ch) || (i >= dlen)) {
			break;
		}
	}
	printf("proc_recv_respondTx_save()- pswd=\"%s\"\r\n", m_configuration_Router_pswd);

	//////// Send back the RESPONSE, with the new STORED configuration data
	proc_recv_respondTx_query(sock);
}





void tcpServer_loopSlice()
{
	extern uint8_t wifiProcs_getConnectionMode();
	extern uint8_t wifiProcs_getStatusWifi();
	static bool bConnected = false;
	int8_t ret;


	if (MODE_CONN_APP != wifiProcs_getConnectionMode()) {
		return;
	}
	//////// IN CONN-APP MODE /////////////////////


	//////////////////////////////////////////////////////	if (m_state_wifi_connected) {
	if (WIFI_CONNECTED_APP == wifiProcs_getStatusWifi()) {
		if (! bConnected) {
			bConnected = true;
			printf("tcp_server_loopSlice()- Connected ==> tcpServer_setup()\r\n");
			tcpServer_setup();
		}
		} else {
		if (bConnected) {
			bConnected = false;
			printf("tcp_server_loopSlice()- DIS_connected ==> tcpServer_close()\r\n");
			tcpServer_close();
		}
	}
	
}








////////
//////////////////////////////////[ tcp_client_announce.c ]/////////////////////////////
////////


#include "asf.h"
#include "pseudo_ciu.h"
#include <string.h>



////////// TODO: need to use dnsLoopup() to resolve this IP-number
#define CLOUD_SERVER_IP         ((172<<24)|(217<<16)|(7<<8)|(180))
#define CLOUD_SERVER_PORT       (80)

#define STRING_HTTP_GET_MSG_0 (	"GET /histov0?w=1&msg=" )

#define STRING_HTTP_GET_MSG_1 (	" HTTP/1.1\r\n"								\
								"Host: wifi-state-jonovos.appspot.com\r\n"	\
								"User-Agent: Atmel Winc15x0 Custom\r\n"		\
								"Accept: text/html\r\n"						\
								"Referer: none\r\n"							\
								"Accept-Language: en-US,en,q=0.9\r\n"		\
								"\r\n" )


/////////////// TODO: Put this buffer into common file, for re-use.
#define SIZEOF_tx_text  (256)
static char    m_msg_tx_text[ SIZEOF_tx_text ];


static SOCKET  m_socket_tcp_client = (-1);


//////// ADDRESS for tcp-ip socket
static struct sockaddr_in  m_tcpClient_addr;


static uint8_t  m_tcpClient_Initialized = (0);


static uint8_t  m_flag_announced = (0);







/**
 * \brief Callback to get the Data from socket.
 *
 * \param[in] sock socket handler.
 * \param[in] u8Msg socket event type. Possible values are:
 *  - SOCKET_MSG_BIND
 *  - SOCKET_MSG_LISTEN
 *  - SOCKET_MSG_ACCEPT
 *  - SOCKET_MSG_CONNECT
 *  - SOCKET_MSG_RECV
 *  - SOCKET_MSG_SEND
 *  - SOCKET_MSG_SENDTO
 *  - SOCKET_MSG_RECVFROM
 * \param[in] pvMsg is a pointer to message structure. Existing types are:
 *  - tstrSocketBindMsg
 *  - tstrSocketListenMsg
 *  - tstrSocketAcceptMsg
 *  - tstrSocketConnectMsg
 *  - tstrSocketRecvMsg
 */
static void tcpClient_socket_callback(SOCKET sock, uint8_t u8Msg, void *pvMsg)
{
	switch (u8Msg)
	{
	case SOCKET_MSG_CONNECT:
		{
			tstrSocketConnectMsg *pstrConnect = (tstrSocketConnectMsg *)pvMsg;
			if (pstrConnect && pstrConnect->s8Error >= 0) {
				printf("CB_socket(): Connect success\r\n");
				printf("CB_socket(): Send:\"%s\" ... ... ...\r\n", m_msg_tx_text);
				send(m_socket_tcp_client, m_msg_tx_text, strlen(m_msg_tx_text), (0));
			} else {
				printf("CB_socket(): connect error\r\n");
				close(m_socket_tcp_client);
				m_socket_tcp_client = (-1);
			}
		}
		break;


	case SOCKET_MSG_SEND:
		{
			printf("CB_socket(): Send success.  Recv...\r\n");
			recv(m_socket_tcp_client, m_rxBuff, m_rxBuff_SIZE, 0);
		}
		break;


	case SOCKET_MSG_RECV:
		{
			extern void tcpClient_recv(uint8_t *p);
			tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
			if (pstrRecv && pstrRecv->s16BufferSize > 0) {
				tcpClient_recv(pstrRecv->pu8Buffer);
//				printf("CB_socket(): Recv success\r\n");
//				printf("CB_socket(): Recv:\"%s\"\r\n", pstrRecv->pu8Buffer);
//				m_state_announced = (1);
			} else {
				printf("CB_socket(): recv error\r\n");
			}
			close(m_socket_tcp_client);
			m_socket_tcp_client = (-1);
		}
		break;


	default:
		printf("CB_socket(): UNKNOWN -- (%d)\r\n", u8Msg);
		break;
	}

}



void tcpClient_recv(uint8_t *parg)
{
    printf("tcpClient_recv()- rx: %s\r\n", parg);

	//////// find the marker in the string ("t":)
	char *pch = strstr(parg, "\"t\":");
	//////// error if null
	if (NULL == pch) {
		printf("tcpClient_recv()- *ERROR- no \"t\": marker\r\n");
		return;
	}

	//////// advance PTR past the marker text (4-characters)
	pch += 4;
	//////// simple DIGIT parse of the string that follows to get the integer "t":1562000000
	uint32_t tm = 0;
	for(;;) {
		char ch = *pch++;
		if ((ch >= ('0')) && (ch <= ('9'))) {
			tm = ((10 * tm) + (ch - ('0')));
			continue;
		}
		break;
	}
	//////// ensure it contains a valid time-stamp
	if (tm > 1562000000) {
		//////// save the time-stamp from the CLOUD SERVICE
		m_configuration_tm_connect_Cloud = tm;
		printf("tcpClient_recv()- time = %d\r\n", m_configuration_tm_connect_Cloud);
	} else {
		printf("tcpClient_recv()- *Error- t=%d \r\n", m_configuration_tm_connect_Cloud);
	}

	//////// set this flag, so CIU doesn't continuously access this Cloud Function.
	m_flag_announced = (1);
}



void tcpClient_init()
{
	//////// Initialize socket address structure
	m_tcpClient_addr.sin_family      = AF_INET;
	m_tcpClient_addr.sin_port        = _htons(CLOUD_SERVER_PORT);
	m_tcpClient_addr.sin_addr.s_addr = _htonl(CLOUD_SERVER_IP);


	//////// setup the payload to send
	strcpy (m_msg_tx_text,  STRING_HTTP_GET_MSG_0);
	strcat (m_msg_tx_text,  "(");
	strcat (m_msg_tx_text,  m_mac_addr_str);
	strcat (m_msg_tx_text,  ")(");
	strcat (m_msg_tx_text,  m_configuration_Router_ssid);
	strcat (m_msg_tx_text,  ")");
	strcat (m_msg_tx_text,  STRING_HTTP_GET_MSG_1);


	//////// Register the Callback, for the Socket module
	registerSocketCallback(tcpClient_socket_callback, NULL);
}


void tcpClient_loopSlice()
{
	extern uint8_t wifiProcs_getConnectionMode();
	extern uint8_t wifiProcs_getStatusWifi();
	int8_t ret;

	if (MODE_CONN_ROUTER != wifiProcs_getConnectionMode()) {
		m_tcpClient_Initialized = (0); //////// ensure this is cleared before next client operation
		m_flag_announced = (0);        //////// ensure this is cleared before next client operation
		return;
	}
	//////// IN CONN-ROUTER MODE /////////////////////


	if (WIFI_CONNECTED_ROUTER != wifiProcs_getStatusWifi()) {
		return;
	}
	//////// IN WIFI-CONNECTED TO ROUTER /////////////////////


	if (m_flag_announced) {
		return;
	}
	//////// NOT Announced yet


	if (m_socket_tcp_client >= 0)  {
		return;
	}
	//////// NOT TCP-CONNECTED YET




	if (! m_tcpClient_Initialized) {
		m_tcpClient_Initialized = (1);
		printf("tcpClient_loopSlice()- NOT Initialized ==> tcpClient_init()\r\n");
		tcpClient_init();
	}





	printf("tcpClient_loopSlice()- socket < 0\r\n");

	//////// Attempt connect.
	if ((m_socket_tcp_client = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("tcpClient_loopSlice()- ERROR- create TCP client socket\r\n");
		return;
	}
	printf("tcpClient_loopSlice()- socket()- success\r\n");

	ret = connect(	m_socket_tcp_client,
					(struct sockaddr *)(& m_tcpClient_addr),
					sizeof(struct sockaddr_in));

	if (ret < 0) {
		printf("tcpClient_loopSlice()- ERROR- connect(): (%d)\r\n", ret);
		close(m_socket_tcp_client);
		m_socket_tcp_client = (-1);
	}
	printf("tcpClient_loopSlice()- Connect()-->(%d) ...\r\n", ret);



	//////// RETURN TO MAIN-LOOPER
	return;
}






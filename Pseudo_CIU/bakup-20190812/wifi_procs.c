////////
//////////////////////////////////[ wifi_procs.c ]/////////////////////////////
////////


#include "asf.h"
#include "pseudo_ciu.h"
#include "driver/include/m2m_wifi.h"
#include <string.h>




///////////////////////////////////////////////////////////////////////////////////

extern void wifiProcs_setMode(int m);
extern void wifiProcs_init();
extern void wifiProcs_Callback(uint8_t u8MsgType, void *pvMsg);

///////////////////////////////////////////////////////////////////////////////////

static	tstrWifiInitParam  m_param_Wifi;

#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define MAIN_WLAN_CHANNEL        (6)
static  tstrM2MAPConfig			 m_structM2MAPConfig;

#define          m_delay_before_wifi_router_connect_MAX (2*1000*1000)
static	int32_t  m_delay_before_wifi_router_connect   = (0);





///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


//////// current IP number
uint8_t  m_ip_number[4] = {0,0,0,0};


//////// INTENDED MODE for the Wifi module ////////////////////////////////////////
// It doesn't matter what the CURRENT mode is.
// The slice manager looks at  m_status_wifi_connection and articulates function calls.
uint8_t  m_designated_wifi_mode = (MODE_DISCONNECT);

//////// STATE of WIFI system
uint8_t  m_status_wifi = (WIFI_DISCONNECTED);

//////// SESSION FLAG
uint8_t  m_APP_session_concluded = (0);




void wifiProcs_Callback(uint8_t u8MsgType, void *pvMsg)
{
	printf("CB_wifi: *Callback*Entry.\r\n");

	switch (u8MsgType)
	{
		//////////////////////////////////////////////////////////////////////////////////
		case M2M_WIFI_RESP_CON_STATE_CHANGED:
		{
			tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;

			if (M2M_WIFI_CONNECTED == pstrWifiState->u8CurrState) {
				printf("CB_wifi: *Con*State*Changed* ==> CONNECT. Wait for DHCP...\r\n");
				//////// Low-level connected. Touch the DHCP
				m2m_wifi_request_dhcp_client();
				break;
			}

			if (M2M_WIFI_DISCONNECTED == pstrWifiState->u8CurrState) {
				print_div();
				if (MODE_CONN_ROUTER == m_designated_wifi_mode) {
					printf("CB_wifi: ==> DISCONNECTED (router)\r\n");
					//////// We should RE-Trigger the call to RE-CONNECT, after a DELAY
					//////// This DELAY is controlled in  wifiProcs_loopSLICE()
					//////// wifiProcs_loopSLICE() see this condition from  m_status_wifi==WIFI_DISCONNECTED
				}
				else if (MODE_CONN_APP == m_designated_wifi_mode) {
					printf("CB_wifi: ==> DISCONNECTED (app)\r\n");
					//////// Already true, at the moment AFTER it goes into AP mode, the FIRST time
					////////  Don't want to CAUSE a re-entry into AP mode.
					////////  In fact, Winc already transitioned. We don't have to DO anything.
					////////
					//////// modeAPP_loopSLICE(): m_APP_session ==> MODE_CON_ROUTER
				}
				else {
					printf("CB_wifi: ==> DISCONNECTED (??)\r\n");
					//////// THIS should NEVER HAPPEN
				}
				m_status_wifi = (WIFI_DISCONNECTED);
				break;
			}
		}
		break;


		//////////////////////////////////////////////////////////////////////////////////
		case M2M_WIFI_REQ_DHCP_CONF:
		{
			print_div();
			uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
			m_ip_number[0] = pu8IPAddress[0];
			m_ip_number[1] = pu8IPAddress[1];
			m_ip_number[2] = pu8IPAddress[2];
			m_ip_number[3] = pu8IPAddress[3];
			printf("CB_wifi: M2M_WIFI_REQ_DHCP_CONF: IP is %u.%u.%u.%u\r\n",
						m_ip_number[0], m_ip_number[1], m_ip_number[2], m_ip_number[3] );

			if (MODE_CONN_ROUTER == m_designated_wifi_mode) {
				m_status_wifi = (WIFI_CONNECTED_ROUTER);
				printf("CB_wifi: ==> CONNECTED **ROUTER**\r\n");
			} else {
				m_status_wifi = (WIFI_CONNECTED_APP);
				printf("CB_wifi: ==> CONNECTED **APP**\r\n");
			}

		}
		break;


		//////////////////////////////////////////////////////////////////////////////////
		case M2M_WIFI_RESP_GET_SYS_TIME:
		{
			tstrSystemTime *ptrSysTime_now = (tstrSystemTime *)pvMsg;
			m_configuration_tm_connect_Wifi = seconds_since_1970_Jan_01(
												ptrSysTime_now->u16Year,
												ptrSysTime_now->u8Month,
												ptrSysTime_now->u8Day,
												ptrSysTime_now->u8Hour,
												ptrSysTime_now->u8Minute,
												ptrSysTime_now->u8Second  );
			for (int8_t i=0; i<16; i++) {
				if (8==i) {
					printf("-TIMESTAMP-(Z) -- %4d-%02d-%02d - %u:%02u:%02u\r\n",
									ptrSysTime_now->u16Year,
									ptrSysTime_now->u8Month,
									ptrSysTime_now->u8Day,
									ptrSysTime_now->u8Hour,
									ptrSysTime_now->u8Minute,
									ptrSysTime_now->u8Second	);
					printf("-TIMESTAMP == %ld\r\n", m_configuration_tm_connect_Wifi);
				}
				for(int8_t j=0; j<8; j++) {
					printf("-TIME");
				}
				printf("\r\n");
			}
			break;
		}


		//////////////////////////////////////////////////////////////////////////////////
		default:
			printf("CB_wifi: UNKNOWN (%d) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n", u8MsgType);
			break;
	}

}


uint8_t wifiProcs_getConnectionMode()
{
	return (m_designated_wifi_mode);
}

uint8_t wifiProcs_getStatusWifi()
{
	return (m_status_wifi);
}


//// MODE_CONN_APP  ||  MODE_CONN_ROUTER  ||  MODE_DISCONNECTED
void wifiProcs_setConnectionMode(int m)
{
	switch (m)
	{
	case MODE_DISCONNECT:
		{
			m_designated_wifi_mode = (MODE_DISCONNECT);

			if (WIFI_DISCONNECTED == m_status_wifi) {
				printf("WP_setConnMode()- Already disconnected.\r\n");
				break;
			}
			printf("WP_setConnMode()- m2m_wifi_disconnect() ...\r\n");
			m2m_wifi_disconnect();
			break;
		}


	case MODE_CONN_APP:
		{
			if (MODE_CONN_ROUTER == m_designated_wifi_mode) {
				//////// disconnect from the ROUTER first
				printf("WP_setConnMode()- disconnecting from Router...\r\n");
				m2m_wifi_disconnect();
				printf("WP_setConnMode()- disconnected from Router\r\n");
				//////// this will trigger a callback
			}

			m_status_wifi = WIFI_DISCONNECTED;
			m_designated_wifi_mode = (MODE_CONN_APP);


			//////// generate a new SSID/PSWD
			printf("WP_setConnMode()- setup new ssid/pswd\r\n");
			//////// calculate the new random ssid
			srand(m_Count_main_loop);

			//////// generate new wifi SSID/PASSW using RAND(), after re-seeding above
			get_wifi_settings();
			printf("commissioning_loop()- --%s--%s--\r\n", m_configuration_APP_ssid, m_configuration_APP_pswd);






			//////// Initialize AP mode parameters structure with SSID, channel and OPEN security type.
			memset(& m_structM2MAPConfig, 0x00, sizeof(tstrM2MAPConfig));

			//////// setup the AP mode parameters with SSID/Password/...
			strcpy((char *)(& m_structM2MAPConfig.au8SSID), m_configuration_APP_ssid);
			strcpy((char *)(& m_structM2MAPConfig.au8Key), m_configuration_APP_pswd);

			m_structM2MAPConfig.u8KeySz = strlen(m_configuration_APP_pswd);
			m_structM2MAPConfig.u8KeyIndx = MAIN_WLAN_WEP_KEY_INDEX;
			m_structM2MAPConfig.u8SecType = M2M_WIFI_SEC_WPA_PSK;
			m_structM2MAPConfig.u8ListenChannel = MAIN_WLAN_CHANNEL;

			//////// setup the AP mode parameters with initial IP-# values
			m_structM2MAPConfig.au8DHCPServerIP[0] = (TCPIP_SERVER_IP_0);
			m_structM2MAPConfig.au8DHCPServerIP[1] = (TCPIP_SERVER_IP_1);
			m_structM2MAPConfig.au8DHCPServerIP[2] = (TCPIP_SERVER_IP_2);
			m_structM2MAPConfig.au8DHCPServerIP[3] = (TCPIP_SERVER_IP_3);


			//////// start AP mode
			printf("WP_setConnMode()- starting AP mode\r\n");
			uint8_t ret = m2m_wifi_enable_ap(& m_structM2MAPConfig);
			if (M2M_SUCCESS != ret) {
				printf("WP_setConnMode()- m2m_wifi_enable_ap call error!\r\n");
				printf("WP_setConnMode()- /////////////////////// HALTED\r\n");
				while (1) {
					;////////--infinite-loop
				}
			}

			printf("WP_setConnMode()- Started. (WPA2) SSID == %s\r\n", m_structM2MAPConfig.au8SSID);
			printf("WP_setConnMode()- Started. PASSWORD    == %s\r\n", m_structM2MAPConfig.au8Key);

			m_status_wifi = (WIFI_CONNECTING_APP);
			m_APP_session_concluded = (0);


			break;
		}


	case MODE_CONN_ROUTER:
		{
			uint8_t  res;
			if (MODE_CONN_APP == m_designated_wifi_mode) {
				res = m2m_wifi_disable_ap();
				if (M2M_SUCCESS != res) {
					printf("WP_setConnMode()- m2m_wifi_disable_ap()- Error!\r\n");
					printf("WP_setConnMode()- ////////////////////// HALTED\r\n");
					while (1) {
						;////////--infinite-loop
					}
				}
			}
			else {
				printf("WP_setConnMode()- NOT in A.P. mode\r\n");
			}
			m_designated_wifi_mode = (MODE_CONN_ROUTER);
			m_status_wifi = WIFI_DISCONNECTED;

			printf("WP_setConnMode()- Connecting ROUTER (%s)/%s...\r\n", m_configuration_Router_ssid, m_configuration_Router_pswd);

			//////////////////////////////////////////////////////////////////////////////////////////////
			//////// this will trigger a callback with ip number etc
			//////// if this fails, it causes a DISCONNECTED event in the callback
			m2m_wifi_connect(	m_configuration_Router_ssid,
								strlen(m_configuration_Router_ssid),
								M2M_WIFI_SEC_WPA_PSK,
								m_configuration_Router_pswd,
								M2M_WIFI_CH_ALL			);

			m_status_wifi = (WIFI_CONNECTING_ROUTER);
			//////////////////// at callback, this is NOT updated!!!! FIX THIS

			break;
		}


	default:
		printf("WP_setConnMode()- UNKNOWN (%d) ~~~~\r\n", m);
		break;
	}	
}



void wifiProcs_init()
{
	int8_t  ret;


	//////// Initialize Wifi parameters structure. CLEAR-ZERO
	memset((uint8_t *)(& m_param_Wifi), (0), sizeof(tstrWifiInitParam));

	//////// Initialize Wifi driver with data and status callbacks
	m_param_Wifi.pfAppWifiCb = (wifiProcs_Callback);
	ret = m2m_wifi_init(& m_param_Wifi);
	if (M2M_SUCCESS != ret) {
		printf("wifiProcs_init(): m2m_wifi_init call()- Error!(%d)\r\n", ret);
		printf("wifiProcs_init(): ///////// HALTED\r\n");

		while (1) {
			;////////--infinite-loop
		}
	}
	printf("WifiProcs_init()- m2m_wifi_init()- Succes.\r\n");

	//////// get the MAC address of the Winc
	m2m_wifi_get_mac_address(m_mac_addr);
	sprintf(m_mac_addr_str, "%02X:%02X:%02X:%02X:%02X:%02X",
							m_mac_addr[0], m_mac_addr[1], m_mac_addr[2],
							m_mac_addr[3], m_mac_addr[4], m_mac_addr[5]  );
	printf("WifiProcs_init() wifi-MAC: %s\r\n", m_mac_addr_str );

	wifiProcs_setConnectionMode(MODE_DISCONNECT);

	print_div();
}




void modeRouter_loopSlice()
{
	if (MODE_CONN_ROUTER != wifiProcs_getConnectionMode()) {
		return;
	}
	//////// CIU is in ROUTER-CONNECTION MODE /////////////////////////////////////////

	if (buttonSW0_isTriggered) {
		//////// While in CONN-ROUTER mode, Button-SW0 has been TRIGGERED.
		buttonSW0_isTriggered = (0);
		if (wifiProcs_getStatusWifi() == WIFI_DISCONNECTED) {
			printf("modeRouter_loopSlice()- triggered - (wifi-DISconnected)\r\n");
			printf("modeRouter_loopSlice()- triggered ===> MODE_CONN_APP\r\n");
		} else if (wifiProcs_getStatusWifi() == WIFI_CONNECTED_ROUTER ) {
			printf("modeRouter_loopSlice()- triggered - (wifi-Router-Connected)\r\n");
			printf("modeRouter_loopSlice()- triggered ===> MODE_CONN_APP\r\n");
		} else {
			printf("modeRouter_loopSlice()- triggered - (wifi-unknown)\r\n");
			printf("modeRouter_loopSlice()- triggered ===> MODE_CONN_APP\r\n");
		}
		//////// Button-SW0 triggered, so go to CONNECT-to-APP MODE ////////
		wifiProcs_setConnectionMode(MODE_CONN_APP);
		return;
	}


	if (wifiProcs_getStatusWifi() == WIFI_DISCONNECTED) {
		//////// The router is NOT CONNECTED.

		//////////////////////////////////////////////////- DELAY before trying again
		//////////////////////////////////////////////////////////////////////////////
		if (0 < m_delay_before_wifi_router_connect) {
			-- m_delay_before_wifi_router_connect;
			return;
		}
		m_delay_before_wifi_router_connect = (m_delay_before_wifi_router_connect_MAX);
		//////////////////////////////////////////////////////////////////////////////

		//// the status-wifi will CHANGE from Disconnected to Connecting...
		printf("modeRouter_loopSlice()- WIFI_DISCONNECTED ===> MODE_CONN_ROUTER\r\n");
		wifiProcs_setConnectionMode(MODE_CONN_ROUTER);
		return;
	}
}




void modeAPP_loopSlice()
{
	if (MODE_CONN_APP != wifiProcs_getConnectionMode()) {
		return;
	}
	//////// CIU is in CONNECT-to-APP MODE /////////////////////////////////////////

    uint8_t statusWifi = wifiProcs_getStatusWifi();

	if (WIFI_DISCONNECTED == statusWifi) {
		//////// WIFI = DISCONNECTED
		if (m_APP_session_concluded) {
			m_APP_session_concluded = (0);
			wifiProcs_setConnectionMode(MODE_CONN_ROUTER);
		}
		if (buttonSW0_isTriggered) {
			printf("modeAPP_loopSlice() - trigger ignored.\r\n");
			buttonSW0_isTriggered = (0);
		}
		return;
	}


	/*- --------------THIS-IS-ONLY-FOR-DEBUGGING--------------------------
	if (WIFI_CONNECTED_APP == statusWifi) {
		//////// WIFI = CONNECTED TO APP
		//
		/////////////////////------ BUTTON-TRIGGERS END-OF-SESSION ----
		if (buttonSW0_isTriggered) {
			buttonSW0_isTriggered = (0);
			if (! m_APP_session_concluded) {
				m_APP_session_concluded = (1);
				printf("modeAPP_loopSlice() - SESSION CONCLUDED\r\n");
			}
			else {
				printf("modeAPP_loopSlice() - session concluded ALREADY. trigger-ignored.\r\n");
			}
		}
		return;
	}
	------------------------------------------------------------------- -*/
}






int seconds_since_1970_Jan_01(uint16_t year, uint8_t month, uint8_t day, uint8_t hh, uint8_t mm, uint8_t ss)
{
	int  n_era, n_year_of_era, n_day_of_year, n_day_of_era, n_dayz, n_seconds;

	if (month <= 2)  --year;

	if (year >= 0)  n_era = (year / 400);
	else  n_era = ((year - (400 - 1)) / 400);

	n_year_of_era = (year - (400 * n_era));

///	n_day_of_year = (((153 * (month + (month > 2 ? -3 : 9)) + 2) / 5) + day - 1);
	if (month > 2)
		n_day_of_year = ((((153 * (month - 3)) + 2) / 5) + day - 1);
	else
	   n_day_of_year = ((((153 * (month + 9)) + 2) / 5) + day - 1);

	n_day_of_era = ((365 * n_year_of_era) + (n_year_of_era / 4) - (n_year_of_era / 100) + n_day_of_year);

	n_dayz = ((146097 * n_era) + (n_day_of_era) - 719468);

	n_seconds = (ss) + (60 * mm) + (60 * 60 * hh)  +  (60 * 60 * 24 * n_dayz);

	return n_seconds;
}

/*-
!!!!!!!!!!!!!!!!!!!! THIS DOES NOT WORK !!!!!!!!!!!!
int seconds_since_1970_Jan_01(uint16_t year, uint8_t month, uint8_t day, uint8_t hh, uint8_t mm, uint8_t ss)
{
    int n_day_of_year = (((153 * (month + (month > 2 ? -3 : 9)) + 2) / 5) + day - 1);

	int n_seconds;
	n_seconds  = (int)ss;
	n_seconds += (60 * (int)mm);
	n_seconds += (3600 * (int)hh);
	n_seconds += (86400 * n_day_of_year);
	n_seconds += (31536000 * (year - 70));
	n_seconds += (86400 * ((year - 69) / 4));
	n_seconds -= (86400 * ((year - 1) / 100));
	n_seconds += (86400 * ((299 + year) / 400));

	return (n_seconds);
}
-*/






/////////////-- end

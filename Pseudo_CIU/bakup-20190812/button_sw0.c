////////
//////////////////////////////////[ button_sw0.c ]/////////////////////////////
////////

#include "asf.h"
#include "pseudo_ciu.h"



////////////////////////////////////// these delays are experimentally determined
#define LONG_THRESHHOLD    (900000)
#define SHORT_THRESHHOLD   (100000)
#define ACTIVE_THRESHHOLD  (100000)


static int       m_countActive = (0);
static int       m_countIdle   = (0);
static uint8_t   m_btnState    = (0);


///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////                 //////////////////////////////////
////////////////////////////////   GLOBAL  DATA  //////////////////////////////////
////////////////////////////////                 //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

uint8_t  buttonSW0_isTriggered  = (0);




uint8_t buttonSW0_isActive()
{
	return (m_btnState);
}




void buttonSW0_loopSlice()
{
    if (! m_btnState)
	{
		//////// SW-0 STATE == **OFF** /////////////////////////

		if (ioport_get_pin_level(SW0_PIN) == SW0_ACTIVE)
		{
			//////// SW-0 is "on"
			if ((++m_countActive) >= LONG_THRESHHOLD) {
				//////// ON for the minimum time to transition to ON
				m_btnState = (1);
				buttonSW0_isTriggered = (1);
				printf("BTN: ===>[_ACTIVE_] (ACTIVE)-(ACTIVE)\r\n");
			}
			m_countIdle = (0); //// clear the IDLE count
		}
		else
		{
			//////// SW-0 is "off"
			//////// CLEAR the ON-Counter
			m_countActive = (0);
		}
	}
	else
	{
		//////// SW-0 STATE == **ON** /////////////////////////

		if (ioport_get_pin_level(SW0_PIN) != SW0_ACTIVE)
		{
			//////// SW-0 is "off"
			if ((++m_countIdle) >= SHORT_THRESHHOLD) {
				//////// OFF for the minimum time to transition to OFF
				m_btnState = (0);
				printf("BTN: ===>[_idle_] (idle)-(idle)\r\n");
			}
			m_countActive = (0); //// clear the ACTIVE count
		}
		else
		{
			//////// SW-0 is "on"
			//////// CLEAR the OFF-Counter
			m_countIdle = (0);
		}
	}

}



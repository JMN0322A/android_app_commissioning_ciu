/**
 * \file
 *
 * \brief MAIN configuration.
 *
 * Copyright (c) 2016-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////// Security mode
#define MAIN_WLAN_CHANNEL   (7) //////// < Channel number

#define  SAT_WPA2           (1)
#define  SAT_WPA            (0)
#define  SAT_OPEN           (0)
#define  WPA_G4             (0)
#define  WPA_G37            (0)



#if SAT_WPA2
//////////////////////////////////0........1.........2.........3..
//////////////////////////////////12345678901234567890123456789012
#define MAIN_WLAN_SSID           "SAT_YouCantGuessThisPassword"    //////// AabiWk5qlf9~0TgKJw2-f4&*^@"
#define MAIN_WLAN_AUTH           M2M_WIFI_SEC_WPA_PSK
#define MAIN_WLAN_WPA_KEY        "0000000000"
#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define USE_WPA                  (1)
#endif


#if SAT_WPA
//////////////////////////////////0........1.........2.........3..
//////////////////////////////////12345678901234567890123456789012
#define MAIN_WLAN_SSID           "SAT_YouCantGuessThisPassword"    //////// AabiWk5qlf9~0TgKJw2-f4&*^@"
#define MAIN_WLAN_AUTH           M2M_WIFI_SEC_WEP
#define MAIN_WLAN_WEP_KEY        "0000000000"
#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define USE_WEP                  (1)
#endif


#if SAT_OPEN
//////////////////////////////////0........1.........2.........3..
//////////////////////////////////12345678901234567890123456789012
#define MAIN_WLAN_SSID           "SAT_XxxxxxxxxxxxxxxxxxxxxxxxX"
#define MAIN_WLAN_AUTH           M2M_WIFI_SEC_OPEN
#define USE_WEP                  (0)
#endif


#if WPA_G4
//////////////////////////////////0........1.........2.........3..
//////////////////////////////////12345678901234567890123456789012
#define MAIN_WLAN_SSID           "GOOGLE_FOUR"
#define MAIN_WLAN_AUTH           M2M_WIFI_SEC_WEP
#define MAIN_WLAN_WEP_KEY        "0000000000"
#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define USE_WEP                  (1)
#endif


#if WPA_G37
//////////////////////////////////0........1.........2.........3..
//////////////////////////////////12345678901234567890123456789012
#define MAIN_WLAN_SSID           "GOOGLE_379009"
#define MAIN_WLAN_AUTH           M2M_WIFI_SEC_WEP
#define MAIN_WLAN_WEP_KEY        "0000000000"
#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define USE_WEP                  (1)
#endif



////////////////////////////////////////0........1.........2.........3..
////////////////////////////////////////12345678901234567890123456789012
#define MAIN_WLAN_SSID_OPEN_ALTERNATE  "SAT_YyyyyyyyyyyyyyyyyyyyyyyyyY"
#ifndef USE_WEP
#define  USE_WEP  (0)
#endif


#ifdef __cplusplus
}
#endif

#endif /* MAIN_H_INCLUDED */

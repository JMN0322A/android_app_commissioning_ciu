///////////////////////////////////////////////////////////////////
////////
//////// atsam55g_winc1500_main.c
////////
////////


#include "asf.h"
#include "driver/include/m2m_wifi.h"
#include "driver/source/nmasic.h"
#include "socket/include/socket.h"
#include <string.h>


#define MAIN_WLAN_WEP_KEY_INDEX  (1)
#define MAIN_WLAN_CHANNEL        (6)

#define STRING_EOL    "\r\n"
#define STRING_HEADER STRING_EOL \
                      "-- ============================================== --"STRING_EOL	\
                      "-- Novosel SAMG55-Xplained + WINC1500 Development --"STRING_EOL	\
                      "-- "BOARD_NAME " --"STRING_EOL	\
	                  "-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

extern void brascii64b_encode( unsigned char *input_ptr, int input_len, char *pOutput );
extern int  brascii64b_decode( char *input_ptr, int input_len, unsigned char *pOutput );
extern char brascii64b_bin_to_char( unsigned char x );
extern unsigned char brascii64b_char_to_bin( char x );
extern void generate_ssid();
extern void get_wifi_settings();
extern void test_decode_password(char *ssid);
extern uint32_t wait_for_button_press();
extern void looper_cycle_button();
extern void looper_tcp_server();
//extern void proc_recv(uint8_t *);



#define         m_random_128bits_LEN  (16)
static uint32_t m_random_128bits[ m_random_128bits_LEN ];

static char     m_strSSID[32] = "SAT_";
static char     m_strPSWD[32] = "0000000000";
static uint8_t  m_mac_addr[ M2M_MAC_ADDRES_LEN ];

static tstrM2MAPConfig  m_strucM2MAPConfig;

static bool     m_state_connected = false;

#define         m_configuration_string_SIZE   (80)
static char     m_configuration_string[m_configuration_string_SIZE] = "This is extremely meow";



//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////-Debug----override--ssid--passw-////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
static bool m_bFlagDebug = false;    /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////







static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	//////// Configure UART console.
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void callback_wifi(uint8_t u8MsgType, void *pvMsg)
{
	switch (u8MsgType)
	{
		case M2M_WIFI_RESP_CON_STATE_CHANGED:
		{
			tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
			if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED) {
				printf("State m2m_wifi_Connecting...\r\n");
			} else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED) {
				printf("State m2m_wifi_DISconnected\r\n");
			}
			m_state_connected = false;

			break;
		}

		case M2M_WIFI_REQ_DHCP_CONF:
		{
			uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
			printf("Station connected\r\n");
			printf("Station IP is %u.%u.%u.%u\r\n",
					pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]);
			m_state_connected = true;
			break;
		}

		default:
		{
			break;
		}
	}
}








int main(void)
{
	int16_t sw0_count;
	int8_t  sw0_pressed;
	const int16_t sw0_threshhold = 30000;


	//////// Initialize the board.
	sysclk_init();
	board_init();

	//////// Initialize the UART console.
	configure_console();
	printf(STRING_HEADER);
    printf("@main() - - - -\r\n");

	//////// Initialize the BSP.
	nm_bsp_init();

	//////// Initialize Wi-Fi driver with data and status callbacks.
    tstrWifiInitParam  param;
	memset((uint8_t *)(& param), 0, sizeof(tstrWifiInitParam));
	param.pfAppWifiCb = (callback_wifi);
	uint8_t ret = m2m_wifi_init(& param);
	if (M2M_SUCCESS != ret) {
		printf("@main()- *ERROR-  m2m_wifi_init() - - - - (Error: %d)\r\n", ret);
		while (1) {
		} //// forever loop HERE
	}


    //////// get the MAC address of the Winc
    printf("Obtaining MAC address from Winc...\r\n");
    m2m_wifi_get_mac_address(m_mac_addr);
	////
    printf("Winc MAC-Addr: %02X:%02X:%02X:%02X:%02X:%02X\r\n",
            m_mac_addr[0], m_mac_addr[1], m_mac_addr[2],
            m_mac_addr[3], m_mac_addr[4], m_mac_addr[5]  );


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////-Debug----override--ssid--passw-////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if (ioport_get_pin_level(SW0_PIN) == SW0_ACTIVE) { ///////////////////////////////////////
  printf("****DEBUG-AP-MODE****\r\n");             ///////////////////////////////////////
  m_bFlagDebug = true;  }                          ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

    ////////--////////////////////// WAIT FOR THE BUTTON /////////////
    ////////--////////////////////// WAIT FOR THE BUTTON /////////////
    ////////--////////////////////// WAIT FOR THE BUTTON /////////////
    ////////--////////////////////// WAIT FOR THE BUTTON /////////////
    int32_t nSeed = (int32_t) wait_for_button_press();
	printf("m_longPress_count == %ld.\r\n", nSeed);

	//////// seed the random generator with the count
	srand(nSeed);
    //
    printf("rand()'s: ");
    for (int i=0; i<6; i++) {
		printf("%08x ", (rand()));
	}
	printf("\r\n");




	//////// Initialize AP mode parameters structure with SSID, channel and OPEN security type.
	memset(&m_strucM2MAPConfig, 0x00, sizeof(tstrM2MAPConfig));

    //////// generate new wifi SSID/PASSW using the random value above
	get_wifi_settings();
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////-Debug----override--ssid--passw-////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if (m_bFlagDebug) {                                   ////////////////////////////////////
 strcpy(m_strSSID, "SAT_XxxxxxxxxxZzzzzzzzzz00");     ////////////////////////////////////
 strcpy(m_strPSWD, "0000000000");   }                 ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

    //////// setup the AP mode parameters with SSID/Password/...
	strcpy((char *)&m_strucM2MAPConfig.au8SSID, m_strSSID);
	strcpy((char *)&m_strucM2MAPConfig.au8Key, m_strPSWD);
	m_strucM2MAPConfig.u8KeySz = strlen(m_strPSWD);
	m_strucM2MAPConfig.u8KeyIndx = MAIN_WLAN_WEP_KEY_INDEX;
	m_strucM2MAPConfig.u8SecType = M2M_WIFI_SEC_WPA_PSK;
	m_strucM2MAPConfig.u8ListenChannel = MAIN_WLAN_CHANNEL;

    //////// setup the AP mode parameters with initial IP-# values
	m_strucM2MAPConfig.au8DHCPServerIP[0] = 172; //172; //192;
	m_strucM2MAPConfig.au8DHCPServerIP[1] =  29; //16;  //168;
	m_strucM2MAPConfig.au8DHCPServerIP[2] = 188; //188; //1;
	m_strucM2MAPConfig.au8DHCPServerIP[3] =   1; //1;   //1;


	//////// start AP mode
	printf("@main()- starting AP mode\r\n");
	ret = m2m_wifi_enable_ap(&m_strucM2MAPConfig);
	if (M2M_SUCCESS != ret) {
		printf("main: m2m_wifi_enable_ap call error!\r\n");
		while (1) {
		}
	}

	printf("AP mode started. (WPA2) SSID == %s\r\n", m_strucM2MAPConfig.au8SSID);
	printf("AP mode started. PASSWORD    == %s\r\n", m_strucM2MAPConfig.au8Key);


    sw0_count = (0);
	while (1) {
		//////// Handle pending events from network controller.
		while (m2m_wifi_handle_events(NULL) != M2M_SUCCESS)
		{
			; ////
		}

		looper_cycle_button();

		looper_tcp_server();
	}

	return 0;
}



void looper_cycle_button()
{
#define  LOOP_CYCLE_THRESH  (1000000)
	static int32_t nCount = (0);
	static bool bActive = false;

	if (ioport_get_pin_level(SW0_PIN) == SW0_ACTIVE)
	{
		if (! bActive) { //// ON while INACTIVE
			++nCount;
			if (nCount >= LOOP_CYCLE_THRESH) {
				printf("LONG PRESS DETECTED\r\n"); ////-----------------------------------------
				nCount = (LOOP_CYCLE_THRESH / 3);
				bActive = true;
			}
		}
		else { //// else ON while ACTIVE
			nCount = (LOOP_CYCLE_THRESH / 3);
		}
	}
	else //// otherwise the button is IDLE
	{
		if (bActive) { //// OFF WHILE ACTIVE
			--nCount;
			if (nCount <= 0) {
				printf("BUTTON RELEASED.\r\n"); ////-----------------------------------------
				nCount = (0);
				bActive = false;
			}
		}
		else { //// else OFF while INACTIVE
			nCount = (0);
		}
	}
#undef  LOOP_CYCLE_THRESH
}




uint32_t wait_for_button_press()
{
#define LONG_THRESHHOLD    (9900000)
#define ACTIVE_THRESHHOLD  (1000000)
	int nCyclesTmp= LONG_THRESHHOLD;
	int nCountActive = 0;
	uint32_t longCount = (0);

    //////// loop while button IDLE //////////////////////
    while (1) {
	    if (ioport_get_pin_level(SW0_PIN) != SW0_ACTIVE )
		{
			if (++nCyclesTmp >= LONG_THRESHHOLD) {
				nCyclesTmp = (0);
             	printf("@wait_for_button_press() ..(idle)\r\n");
			}
			++ longCount;
			nCountActive = (0);
		}
		else {
			if( (++nCountActive) >= ACTIVE_THRESHHOLD) {
				nCountActive = (0);
                printf("@wait_for_button_press() *-ACTIVE-ACTIVE-ACTIVE-ACTIVE-*\r\n");
				break;
			}
		}
	}

    //////// loop while button ACTIVE //////////////////
    while (1) {
	    if (ioport_get_pin_level(SW0_PIN) == SW0_ACTIVE )
	    {
		    if (++nCyclesTmp >= LONG_THRESHHOLD) {
			    nCyclesTmp = (0);
			    printf("@wait_for_button_press() ..(active)\r\n");
		    }
		    ++ longCount;
			nCountActive = (0);
	    }
	    else {
		    if( (++nCountActive) >= ACTIVE_THRESHHOLD) {
			    printf("@wait_for_button_press() RECOGNIZED\r\n");
			    break;
		    }
	    }
    }

	printf("@wait_for_button_press() -- RETURN >>> %ld.\r\n", longCount);
	return (longCount);

#undef  ACTIVE_THRESHHOLD
#undef  LONG_THRESHHOLD
}













void generate_ssid_R128bits(uint8_t *ptr)
{
    brascii64b_encode(ptr, 16, m_strSSID + 4);
	m_strSSID[26] = ('\0');	
    printf("******ssid==%s ********\r\n", m_strSSID);
}


























////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////// K-Key  length = 26-bytes  (208-bits)                                   ////////
//////// Here, it is a 32-byte sequence of random-ish bytes                     ////////
//////// ***** THIS ARRAY IS SHARED WITH THE MOBILE APP ********                ////////
//////// *****                                                                  ////////
//////// ***** DO * NOT * EDIT * THIS ********                                  ////////
//////// ***** DO * NOT * EDIT * THIS ********                                  ////////
//////// ***** DO * NOT * EDIT * THIS ********                                  ////////
////////                                                                        ////////
uint8_t *get_kkey_26B_208b()                                                    ////////
{                                                                               ////////
	static uint8_t x[] =  { 0x1f, 0x68, 0x48, 0xcc,   0x59, 0xcf, 0x8a, 0x9c,   ////////
					        0x4d, 0xe7, 0xc4, 0xf2,   0x0d, 0x5a, 0xa0, 0x93,   ////////
							0x3d, 0xd0, 0x56, 0x8f,   0x6c, 0x06, 0xd3, 0x37,   ////////
							0x0d, 0x6b, 0x50, 0x91,   0x35, 0x9b, 0xbd, 0xfd }; ////////
                                                                                ////////
    return &(x[0]);	                                                            ////////
}                                                                               ////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////




//////// - MAC-ADDRESS  length = 6-bytes (48-bits)
uint8_t *get_mac_6B_48b()
{
    return &(m_mac_addr[0]);
}



//////// - Combined to produce DECODE-ABLE ... length = 16-bytes (128-bits)
uint8_t *combineXORY_16B_128b( uint8_t* rand_128b, uint8_t* kkey_208b, uint8_t* mac_48b )
{
#define SIZEOF_RNDX (16)
#define SIZEOF_MACX (6)
	static uint8_t x[SIZEOF_RNDX];

	for (int i=(0); i<SIZEOF_MACX; i++)
	{
		x[i] = (rand_128b[i] ^ mac_48b[i]);
	}
	for (int i=(SIZEOF_MACX), j=(0);  i<SIZEOF_RNDX;  ++i,++j)
	{
		x[i] = (rand_128b[i] ^ kkey_208b[j]);
	}
	printf("Combined--");
	for (int i=0; i<16; i++) {
		printf("-%02x", x[i]);
	}
	printf("\r\n");
	return x;

#undef SIZEOF_MACX
#undef SIZEOF_RNDX
}



//////// - produces a 16-byte random number, which determines the wifi PASSWORD
uint8_t *create_random_16B_128b()
{
#define SIZEOF_RNDX (16)
    static uint8_t x[SIZEOF_RNDX];

	for (int i=0; i<SIZEOF_RNDX; ++i) {
		x[i] = (uint8_t)(rand() & 0x0ff);
	}

    printf("Rand128 ==> ");
	for (int i=0; i<SIZEOF_RNDX; i+=4) {
		for (int j=0; j<4; j++) {
			printf((0==j)?("%02x"):("-%02x"), x[i+j]);
		}
		if (i < (SIZEOF_RNDX - 4)) printf("~~~");
	}
	printf("\r\n");

	return x;
	
#undef SIZEOF_RNDX
}



char *get_password(uint8_t *pRand16bytes)
{
#define SIZEOF_PASSW (28)
	static char x[SIZEOF_PASSW];

	brascii64b_encode(pRand16bytes, 16,  x);
	x[22] = ('\0');
	printf("Wifi-Password==> %s\r\n", (x));

	return x;

#undef SIZEOF_PASSW
}



char *get_ssid(uint8_t *pRndKkeyMac)
{
#define SSID_PREFIX_LENGTH (4)
#define SSID_PREFIX        ("SAT_")
#define SIZEOF_SSID        (28)
	static char x[SIZEOF_SSID];

	strcpy(x, SSID_PREFIX);
	brascii64b_encode(pRndKkeyMac, 16,  (x + SSID_PREFIX_LENGTH));
	x[26] = ('\0');
	printf("Wifi-SSID==> %s\r\n", (x));

	return x;

#undef SIZEOF_SSID
#undef SSID_PREFIX
#undef SSID_PREFIX_LENGTH
}






void get_wifi_settings()
{
	uint8_t *ptr_rnd;
	uint8_t *ptr_kkey;
	uint8_t *ptr_mac;
	uint8_t *ptr_comb;
	char    *ptr_pswd;
	char    *ptr_ssid;

    for(;;) {
		ptr_rnd  = create_random_16B_128b();
		ptr_kkey = get_kkey_26B_208b();
		ptr_mac  = get_mac_6B_48b();

		ptr_pswd = get_password(ptr_rnd);

		ptr_comb  = combineXORY_16B_128b(ptr_rnd, ptr_kkey, ptr_mac);
		ptr_ssid  = get_ssid(ptr_comb);

        //////// ensure the first random-character is a capitol-Letter
		if ((('A') <= ptr_ssid[4]) && (ptr_ssid[4] <= ('Z'))) {
			break;
		}
		printf("--------again\r\n");
 	}

	strcpy(m_strSSID, ptr_ssid);
	strcpy(m_strPSWD, ptr_pswd);
}








char brascii64b_bin_to_char( unsigned char x )
{
	char r;

	if( x < (26) )  {
		r = ((x) + ('A'));
	}
	else
	if( x < (26+26) )  {
		r = ((x - 26) + ('a'));
	}
	else
	if( x < (26+26+10) )  {
		r = ((x - 26 - 26) + ('0'));
	}
	else
	if( (0x3e) == x )  {
		r = ('-');
	}
	else
	if( (0x3f) == x )  {
		r = ('~');
	}
	else  {
		r =('\xff');
	}

	return (r);
}


void brascii64b_encode( unsigned char *input_ptr, int input_len, char *pOutput )
{
	unsigned char bin[4];
	unsigned char num[4];
	char          b64[4];
	unsigned char xByte;
	int i, len;

	for(;;)
	{
		//// prepare
		b64[0] = b64[1] = b64[2] = b64[3] = ('\0');
		bin[0] = bin[1] = bin[2]          = (unsigned char)(0);
		num[0] = num[1] = num[2]          = (unsigned char)(0);
		len = 0;

		//// process the next three input bytes
		for( i=0; i<3; i++ )
		{
			//// if no more, stop
			if( input_len <= 0)
			break;

			//// get the next data byte
			xByte = (int)(*input_ptr++);
			--input_len;

			//// save it into array of three bytes
			bin[i] = xByte;
			//// count it
			++len;
		}
		if ((0)==len)
		return;

		//// process into four values(0-63)
		num[0] = ((bin[0] >> 2) & 0x3f);
		num[1] = ((bin[0] << 4) & 0x30) | ((bin[1] >> 4) & 0x0f);
		num[2] = ((bin[1] << 2) & 0x3c) | ((bin[2] >> 6) & 0x03);
		num[3] = ((bin[2]     ) & 0x3f);

		//// translate four values into characters
		b64[0] = brascii64b_bin_to_char( num[0] );
		b64[1] = brascii64b_bin_to_char( num[1] );
		b64[2] = brascii64b_bin_to_char( num[2] );
		b64[3] = brascii64b_bin_to_char( num[3] );

		//// output it
		if( len == 1 )  {
			*pOutput++ = b64[0];
			*pOutput++ = b64[1];
			*pOutput++ = ('_');
			*pOutput++ = ('_');
		}
		else
		if( len == 2 )  {
			*pOutput++ = b64[0];
			*pOutput++ = b64[1];
			*pOutput++ = b64[2];
			*pOutput++ = ('_');
		}
		else
		if( len == 3 )  {
			*pOutput++ = b64[0];
			*pOutput++ = b64[1];
			*pOutput++ = b64[2];
			*pOutput++ = b64[3];
		}

	}
	*pOutput = ('\0');
}


unsigned char brascii64b_char_to_bin( char x )
{
	unsigned char r;

	if( ('A') <= x  &&  x <= 'Z' )  {
		r = x - ('A');
	}
	else
	if( ('a') <= x  &&  x <= 'z' )  {
		r  = x - ('a');
		r += (26);
	}
	else
	if( ('0') <= x  &&  x <= '9' )  {
		r = x - ('0');
		r += (26+26);
	}
	else
	if( ('-') == x )  {
		r = (0x3e);
	}
	else
	if( ('~') == x )  {
		r = (0x3f);
	}
	else
	if( ('_') == x )  {
		r = (0x00);
	}
	else  {
		r = (0x7f);
	}
	return (r);
}


int brascii64b_decode( char *input_ptr, int input_len, unsigned char *pOutput )
{
	char b64[4];
	unsigned char bin[4];
	char x,xbin;
	int i, len, nCount;

	nCount = (0);
	while( input_len > 0 )
	{
		//// prepare
		b64[0] = b64[1] = b64[2] = b64[3] =          (char)(0);
		bin[0] = bin[1] = bin[2]          = (unsigned char)(0);
		len = 0;

		//// process the next four input characters
		for( i=0; i<4; i++ )
		{
			//// if no more, stop
			if( input_len <= 0 )
			break;

			//// get the next char
			x = *input_ptr++;
			--input_len;
			//// ignore white space
			//// printf( "%c", x );
			if( (' ')==x || ('\r')==x || ('\n')==x )
			break;
			//// if padding-space, stop
			if( ('=') == x )
			break;

			//// convert ascii-char to the binary 0..63
			xbin = brascii64b_char_to_bin( x );
			//// if error, stop
			if( xbin & 0xc0 )  {
				printf( "*=(%02x(%02x))=*\n", xbin, x );
				i=999;
				break;
			}
			////////printf("*b64_decode(): \'%c\' : %02x => %02x\n", x, x, xbin);

			//// save the binary-ified binary 0..63
			b64[i] = (xbin);
			//// count the piece
			++len;
		}
		if (i>=999)
		break;

		//// process into three binary data bytes
		bin[0] = ((b64[0] << 2) & 0xfc) | ((b64[1] >> 4) & 0x03);
		bin[1] = ((b64[1] << 4) & 0xf0) | ((b64[2] >> 2) & 0x0f);
		bin[2] = ((b64[2] << 6) & 0xc0) | ((b64[3])      & 0x3f);

		////      --> three Bytes  len==4
		////   =  --> two   Bytes  len==3
		////  ==  --> one   Byte   len==2
		if( len >= 2 )  {
			*pOutput++ = bin[0];
			++nCount;
			if( len >= 3 )  {
				*pOutput++ = bin[1];
				++nCount;
				if( len >= 4 )  {
					*pOutput++ = bin[2];
					++nCount;
				}
			}
		}


	}

	return nCount;
}





static struct sockaddr_in m_addr;
static SOCKET  tcp_server_socket = -1;
static SOCKET  tcp_client_socket = -1;
uint8_t rxBuff[1460];
char m_tmp[512];

static void socket_callback(SOCKET sock,  uint8_t u8Msg,  void *pvMsg)
{
	switch (u8Msg)
	{
	case SOCKET_MSG_BIND:
	{
		tstrSocketBindMsg *pstrBind = (tstrSocketBindMsg *)pvMsg;
		if (pstrBind && pstrBind->status == 0) {
			printf("socketCB()- Bind success. => next: Listen-()\r\n");
			listen(tcp_server_socket, 1);
		} else {
			printf("socketCB()- Bind() ERROR!\r\n");
			tcp_server_socket = -1;
		}
	}
	break;

	case SOCKET_MSG_LISTEN:
	{
		tstrSocketBindMsg *pstrListen = (tstrSocketListenMsg *)pvMsg;
		if (pstrListen && pstrListen->status == 0) {
			printf("socketCB()- Listen success. => next: Accept-()\r\n");
			accept(tcp_server_socket, NULL, NULL);
		} else {
			printf("socketCB()- Listen() ERROR!\r\n");
			close(tcp_server_socket);
			tcp_server_socket = -1;
		}
	}
	break;

	case SOCKET_MSG_ACCEPT:
	{
		tstrSocketAcceptMsg *pstrAccept = (tstrSocketAcceptMsg *)pvMsg;
		if (pstrAccept) {
			printf("socketCB()- Accept success. => next: recv-()\r\n");
			accept(tcp_server_socket, NULL, NULL);
			tcp_client_socket = pstrAccept->sock;
			recv(tcp_client_socket, rxBuff, sizeof(rxBuff), 0);
		} else {
			printf("socketCB()- Accept() ERROR!\r\n");
			close(tcp_server_socket);
			tcp_server_socket = -1;
		}
	}
	break;

	case SOCKET_MSG_SEND:
	{
		extern void setup_tcpServer();
		printf("socketCB()- send() success.\r\n");
		printf("socketCB()- End.\r\n");
		printf("socketCB()- close socket\r\n");
		close(tcp_server_socket);
		close(tcp_client_socket);
		tcp_client_socket = -1;
		tcp_server_socket = -1;
		//
		printf("socketCB()- closed. => next: Setup-tcpServer-()\r\n");
		setup_tcpServer();
	}
	break;

	case SOCKET_MSG_RECV:
	{
		tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
		int16_t  len, i,j;
		char *pch;
		if (pstrRecv && pstrRecv->s16BufferSize > 0) {
			pch = pstrRecv->pu8Buffer;
			len = pstrRecv->s16BufferSize;
			pch[len] = ('\0');
			//- -----------------------------------------------------------------
			if (('$')==pch[0]) {
					printf("socketCB()- recv-[%d]-\"%s\" ********\r\n", len, pch);
			    	for (i=0, j=1;  j <= len;  ++j, ++i)
				    m_configuration_string[i] = pch[j];
				//strncpy( m_configuration_string, pstrRecv->pu8Buffer + 1, pstrRecv->s16BufferSize - 1);
				strcpy( m_tmp, "OK--");
			}
			else if (('?'==pch[0]) && ('Q'==pch[1]) && ('u'==pch[2])) {
				printf("socketCB()- recv-[%d]-\"%s\" ********\r\n", len, pch);
				strcpy( m_tmp, "OK--");
			}
			else {
                extern void proc_recv(SOCKET sock, tstrSocketRecvMsg *pstrRecv);
			    proc_recv(sock, pstrRecv);
				break;
			}

			strcat(m_tmp, m_configuration_string);
			////////////////// IMPORTANT!!!! EOL-Termination MUST be included here.
			strcat(m_tmp, "\r\n");
		    printf("socketCB()-TX: %s\r\n", m_tmp);
			send(tcp_client_socket, m_tmp, (strlen(m_tmp)), 0);
			//----------------------------------------------------------------- -*-/
			
		} else {
			printf("socketCB()- Recv() ERROR!\r\n");
			close(tcp_client_socket);
			tcp_client_socket = -1;
			close(tcp_server_socket);
			tcp_server_socket = -1;
		}
	}
	break;

	default:
		printf("socketCB()- unknown case %d\r\n", u8Msg);
		break;
	}

}

void proc_recv(SOCKET sock, tstrSocketRecvMsg *pstrRecv)
{
	int16_t  len, i,j;
	char *pch;
	
	
	pch = pstrRecv->pu8Buffer;
	len = pstrRecv->s16BufferSize;
	pch[len] = ('\0');
	//- -----------------------------------------------------------------
	printf("proc_recv()-RX(%d-bytes)-", len);
	for (int i=0; i<len; i++) {
		printf((0==i)?("%02x"):("-%02x"), (uint8_t)pch[i] );
	}
//	for(int i=0; i<4; i++) {
//		printf("proc_recv()-[%d]-RX-[%d]-\"%s\"\r\n", sock, len, pch);
//	}

    uint8_t dlen = pch[1];
	uint16_t csum = (0);
	for (int i=0; i<dlen; i++) {
		csum += (uint16_t)(pch[2 + i]);
	}
	csum %= 0xfe;
	csum ^= 0xe5;
	printf("---ck(%02x)\r\n", (uint8_t)csum);




	if (('$')==pch[0]) {
		for (i=0, j=1;  j <= len;  ++j, ++i)
		m_configuration_string[i] = pch[j];
		//strncpy( m_configuration_string, pstrRecv->pu8Buffer + 1, pstrRecv->s16BufferSize - 1);
		strcpy( m_tmp, "OK--");
	}
	else if (('?')==pch[0]) {
		strcpy( m_tmp, "OK--");
	}
	else {
		strcpy( m_tmp, "no--");
	}
	strcat(m_tmp, m_configuration_string);
	////////////////// IMPORTANT!!!! EOL-Termination MUST be included here.
	strcat(m_tmp, "\r\n");
	printf("proc_recv()-[%d]-TX: %s\r\n", tcp_client_socket, m_tmp);
	send(tcp_client_socket, m_tmp, (strlen(m_tmp)), 0);
}



/*-
void proc_recv(uint8_t * pData)
{
	//////// print to console the data which was just received
	for(int i=0; i<10; i++) {
		printf("socketCB()- recv(): \"%s\"\r\n", pData);
	}

    //////// branch based on the operation commanded in the data
	switch (pData[0]) {
		//////// Inquiry command. Send back the current value for DATA
		case ('?'):
		    strcpy(m_tmp, "*OK---");
		    strcat(m_tmp, m_configuration_string);
		    strcat(m_tmp, "\r\n");
			break;
		
		//////// Save command. Save and send back the new value for DATA
		case ('$'):
		    strcpy( m_configuration_string, pData + 1);
		    strcpy(m_tmp, ":OK---");
		    strcat(m_tmp, m_configuration_string);
		    strcat(m_tmp, "\r\n");
		    break;

		//////// unrecognized default response
		default:
		    strcpy(m_tmp, "?NOP--");
			strcat(m_tmp, pData);
			strcat(m_tmp, "\r\n");
			break;
	}

    //////// send the response
	printf("socketCB()- TX: %s\r\n", m_tmp);
	send(tcp_client_socket, m_tmp, (strlen(m_tmp)), 0);
}
-*/


/////////////////////////---172.29.188.1---////////////////////////////
void setup_tcpServer()
{
	static bool bInit = false;
	m_addr.sin_family      = AF_INET;
	m_addr.sin_port        = _htons(7265);
	m_addr.sin_addr.s_addr = _htonl((172 << 24)|(29 << 16)|(188 << 8)|(1));

   if (! bInit) {
		socketInit();
		registerSocketCallback(socket_callback, NULL);
		bInit = true;
    }


    if (tcp_server_socket < 0) {
	    if ((tcp_server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			printf("setup_tcpServer()- ERROR - create tcp server socket\r\n");
		}
		printf("setup_tcpServer()- create socket (%d) => next: Bind-()\r\n", (int)(tcp_server_socket) );
	} else {
		printf("setup_tcpServer()- reusing socket (%d) => next: Bind-()\r\n", (int)(tcp_server_socket) );
	}

    bind(tcp_server_socket, (struct sockaddr *)(& m_addr), sizeof(struct sockaddr_in));
}



void looper_tcp_server()
{
	static bool bConnected = true;
	if (m_state_connected) {
		if (! bConnected) {
			bConnected = true;
			printf("looper_server()- Connected\r\n");
			setup_tcpServer();
		}
	} else {
		if (bConnected) {
			bConnected = false;
			printf("looper_server()- DIS_connected\r\n");
		}
	}
	
}


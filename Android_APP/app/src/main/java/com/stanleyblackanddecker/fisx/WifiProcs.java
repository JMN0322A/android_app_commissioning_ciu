package com.stanleyblackanddecker.fisx;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import java.util.List;

public class WifiProcs {


    public  final static String SSID_PREFIX_MARKER = "SAT_";
    private final static String TAG = "{WifiProcs}";

    private AppServiceData mData;
    private Activity mActivity;
    private ZDialog mZDialog;
    private WifiManager mWifiManager = null;


    WifiProcs(AppServiceData data, Activity a, ZDialog d) {
        mData = data;
        mActivity = a;
        mZDialog = d;
        Log.i(TAG, "Constructor - setting App Context");
    }

    public void set_ZDialog(ZDialog d) {
        mZDialog = d;
    }
    public void set_Activity(Activity a) {
        mActivity = a;
    }


    public final static int LOCA_PERMI_REQUEST_CODE_IGNORED = (1001);

    /*-
    //////////////////// these had to be moved to CONTAINER-ACTIVITY //////////////////////////////
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG,"TURN ON SYSTEM LOCATIONS  RESULT IGNORED");
    }

    @Override
    public void onRequestPermissionsResult( int nRequestCode, String[] permissions, int [] granted) {
        if (LOCA_PERMI_REQUEST_CODE_IGNORED == nRequestCode) {
            Log.i(TAG,"LOCATION PERMISSIONS CHANGED  RESULT IGNORED");
        }
    }
    -*/

    private boolean isLocationsRequired() {
        //// true if Android version is Marshmallow or above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return true;
        }
        //// otherwise, this screen of settings can be excluded from app-flow.
        return false;
    }

    private boolean isEnabledSystemLocations() {
        final LocationManager manager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        }
        return false;
    }

    private void systemRequest_locations() {
        mActivity.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCA_PERMI_REQUEST_CODE_IGNORED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isEnabledLocationsPermission() {
        if (mActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void systemRequestPermission_locations() {
        mActivity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCA_PERMI_REQUEST_CODE_IGNORED);
    }

    public boolean isReadySystemDependencies() {
        Log.i(TAG, "isReadySystemDependencies()- ***************************************");
        Log.i(TAG, "isReadySystemDependencies()- ****************************************");
        Log.i(TAG, "isReadySystemDependencies()- *****************************************");
        Log.i(TAG, "isReadySystemDependencies()- ******************************************");

        if (! ensureWifiEnabled()) {
            return false;
        }

        if (! isLocationsRequired()) {
            Log.i(TAG, "isReadySystemDependencies()- Locations NOT required");
            return true;
        }
        Log.i(TAG, "isReadySystemDependencies()- Locations Permission is REQUIRED");

        if (! isEnabledLocationsPermission()) {
            Log.i(TAG, "isReadySystemDependencies()- Locations Permission is NOT enabled");
            mZDialog.setTitle("NOT READY")
                    .setMessage("Location Permissions are required for this APP.\n\nEnable Permissions?")
                    .setModal(true)
                    .setStrCancel("NO")
                    .setStrOk("YES")
                    .setDialogOkListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            systemRequestPermission_locations();
                        }
                    })
                    .show();

            return false;
        } else {
            Log.i(TAG, "isReadySystemDependencies()- Locations Permission is ENABLED");
        }

        if (! isEnabledSystemLocations()) {
            Log.i(TAG, "isReadySystemDependencies()- System Locations is NOT enabled");
            mZDialog.setTitle("NOT READY")
                    .setMessage("System Location is turned off.\n\nEnable Locations?")
                    .setModal(true)
                    .setStrCancel("NO")
                    .setStrOk("YES")
                    .setDialogOkListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            systemRequest_locations();
                        }
                    })
                    .show();

            return false;
        } else {
            Log.i(TAG, "isReadySystemDependencies()- System Locations is ENABLED");
        }

        return true;
    }


    public boolean ensureWifiEnabled() {
        initialize_wifiManager();
        if (null == mWifiManager) {
            Log.i(TAG, "ensureWifiEnabled() - ERROR ==== NO Wifi-Manager ========");
            return false;
        }
        if (!mWifiManager.isWifiEnabled()) {
            Log.i(TAG, "ensureWifiEnabled() - Wifi NOT enabled");
            mWifiManager.setWifiEnabled(true);
            Log.i(TAG, "ensureWifiEnabled() - Wifi set to enabled");
        } else {
            Log.i(TAG, "ensureWifiEnabled() - Wifi IS Enabled");
        }
        return true;
    }


//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////


    private boolean mRegisteredWifiRx = false;
    private WifiReceiver mWifiBcstReceiver;
    private List<ScanResult> mWifiList;
    private final int WIFI_ITEMS_MAX = 256;
    private WifiScanItem[] mWifiItems = new WifiScanItem[WIFI_ITEMS_MAX];
    private int mWifiItemsCount = 0;
    private int mNdxFoundWifi;


    public boolean initialize_wifiManager() {
        if (null != mWifiManager) {
            Log.i(TAG, "initialize_wifiManager() - Already initialized");
            return true;
        }
        if (null == mActivity) {
            Log.i(TAG, "initialize_wifiManager() - NOT initialized, and NULL ACTIVITY =========");
            return false;
        }

        Context ctx = (Context) mActivity;
        mWifiManager = (WifiManager) ctx
                .getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        Log.i(TAG, "initalize_wifiManager() - wifi-manager == " + mWifiManager.toString());

        return (null != mWifiManager);
    }


    public void registerWifiRx() {
        initialize_wifiManager();
        if (null == mWifiManager) {
            Log.i(TAG, "registerWifiRx() - ERROR ==== NO Wifi-Manager ========");
            return;
        }

        unRegisterWifiRx();
        ////
        mWifiBcstReceiver = new WifiReceiver();
        if ((null != mWifiBcstReceiver) && (null != mActivity)) {
            mActivity.registerReceiver(mWifiBcstReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            mRegisteredWifiRx = true;
            Log.i(TAG, "registerWifiRx() - Registered with Android");
        }
    }

    public void unRegisterWifiRx() {
        if (mRegisteredWifiRx) {
            mRegisteredWifiRx = false;
            Log.i(TAG, "unRegisterWifiRx() - Flag was TRUE");
            if ((null != mWifiBcstReceiver) && (null != mActivity)) {
                mActivity.unregisterReceiver(mWifiBcstReceiver);
                mWifiBcstReceiver = null;
                Log.i(TAG, "unRegisterWifiRx() - UN-registered from Android");
            }
        } else {
            Log.i(TAG, "unRegisterWifiRx() - Flag was FALSE ... *NO* Un-Register");
        }
    }

    public boolean startScan() {
        mNdxFoundWifi = (-1);
        if (null == mWifiManager) {
            Log.i(TAG, "startScan() - ERROR = WIFI-MANAGER NOT INITIALIZED");
            return false;
        }
//        Log.i(TAG, "**************** startScan() ******** NOT * DONE **********************");
//////////////////////////////////////////////////////////////////////////////////////////////////////
////        if (mWifiManager.startScan()) {                  //------do-for-lower-Android-versions----
////            Log.i(TAG, "startScan() - Scanning.");
////        } else {
////            Log.i(TAG, "startScan() - ERROR - - UNABLE TO INITIATE SCANNING - - - - - - - -");
////        }
//////////////////////////////////////////////////////////////////////////////////////////////////////
        Log.i(TAG, "**************** startScan() ******** * *******************************");
        if (mWifiManager.startScan()) {                  //------do-for-lower-Android-versions----
            Log.i(TAG, "startScan() - Scanning. - - - - - - - - - - - - - - - - - - - - - -");
        } else {
            Log.i(TAG, "startScan() - ERROR - - UNABLE TO INITIATE SCANNING - - - - - - - -");
        }
        return true;
    }


    class WifiReceiver extends BroadcastReceiver {

        public void onReceive(Context c, Intent intent) {
            Log.i(TAG, "*wifiReceiver* onReceive()");
            //// clear out the previous indicators
            for (int i = 0; i < mWifiItemsCount; i++) {
                mWifiItems[i].mFreq = (0);
                mWifiItems[i].mRssi = (0);
                mWifiItems[i].mSSID = ("");
                mWifiItems[i].mBSSID = ("");
                mWifiItems[i].mID = (0);
            }

            //// get the next results
            mWifiList = mWifiManager.getScanResults();
            Log.i(TAG, "*wifiReceiver* getScanResults() - " + mWifiList.toString());

            //////////////----*wifiReceiver* getScanResults() - [SSID: da-robotics, BSSID: 02:f0:21:32:22:5f, capabilities: [WPA2-PSK-CCMP][ESS], level: -37, frequency: 2437, timestamp: 10885990774, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: GOOGLE_FOUR, BSSID: f8:f0:05:e4:60:24, capabilities: [WEP][ESS], level: -38, frequency: 2442, timestamp: 10886111176, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: bdksecure, BSSID: 70:0f:6a:7a:06:81, capabilities: [WPA2-EAP-CCMP][ESS], level: -40, frequency: 2412, timestamp: 10885446659, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: da-sohpi, BSSID: 0a:f0:21:32:22:5f, capabilities: [WPA2-PSK-CCMP][ESS], level: -41, frequency: 2437, timestamp: 10886017080, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: Vision, BSSID: 54:13:79:cf:51:05, capabilities: [WPA2-PSK-CCMP+TKIP][WPA-PSK-CCMP+TKIP][ESS][WPS], level: -43, frequency: 2412, timestamp: 10885441715, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: ATA_Dev, BSSID: 70:0f:6a:7a:06:8c, capabilities: [WPA2-PSK-CCMP][ESS], level: -47, frequency: 5260, timestamp: 10889859275, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 1, centerFreq0: 5270, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: bdksecure, BSSID: 70:0f:6a:7a:06:8e, capabilities: [WPA2-EAP-CCMP][ESS], level: -47, frequency: 5260, timestamp: 10887391287, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 1, centerFreq0: 5270, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: Guest Access, BSSID: 70:0f:6a:7a:06:8d, capabilities: [ESS], level: -47, frequency: 5260, timestamp: 10887396218, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 1, centerFreq0: 5270, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: swk, BSSID: 70:0f:6a:7a:06:8f, capabilities: [WPA2-EAP-CCMP][ESS], level: -48, frequency: 5260, timestamp: 10887386105, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 1, centerFreq0: 5270, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: , BSSID: 04:f0:21:32:23:23, capabilities: [ESS], level: -48, frequency: 5785, timestamp: 10887143840, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 1, centerFreq0: 5795, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: IOT-PG, BSSID: 2c:5a:0f:08:52:f0, capabilities: [WPA2-PSK-CCMP][ESS], level: -51, frequency: 2412, timestamp: 10885438789, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: , BSSID: 00:1a:30:e5:32:c0, capabilities: [WPA2-EAP-CCMP][WPA-EAP-TKIP][ESS], level: -53, frequency: 2462, timestamp: 10886519561, distance: ?(cm), distanceSd: ?(cm), passpoint: no, ChannelBandwidth: 0, centerFreq0: 0, centerFreq1: 0, 80211mcResponder: is not supported, Carrier AP: no, Carrier AP EAP Type: -1, Carrier name: null, SSID: NETGEAR51, BSSID: b0:39
            // {FragmSearch}: onSearchResults(): 02:f0:21:32:22:5f -37dBm  2437mHz  ch.6 "da-robotics"
            // {FragmSearch}: onSearchResults(): f8:f0:05:e4:60:24 -38dBm  2442mHz  ch.7 "GOOGLE_FOUR" *************
            // {FragmSearch}: onSearchResults(): 70:0f:6a:7a:06:81 -40dBm  2412mHz  ch.1 "bdksecure"
            // {FragmSearch}: onSearchResults(): 0a:f0:21:32:22:5f -41dBm  2437mHz  ch.6 "da-sohpi"
            // {FragmSearch}: onSearchResults(): 54:13:79:cf:51:05 -43dBm  2412mHz  ch.1 "Vision"
            // {FragmSearch}: onSearchResults(): 2c:5a:0f:08:52:f0 -51dBm  2412mHz  ch.1 "IOT-PG"
            // {FragmSearch}: onSearchResults(): 00:1a:30:e5:32:c0 -53dBm  2462mHz  ch.11 ""
            // {FragmSearch}: onSearchResults(): b0:39:56:67:65:c0 -54dBm  2427mHz  ch.4 "NETGEAR51"
            // {FragmSearch}: onSearchResults(): 00:06:25:f6:49:52 -59dBm  2437mHz  ch.6 "linksys"
            // {FragmSearch}: onSearchResults(): 00:27:e3:50:eb:61 -66dBm  2462mHz  ch.11 "bdksecure"
            // {FragmSearch}: onSearchResults(): 5c:5b:35:33:fd:81 -67dBm  2412mHz  ch.1 "GoogleGuest-Legacy"
            // {FragmSearch}: onSearchResults(): fa:8f:ca:57:b7:03 -70dBm  2412mHz  ch.1 "HKEnchant80021bd.l006"
            // {FragmSearch}: onSearchResults(): 5c:5b:35:35:c1:91 -73dBm  2412mHz  ch.1 "GoogleGuest-Legacy"
            // {FragmSearch}: onSearchResults()~~~~ f8:f0:05:e4:60:24 ~~ GOOGLE_FOUR ~~ -38 ~~~~
            // {FragmSearch}: {animationListener}-onAnimationEnd()


            //// look through each result and add/replace in the list of indicators
            for (int i = 0; i < mWifiList.size(); i++) {
                //// get the next result
                ScanResult sr = mWifiList.get(i);

                //// get its BSSID
                String strBSSID = sr.BSSID;

                //// assume it isn't already in the list
                boolean bUpdated = false;
                //// look for this bssid in the display list
                for (int j = 0; j < mWifiItemsCount; j++) {
                    //// if found, update its contents and exit
                    if (strBSSID.contentEquals(mWifiItems[j].mBSSID)) {
                        mWifiItems[j].mFreq = sr.frequency;
                        mWifiItems[j].mRssi = sr.level;
                        //// mark it as processed
                        bUpdated = true;
                        break;
                    }
                }
                //// BUT-- If it wasn't processed, add it to the found list
                if (!bUpdated) {
                    //// check for bounds
                    if (mWifiItemsCount < (WIFI_ITEMS_MAX - 1)) {
                        mWifiItems[mWifiItemsCount] = new WifiScanItem(mWifiItemsCount, sr.SSID, sr.BSSID, sr.frequency, sr.level);
                        //// bump the index
                        ++mWifiItemsCount;
                    }
                }
            }

            //// consume the found results
            consumeWifiScanResults();

        }
    }


    private final static int SSID_MINIMUM_LENGTH = (8);

    private void consumeWifiScanResults() {
        int max_rssi = (-9999);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mWifiItemsCount; i++) {
            String ssid = mWifiItems[i].mSSID;
            int freq = mWifiItems[i].mFreq;
            int rssi = mWifiItems[i].mRssi;
            int channel = ((freq - (2412 - 7)) / 5);
            if ((null != ssid) && (channel > 0) && (channel < 14)) {
                sb.setLength(0);
                sb.append("onSearchResults(): ");
                sb.append(mWifiItems[i].mBSSID);
                sb.append(" ");
                sb.append(rssi);
                sb.append("dBm  ");
                sb.append(freq);
                sb.append("mHz  ch.");
                sb.append(channel);
                sb.append(" \"");
                sb.append(ssid);
                sb.append("\"");

                if ((null != ssid) && (ssid.length() >= SSID_MINIMUM_LENGTH)) {
                    //////////////////////////if (ssid.toUpperCase().contains(SSID_PREFIX_MARKER)) {
                    if (ssid.substring(0,4).toUpperCase().contains(SSID_PREFIX_MARKER)) {
                        sb.append(" *************");
                        if (rssi > max_rssi) {
                            max_rssi = rssi;
                            mNdxFoundWifi = (i); ////////////////////// (*)--Save the STRONGEST rssi
                        }
                    }
                }

                sb.append("\n");
                Log.i(TAG, sb.toString());
            }
        }

        if (mNdxFoundWifi >= 0) {
            Log.i(TAG, "onSearchResults()~~~~ ["
                    + mNdxFoundWifi
                    + "] ~~ "
                    + mWifiItems[mNdxFoundWifi].mBSSID
                    + " ~~ "
                    + mWifiItems[mNdxFoundWifi].mSSID
                    + " ~~ "
                    + mWifiItems[mNdxFoundWifi].mRssi + " ~~~~");

            if (null != mListenerWifiProcSearchFound) {
                mListenerWifiProcSearchFound.onItemFound(
                        mWifiItems[mNdxFoundWifi].mBSSID,
                        mWifiItems[mNdxFoundWifi].mSSID,
                        mWifiItems[mNdxFoundWifi].mRssi);
                ////////////////--Note: This will fire EVERY TIME the search updates its results
            }

            mNdxFoundWifi = (-1);
        }


    }


    public void initialize() {
        mWifiManager = (WifiManager) mActivity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        //////// ensure ENABLED
        if (mWifiManager.isWifiEnabled()) {
            Log.i(TAG, "initialize() - mWifiManager.isWifiEnabled() == TRUE");
        } else {
            Log.i(TAG, "initialize() - mWifiManager.isWifiEnabled() == FALSE");
            mWifiManager.setWifiEnabled(true);
            Log.i(TAG, "initialize() - mWifiManager - Enabling Wifi");
        }
    }


    interface ListenerWifiProcSearchFound {
        public void onItemFound(String mac, String ssid, int rssi);
    }

    private ListenerWifiProcSearchFound mListenerWifiProcSearchFound = null;

    public void setListenerWifiProcSearchFound(ListenerWifiProcSearchFound li) {
        mListenerWifiProcSearchFound = (li);
    }


    interface ListenerWifiProcConnected {
        public void onItemFound();
    }

    private ListenerWifiProcConnected mListenerWifiProcConnected = null;

    public void setListenerWifiProcConnected(ListenerWifiProcConnected li) {
        mListenerWifiProcConnected = (li);
    }


//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////
//-////////-///////////////////////////////////////////////////////////////////////////////////////




    public boolean wifiConnectCIU(AppServiceData appData) {
        Log.i(TAG, "wifiConnectCIU() - \"" + appData.mCIU_ssid + "\" -\"" + appData.mCIU_password + "\"");

        if (null == mWifiManager) {
            Log.i(TAG, "wifiConnectCIU() - ERROR ==== NO Wifi-Manager");
            return false;
        }



//        connectToWifiCIU(appData.mCIU_ssid, appData.mCIU_password);
//        return false;
/*-----------------------*/

//appData.mCIU_ssid = "Androgen";
//appData.mCIU_password = "3343321243";

        //////////////////////////////////////////////////////////////////////////////
        //////// get the list of all Wifi-Profiles in the device
        /*-
        List<WifiConfiguration> wifiConfigs = mWifiManager.getConfiguredNetworks();


        //////// iterate through the list and find any that matches the target SSID, and REMOVE it
        String qsq = "\"" + appData.mCIU_ssid + "\"";
        boolean bWcRemoved = false;
        for (WifiConfiguration w : wifiConfigs) {
            if ((null != w) && (null != w.SSID)) {
                if (w.SSID.equals(qsq)) {
                    //////// remove this network from the network manager
                    mWifiManager.removeNetwork(w.networkId);
                    bWcRemoved = true;
                    Log.i(TAG, "wifiConnectCIU() - removed the CIU from Net-Manager");
                }
            }
        }
        if (! bWcRemoved) {
            Log.i(TAG, "wifiConnectCIU() - Nothing removed from Net-Manager.");
        }
        -*/



        //////////////////////////////////////////////////////////////////////////////
        //////// add the new wifi CIU to the list of Wifi accesses
        WifiConfiguration w = new WifiConfiguration();
        w.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        w.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        w.status = WifiConfiguration.Status.ENABLED;
        w.SSID = "\"" + appData.mCIU_ssid + "\"";;
        w.hiddenSSID = false;
        w.preSharedKey = "\"" + appData.mCIU_password + "\"";
        int netId = mWifiManager.addNetwork(w);

        if (netId < 0) {
            Log.i(TAG, "wifiConnectCIU() - ERROR - Unabele to add new CIU to Net-Manager");
            return false;
        }
        Log.i(TAG, "wifiConnectCIU() - Added new CIU to Net-Manager");



        //////////////////////////////////////////////////////////////////////////////
        //////// connect to the Network identified by (netId)
        Log.i(TAG, "wifiConnectCIU() - connecting..");
        //mWifiManager.
        mWifiManager.enableNetwork(netId, true);
        mWifiManager.reconnect();
        Log.i(TAG, "wifiConnectCIU() - connecting........");




        //////////////////////////////////////////////////////////////////////////////////////
        //////// enter a wait-loop, looking for the connection
        long tmExpiration = System.currentTimeMillis() + 21000;
        while (System.currentTimeMillis()  <  tmExpiration)
        {
            try {
                Thread.sleep(2000);
            } catch( InterruptedException ie) {
                ;
            }

            String s = (UtilNetwork.getConnectedSSID(mActivity));
            if (null == s) {
                Log.i(TAG, "wifiConnectCIU() - NOT connected at this moment...");
            }
            else {
                Log.i(TAG, "wifiConnectCIU() - ***** CONNECTED TO " + s + " ***************");
                Log.i(TAG, "wifiConnectCIU() - ***** CONNECTED TO " + s + " **************");
                Log.i(TAG, "wifiConnectCIU() - ***** CONNECTED TO " + s + " *************");
                Log.i(TAG, "wifiConnectCIU() - ***** CONNECTED TO " + s + " ************");

                return true;
            }
        }

        Log.i(TAG, "wifiConnectCIU() - Error - Time-Out while trying to connect");
        return false;
/*------------------------*/
    }

















/* ------------------------------------------------------------------------------------------------

    public void connectToWifiCIU(String ssid, String passw) {
        Log.i(TAG, "connectToWifiCIU(\"" + ssid + "\", \"" + passw + "\")");
        if (null == mActivity) {
            Log.i(TAG, "connectToWifiCIU() - ERROR ==== NO Activity");
            return;
        }

        if (null == mWifiManager) {
            Log.i(TAG, "connectToWifiCIU() - ERROR ==== NO Wifi-Manager");
            return;
        }

        ConnectivityManager connManager = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connManager) {
            Log.i(TAG, "connectToWifiCIU() - ERROR ==== conn-Mger is NULL");
            return;
        }

        NetworkInfo netInfo = connManager.getActiveNetworkInfo();
        if (null == netInfo) {
            Log.i(TAG, "connectToWifiCIU() - ERROR ==== netInfo is NULL");
            return;
        }

        boolean bIsConnected = netInfo.isConnected();
        if (!bIsConnected) {
            Log.i(TAG, "connectToWifiCIU() - Currently DIS-connected from WiFi.");
        } else {
            Log.i(TAG, "connectToWifiCIU() - DIS-connecting from WiFi...");
            boolean bIsDisconnected = mWifiManager.disconnect();
        }


        //////// get the list of all Wifi-Profiles in the device
        List<WifiConfiguration> wifiConfigs = mWifiManager.getConfiguredNetworks();

        //////// iterate through the list and find any that matches the given SSID, and REMOVE it
        String qsq = "\"" + ssid + "\"";
        boolean bWcRemoved = false;
        for (WifiConfiguration w : wifiConfigs) {
            if ((null != w) && (null != w.SSID)) {
                if (w.SSID.equals(qsq)) {
                    //////// remove this network from the network manager
                    mWifiManager.removeNetwork(w.networkId);
                    bWcRemoved = true;
                    Log.i(TAG, "connectToWifiCIU() - removed the CIU from Net-Manager");
                }
            }
        }
        if (!bWcRemoved) {
            Log.i(TAG, "connectToWifiCIU() - Nothing removed from Net-Manager.");
        }

        //////// add the new wifi CIU to the list of Wifi accesses
        WifiConfiguration w = new WifiConfiguration();
        //w.allowedAuthAlgorithms.clear();
        //w.allowedKeyManagement.clear();
        //w.allowedGroupCiphers.clear();
        //w.allowedPairwiseCiphers.clear();
        //w.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        //w.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        w.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        w.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        w.status = WifiConfiguration.Status.ENABLED;
        w.SSID = qsq;
        w.hiddenSSID = false;
//        w.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        w.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
//        w.allowedAuthAlgorithms.set(WifiConfiguration.Protocol.WPA);
        w.preSharedKey = "\"" + passw + "\"";
        int netId = mWifiManager.addNetwork(w);

        if (netId < 0) {
            Log.i(TAG, "connectToWifiCIU() - ERROR - Unabele to add new CIU to Net-Manager");
            return;
        }
        Log.i(TAG, "connectToWifiCIU() - Added new CIU to Net-Manager");


        Log.i(TAG, "connectToWifiCIU() - connecting..");
        //mWifiManager.
        mWifiManager.enableNetwork(netId, true);
        mWifiManager.reconnect();
        Log.i(TAG, "connectToWifiCIU() - connecting........");


        final String finalQsq = qsq;
        long tmInitial = System.currentTimeMillis();
        final long tmExpire = ((85 * 1000) + tmInitial);
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                ConnectivityManager connManager = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = connManager.getActiveNetworkInfo();
                if (null == netInfo || !netInfo.isConnected()) {
                    //Log.i(TAG,"Wait-Connect:(" +mRunnableTimeOut+ ") " + finalQsq);
                    Log.i(TAG, "@@@ ConnectWait:(" + (tmExpire - System.currentTimeMillis()) + ") " + finalQsq);
                    if (System.currentTimeMillis() < tmExpire) {
                        mHandler.postDelayed(this, 1000);
                    } else {
                        Log.i(TAG, "@@@ ConnectToAP - FAILED * * * * * * *");
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                procReconnectOriginalAP();
                            }
                        });
                        return;
                    }
                } else {
                    Log.i(TAG, "runnable - netInfo reports Connected.");
                    mTmExpires = (System.currentTimeMillis() + (1000 * 9));
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            procConnectedCIU();
                        }
                    });
                }
            }
        };
        mHandler.postDelayed(mRunnable, 1000);
        Log.i(TAG, "Triggering runnable in 1000-mSec");


    }


    Runnable mRunnable;
    Handler mHandler;
    long mTmExpires;


    private void procConnectedCIU() {
        Log.i(TAG, "procConnectedCIU() ---");
        Log.i(TAG, "procConnectedCIU() ---");
        Log.i(TAG, "procConnectedCIU() ---");
        Log.i(TAG, "procConnectedCIU() ---");
        Log.i(TAG, "procConnectedCIU() ---");
        Log.i(TAG, "procConnectedCIU() ---");
//        if (null != mListenerWifiProcConnected) {
//            mListenerWifiProcConnected.onItemFound();
//        }


    }

    private void procReconnectOriginalAP() {
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
        Log.i(TAG, "procReconnectOriginalAP() ---");
    }
------------------------------------------------------------------------------------------------ -*/














//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////// 5-mSec
    public String getWifiConnectionData() {
        Log.i(TAG, "getWifiConnectionData() - Entry");
        String ssid = (UtilNetwork.getConnectedSSID(mActivity));
        if (null == ssid) {
            Log.i(TAG, "getWifiConnectionData() - NOT CURRENTLY CONNECTED === NULL");
            return (null);
        }

        Log.i(TAG, "getWifiConnectionData() - \"" + ssid + "\"");

        return (ssid);
    }


    ///////////////////// 5-mSec
    public String getWifiConnection_router(AppServiceData appData) {
        Log.i(TAG, "getWifiConnection_router() - Entry");
        String s = (UtilNetwork.getConnectedSSID(mActivity));
        if (null == s) {
            Log.i(TAG, "getWifiConnection_router() - NOT CURRENTLY CONNECTED === NULL");
            return (null);
        }

        appData.mRouter_ssid = (s);
        Log.i(TAG, "getWifiConnection_router() - \"" + appData.mRouter_ssid + "\"");

        return (appData.mRouter_ssid);
    }


    ///////////////////// 30-mSec,  7600-mSec on Android-6
     public boolean getWifiProfiles(AppServiceData appData) {
        Log.i(TAG, "getWifiProfiles() - Entry");
        if (null == mWifiManager) {
            Log.i(TAG, "getWifiProfiles() - *ERROR- WifiManager == null");
            return false;
        }
        List<WifiConfiguration> wifiConfigs = mWifiManager.getConfiguredNetworks();

        //////// iterate through the list and find any that match the CIU's SSID, and REMOVE it
        String qSq = "\"" + appData.mCIU_ssid + "\"";
        Log.i(TAG, "getWifiProfiles() - qSq = " + qSq);
        boolean bWcRemoved = false;
        for (WifiConfiguration w : wifiConfigs) {
            if ((null != w) && (null != w.SSID)) {
                Log.i(TAG, "getWifiProfiles() - nid:" + w.networkId + ": " + w.SSID);
                if (w.SSID.length() > 8) {
                    if (w.SSID.contains(SSID_PREFIX_MARKER)) {
                        //////// remove this network from the network manager
                        mWifiManager.removeNetwork(w.networkId);
                        bWcRemoved = true;
                        Log.i(TAG, "getWifiProfiles() - removed \"" + w.SSID + "\" ********");
                    }
                }
            }
        }
        ////--------------------------------------
        if (bWcRemoved) {
            mWifiManager.saveConfiguration();
            Log.i(TAG, "getWifiProfiles() - *SAVED*CHANGES* - - - - DELAY..");
            try{Thread.sleep(3000);}catch(InterruptedException e){}
        }
        ////--------------------------------------
        if (! bWcRemoved) {
            Log.i(TAG, "getWifiProfiles() - NOTHING REMOVED from Net-Manager.");
        }
        Log.i(TAG, "getWifiProfiles() - return");

        return true;
    }


    ///////////////////// 2000-mSec
    public boolean wifiDisconnect(AppServiceData appData) {
        Log.i(TAG, "wifiDisconnect() - Entry");
        String currentSSID = (UtilNetwork.getConnectedSSID(mActivity));
        if (null == currentSSID) {
            Log.i(TAG, "wifiDisconnect() - Already DIS-Connected");
            return true;
        }
        Log.i(TAG, "wifiDisconnect() - Current SSID = (" + currentSSID + ")");


        try{Thread.sleep(1000);}catch(InterruptedException e){}
        if( mWifiManager.disconnect() ) {
            Log.i(TAG, "wifiDisconnect() - Return - SUCCESS ... 1-second");
            try{Thread.sleep(1000);}catch(InterruptedException e){}
            Log.i(TAG, "wifiDisconnect() - Return - SUCCESS");
            return true;
        }

        Log.i(TAG, "wifiDisconnect() - Return - FAIL");
        return false;
    }


    ///////////////////// 2000-mSec
    //public boolean wifiConnectCIU_temporary(AppServiceData appData) {
    //    Log.i(TAG, "wifiConnectCIU_temporary() - Entry");
    //    Log.i(TAG, "wifiConnectCIU_temporary() - Return");
    //    return false;
    //}


    public static boolean isSSID_of_CIU(String ssid) {
        if (null == ssid) {
            return false;
        }
        if (ssid.length() < 22) {
            return false;
        }
        if (! ssid.contains(SSID_PREFIX_MARKER)) {
            return false;
        }

        return true;
    }





























































    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    /*-
    public void delayer(final int numMilliSeconds, WifiResultListener listener) {
        if (numMilliSeconds <= 0) {
            Log.i(TAG, "delayer() - *ERROR- Invalid Delta-T: " + numMilliSeconds);
            return;
        }
        Log.i(TAG, "delayer() - Delta-T = " + numMilliSeconds);

        WifiDelayProc wdProc = new WifiDelayProc();
        wdProc.setDurationTime( numMilliSeconds );
        wdProc.setWifiListener(listener);
        wdProc.initiate();
        Log.i(TAG, "delayer() - Initiated.");
    }
    -*/





    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////


    private WifiResultListener mTcpCommsResultListener = null;
    public void tcp_comms_setListener(WifiResultListener li) {
        mTcpCommsResultListener = li;
    }
    public void tcp_comms(final byte [] barTX) {
        final WifiAsyncProc waProc = new WifiAsyncProc();
        waProc.setDurationTime(9*1000);
        waProc.setWifiListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                int nCode = result.getCode();
                Log.i(TAG, "tcp_comms()- LISTENER- ******************************************************");
                Log.i(TAG, "tcp_comms()- LISTENER- onWifiResult() ==> "+ (nCode) +" "+ WifiResult.codeToString(nCode) );
                Log.i(TAG, "tcp_comms()- LISTENER- onWifiResult() ==> "+ Utils.byteArrayToHexMixedString((byte[])result.getData()) );
                Log.i(TAG, "tcp_comms()- LISTENER- *******************************************************");
                if (null != mTcpCommsResultListener) {
                    Log.i(TAG, "tcp_comms()- LISTENER- invoke upstream call-back");
                    Log.i(TAG, "tcp_comms()- LISTENER- *******************************************************");
                    mTcpCommsResultListener.onWifiResult(result);
                }
            }
        });
        waProc.setRunnable(new WifiRunnable() {
            @Override
            public void run() {
                TcpIpClient client = new TcpIpClient();
                byte [] ar = client.comms(barTX);
                Log.i(TAG, "tcp_comms()- @run()- tcp-ip-client Terminated.");
                Log.i(TAG, "tcp_comms()- @run()- TERMINATE ********************");
                if (null == ar) {
                    Log.i(TAG, "tcp_comms()- @run()- tcp-ip -> code Fail");
                    getWifiResult().setCode(WifiResult.X_FAIL);
                } else {
                    Log.i(TAG, "tcp_comms()- @run()- tcp-ip -> code Success & str = " + Utils.byteArrayToHexMixedString(ar));
                    getWifiResult().setCode(WifiResult.X_SUCCESS);
                    getWifiResult().setData(ar);
////////////////////getWifiResult().setStr(client.getRx());/////////////////////////////////////////
                }
                Log.i(TAG, "tcp_comms()- @run()- tcp-ip -- Finished. Terminate WifiAsyncProc.");
                waProc.terminateSignalWifiRunnable();
            }
        });
        waProc.initiate();
    }








    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////

    public boolean connectCIU_start(WifiResultListener li) {
        Log.i(TAG, "connectCIU_start() - Entry");
        if (null == mWifiManager) {
            Log.i(TAG, "connectCIU_start() - *ERROR- WifiManager == null");
            return false;
        }

        final WifiAsyncProc waProc = new WifiAsyncProc();
        waProc.setDurationTime(10*1000);
        waProc.setWifiListener(li);////new WifiResultListener() {
//            @Override
//            public void onWifiResult(WifiResult result) {
//                Log.i(TAG,"connectCIU_start()-@Listener- (i)   : " + result.getCode() + result.getStr());
//                Log.i(TAG,"connectCIU_start()-@Listener- (ii)  : " + result.getCode() + result.getStr());
//                Log.i(TAG,"connectCIU_start()-@Listener- (iii) : " + result.getCode() + result.getStr());
//                Log.i(TAG,"connectCIU_start()-@Listener- (iv)  : " + result.getCode() + result.getStr());
//            }
//        });
        waProc.setRunnable(new WifiRunnable() {
            @Override
            public void run() {
                Log.i(TAG,"connectCIU_start()-@Run- Begin.");
                long  tm0 = System.currentTimeMillis();

//                //////// Do this to simulate never finding any result and timing out
//                try { Thread.sleep(15*1000); }
//                catch (InterruptedException ie) {
//                    Log.i(TAG,"connectCIU_start()-@Run- INTERRUPTED ...");
//                    return;
//                }

                WifiInfo winfo = mWifiManager.getConnectionInfo();
                if (null == winfo) {
                    getWifiResult().setCode(WifiResult.X_FAIL);
                    getWifiResult().setStr("getConnectionInfo() - FAIL FAIL FAIL FAIL FAIL FAIL");
                    Log.i(TAG,"connectCIU_start()-@Run- BAD WIFI-INFO - CANT GET CONNECTION INFO");
                    Log.i(TAG,"connectCIU_start()-@Run- Return.");
                    waProc.terminateSignalWifiRunnable();
                    return;
                }
                String QssidQ_connected = winfo.getSSID();

                if ((QssidQ_connected.length() < 22) && (! QssidQ_connected.contains(SSID_PREFIX_MARKER))) {
                    mData.mRouter_ssid = QssidQ_connected;
                    Log.i(TAG,"connectCIU_start()-@Run- Router is ===> " + QssidQ_connected);
                } else {
                    Log.i(TAG,"connectCIU_start()-@Run- **NO-ROUTER-DETECTED** Connection is ===> " + QssidQ_connected);
                }
                Log.i(TAG,"connectCIU_start()-@Run- dt = " + (System.currentTimeMillis() - tm0));


                Log.i(TAG,"connectCIU_start()-@Run- getConfiguredNetworks() . . .");
                List<WifiConfiguration> wifiConfigs= mWifiManager.getConfiguredNetworks();
                Log.i(TAG,"connectCIU_start()-@Run- getConfiguredNetworks(). dt = " + (System.currentTimeMillis() - tm0));

                boolean bNeedsSaving = false;
                for (WifiConfiguration wc : wifiConfigs) {
                    if (null == wc.SSID) {
                        continue;
                    }
                    if (wc.SSID.length() < 22) {
                        Log.i(TAG,"connectCIU_start()-@Run- #" + wc.networkId
                                +": "+wc.BSSID + " - "+wc.SSID  );
                        continue;
                    }
                    if (wc.SSID.contains(SSID_PREFIX_MARKER)) {
                        mWifiManager.removeNetwork(wc.networkId);
                        bNeedsSaving = true;
                        Log.i(TAG,"connectCIU_start()-@Run- #" + wc.networkId
                                +": "+wc.BSSID + " - "+wc.SSID + " ----------REMOVE---"  );
                        continue;
                    }
                }
                if (bNeedsSaving) {
                    mWifiManager.saveConfiguration();
                    Log.i(TAG,"connectCIU_start()-@Run- saveConfiguration(). dt = " + (System.currentTimeMillis() - tm0));
                } else {
                    Log.i(TAG, "connectCIU_start()-@Run- Configurations. dt = " + (System.currentTimeMillis() - tm0));
                }


                WifiConfiguration w = new WifiConfiguration();
                w.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                w.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                w.status = WifiConfiguration.Status.ENABLED;
                w.SSID = "\"" + mData.mCIU_ssid + "\"";;
                w.hiddenSSID = false;
                w.preSharedKey = "\"" + mData.mCIU_password + "\"";
                int netId = mWifiManager.addNetwork(w);

                Log.i(TAG,"connectCIU_start()-@Run- addNetwork(). netId = " + netId);
                Log.i(TAG,"connectCIU_start()-@Run- dt = " + (System.currentTimeMillis() - tm0));
                if (netId < 0) {
                    Log.i(TAG, "connectCIU_start()-@Run-  ERROR - Unabele to add new CIU to Net-Manager");
                    getWifiResult().setCode(WifiResult.X_FAIL);
                    getWifiResult().setStr("connectCIU_start()-@Run- Cant create A.P. profile");
                    Log.i(TAG,"connectCIU_start()-@Run- Return.");
                    waProc.terminateSignalWifiRunnable();
                    return;
                }



                //////////////////////////////////////////////////////////////////////////////
                //////// connect to the Network identified by (netId)
                Log.i(TAG, "connectCIU_start()-@Run-  connecting..");
                mWifiManager.enableNetwork(netId, true);
                ////////////////////////////mWifiManager.reconnect();




                getWifiResult().setCode(WifiResult.X_SUCCESS);
                getWifiResult().setStr("Connecting to CIU: " + mData.mCIU_ssid);
                Log.i(TAG,"connectCIU_start()-@Run- Connecting to CIU:  = " + mData.mCIU_ssid);

                Log.i(TAG,"connectCIU_start()-@Run- Return.");
                waProc.terminateSignalWifiRunnable();
            }
        });
        waProc.initiate();
        Log.i(TAG, "connectCIU_start() - Initiated. Return.");

        return true;
    }






    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////

    public boolean connectRouter_start(WifiResultListener li) {
        Log.i(TAG, "connectRouter_start() - Entry");
        if (null == mWifiManager) {
            Log.i(TAG, "connectRouter_start() - *ERROR- WifiManager == null");
            return false;
        }

        final WifiAsyncProc waProc = new WifiAsyncProc();
        waProc.setDurationTime(10*1000);
        waProc.setWifiListener(li);
        waProc.setRunnable(new WifiRunnable() {
            @Override
            public void run() {
                Log.i(TAG,"connectRouter_start()-@Run- Begin.");
                long  tm0 = System.currentTimeMillis();
                WifiInfo winfo = mWifiManager.getConnectionInfo();
                if (null == winfo) {
                    getWifiResult().setCode(WifiResult.X_FAIL);
                    getWifiResult().setStr("getConnectionInfo() - FAIL FAIL FAIL FAIL FAIL FAIL");
                    Log.i(TAG,"connectRouter_start()-@Run- BAD WIFI-INFO - CANT GET CONNECTION INFO");
                    Log.i(TAG,"connectRouter_start()-@Run- Return.");
                    waProc.terminateSignalWifiRunnable();
                    return;
                }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                String QssidQ_connected = winfo.getSSID();
//
//                if ((QssidQ_connected.length() < 22) && (! QssidQ_connected.contains(SSID_PREFIX_MARKER))) {
//                    mData.mRouter_ssid = QssidQ_connected;
//                    Log.i(TAG,"connectRouter_start()-@Run- Router is ===> " + QssidQ_connected);
//                } else {
//                    Log.i(TAG,"connectRouter_start()-@Run- **NO-ROUTER-DETECTED** Connection is ===> " + QssidQ_connected);
//                }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                Log.i(TAG,"connectRouter_start()-@Run- dt = " + (System.currentTimeMillis() - tm0));



                Log.i(TAG,"connectRouter_start()-@Run- getConfiguredNetworks() . . .");
                List<WifiConfiguration> wifiConfigs= mWifiManager.getConfiguredNetworks();
                Log.i(TAG,"connectRouter_start()-@Run- getConfiguredNetworks(). dt = " + (System.currentTimeMillis() - tm0));



                int netId = (-1);
                for (WifiConfiguration wc : wifiConfigs) {
                    if (null == wc.SSID) {
                        continue;
                    }
                    if ((wc.SSID.length() < 22) && (wc.SSID.contains(mData.mRouter_ssid))) {
                        netId = wc.networkId;
                        Log.i(TAG,"connectRouter_start()-@Run- #" + wc.networkId
                            +": "+wc.BSSID + " - "+wc.SSID + " *******************-ROUTER-*"  );
                    }
                    else {
                        Log.i(TAG,"connectRouter_start()-@Run- #" + wc.networkId
                                +": "+wc.BSSID + " - "+wc.SSID  );
                    }
                }


                /*-
                WifiConfiguration w = new WifiConfiguration();
                w.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                w.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                w.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                w.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                w.status = WifiConfiguration.Status.ENABLED;
                w.SSID = "\"" + mData.mCIU_ssid + "\"";;
                w.hiddenSSID = false;
                w.preSharedKey = "\"" + mData.mCIU_password + "\"";
                int netId = mWifiManager.addNetwork(w);

                Log.i(TAG,"connectRouter_start()-@Run- addNetwork(). netId = " + netId);
                -*/

                Log.i(TAG,"connectRouter_start()-@Run- dt = " + (System.currentTimeMillis() - tm0));
                if (netId < 0) {
                    Log.i(TAG, "connectRouter_start()-@Run-  ERROR - Unabele to add new CIU to Net-Manager");
                    getWifiResult().setCode(WifiResult.X_FAIL);
                    getWifiResult().setStr("connectRouter_start()-@Run- Cant create A.P. profile");
                    Log.i(TAG,"connectRouter_start()-@Run- Return.");
                    waProc.terminateSignalWifiRunnable();
                    return;
                }



                //////////////////////////////////////////////////////////////////////////////
                //////// connect to the Network identified by (netId)
                Log.i(TAG, "connectRouter_start()-@Run-  connecting..");
                mWifiManager.enableNetwork(netId, true);
                ////////////////////////////mWifiManager.reconnect();




                getWifiResult().setCode(WifiResult.X_SUCCESS);
                getWifiResult().setStr("Connecting to Router: " + mData.mRouter_ssid);
                Log.i(TAG,"connectRouter_start()-@Run- Connecting to Router:  = " + mData.mRouter_ssid);

                Log.i(TAG,"connectRouter_start()-@Run- Return.");
                waProc.terminateSignalWifiRunnable();
            }
        });
        waProc.initiate();
        Log.i(TAG, "connectRouter_start() - Initiated. Return.");

        return true;
    }





    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////

    private boolean           mScanWifi_BCastRxerRegistered = (false);
    private ScanWifiRxer      mScanWifi_BcastRxer;
    private List<ScanResult>  mScanWifi_List = (null);
    private final static int  mScanWifi_SIZE_LIST = (256);
    private ScanWifiItem []   mScanWifi_Items = new ScanWifiItem[mScanWifi_SIZE_LIST];
    private int               mScanWifi_Count = (0);

    public class ScanWifiItem {
        public String mSSID;
        public String mBSSID;
        public int    mFreq;
        public int    mRssi;

        public ScanWifiItem() {
            mFreq = mRssi  = (0);
            mSSID = mBSSID = (null);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////// TODO - add a timestamp long to each item
        ////////// TODO - save(ssid,bssid,rssi,freq) ~~ if list FULL, replaces item with OLDEST timestamp
        ////////// TODO - save(..) will UPDATE any values that already exist under same BSSID
        ////////// TODO - getScanWifiItem() needs to carefully examine MARKER, *AND* select STRONGEST rssi
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
////    public int getScanItemsCount() {
////        return mScanWifi_Count;
////    }
////    public ScanWifiItem[] getScanItems() {
////        return mScanWifi_Items;
////    }
    public ScanWifiItem getScanWifiItem_CIU() {
        ScanWifiItem si;
        for (int i=0; i<mScanWifi_Count; i++) {
            si = mScanWifi_Items[i];
            if ((null == si) || (si.mSSID.length() < 20) || (! si.mSSID.contains(SSID_PREFIX_MARKER))) {
                continue;
            }
            return (mScanWifi_Items[i]);
        }
        return (null);
    }


    private class ScanWifiRxer extends BroadcastReceiver {

        private int mCounted = (0);

        public void onReceive(Context c, Intent intent) {
            ++mCounted;
            Log.i(TAG, "{{ScanWifiRxer}}- onReceive() - (# " + mCounted + ")");
            if (null == mScanWifi_BcastRxer) {
                Log.i(TAG, "{{ScanWifiRxer}}- onReceive() - BcastRxer is NULL");
            } else {
                Log.i(TAG, "{{ScanWifiRxer}}- onReceive() - BcastRxer is NOT-null");
            }
            if (mScanWifi_BCastRxerRegistered) {
                Log.i(TAG, "{{ScanWifiRxer}}- onReceive() - BcastRxerRegistered is TRUE");
            } else {
                Log.i(TAG, "{{ScanWifiRxer}}- onReceive() - BcastRxerRegistered is FALSE");
            }

            //// get the next results
            mScanWifi_List = mWifiManager.getScanResults();
            Log.i(TAG, "{{ScanWifiRxer}}- getScanResults() - " + mScanWifi_List.toString());

            //// copy the scan results into the local array
            mScanWifi_Count = (0);
            for( ScanResult s : mScanWifi_List) {
                if (null == s.SSID) {
                    continue;
                }
                if (s.SSID.length() < 2) {
                    continue;
                }

                if (null == mScanWifi_Items[mScanWifi_Count]) {
                    mScanWifi_Items[mScanWifi_Count] = new ScanWifiItem();
                }
                mScanWifi_Items[mScanWifi_Count].mSSID  = s.SSID;
                mScanWifi_Items[mScanWifi_Count].mBSSID = s.BSSID;
                mScanWifi_Items[mScanWifi_Count].mFreq  = s.frequency;
                mScanWifi_Items[mScanWifi_Count].mRssi  = s.level;
                ++mScanWifi_Count;
                if (mScanWifi_Count >= mScanWifi_SIZE_LIST) {
                    break;
                }
            }

            //// consume the found results
            Log.i(TAG, "{{ScanWifiRxer}}- onReceive()- THE LIST:");
            for (int i=0; i < mScanWifi_Count; ++i) {
                ScanWifiItem si = mScanWifi_Items[i];
                if (null == si) {
                    Log.i(TAG, "{{ScanWifiRxer}}- onReceive()- *UN-expected NULL.");
                    break;
                }
                Log.i(TAG,"{{ScanWifiRxer}}- onReceive()- "
                        + "#" + i + ": " + si.mBSSID + " == " + si.mSSID + " * " + si.mFreq + "mHz * " + si.mRssi + "dBm"     );
            }
            Log.i(TAG, "{{ScanWifiRxer}}- onReceive()- Return. (# "+ mCounted +")");
        }
    }

    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////

    public void scanWifiForCIU_Stop() {
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Stop() - " + (int)(1000 * Math.random()));
        if (null == mScanWifi_BcastRxer) {
            Log.i(TAG, "scanWifiForCIU_Stop() - BcastRxer is *NULL*");
        }
        else if (! mScanWifi_BCastRxerRegistered) {
            Log.i(TAG, "scanWifiForCIU_Stop() - BcastRxer-FLAG is *FALSE*");
        }
        else {
            ApplicationFIS.getAppContext().unregisterReceiver(mScanWifi_BcastRxer);
            mScanWifi_BcastRxer = null;
            mScanWifi_BCastRxerRegistered = false;
            Log.i(TAG, "scanWifiForCIU_Stop() - BcastRxer *UN-Registered");
        }
    }

    public boolean scanWifiForCIU_start() {
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_Start() - " + (int)(1000 * Math.random()));
        Log.i(TAG, "scanWifiForCIU_start() - Entry");
        if (null == mWifiManager) {
            Log.i(TAG, "scanWifiForCIU_start() - *ERROR- WifiManager == null");
            return false;
        }
        mScanWifi_BcastRxer = new ScanWifiRxer();
        if (null != mScanWifi_BcastRxer) {
            ApplicationFIS.getAppContext()
                    .registerReceiver(
                        mScanWifi_BcastRxer,
                        new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            mScanWifi_BCastRxerRegistered = true;
            Log.i(TAG, "scanWifiForCIU_start() - BcastRxer Registered");
        }

        final WifiAsyncProc waProc = new WifiAsyncProc();
        waProc.setDurationTime(29*1000);
        waProc.setWifiListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                Log.i(TAG,"scanWifiForCIU_start()-@Listener- (i)   : " + result.getCode());
            }
        });
        waProc.setRunnable(new WifiRunnable() {
            @Override
            public void run() {
                Log.i(TAG,"scanWifiForCIU_start()-@Run- Begin.");

                if (mWifiManager.startScan()) {
                    Log.i(TAG, "scanWifiForCIU_start() - ********************** Scanning...");
                    getWifiResult().setCode(WifiResult.X_SUCCESS);
                } else {
                    Log.i(TAG, "scanWifiForCIU_start() - *ERROR- UNABLE TO INITIATE SCAN **");
                    getWifiResult().setCode(WifiResult.X_FAIL);
                }

                Log.i(TAG,"scanWifiForCIU_start()-@Run- Return.");
                waProc.terminateSignalWifiRunnable();
            }
        });
        waProc.initiate();
        Log.i(TAG, "scanWifiForCIU_start() - Initiated. Return.");

        return true;
    }

    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////

    public boolean disconnectWifiFromAP() {
        Log.i(TAG, "disconnectWifiFromAP() - Entry");
        if (null == mWifiManager) {
            Log.i(TAG, "disconnectWifiFromAP() - *ERROR- WifiManager == null");
            return false;
        }
        final WifiAsyncProc waProc = new WifiAsyncProc();
        waProc.setDurationTime(4000);
        waProc.setWifiListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                Log.i(TAG,"disconnectWifiFromAP()-@Listener- (i)   : " + result.getCode());
                Log.i(TAG,"disconnectWifiFromAP()-@Listener- (ii)  : " + result.getCode());
                Log.i(TAG,"disconnectWifiFromAP()-@Listener- (iii) : " + result.getCode());
                Log.i(TAG,"disconnectWifiFromAP()-@Listener- (iv)  : " + result.getCode());
            }
        });
        waProc.setRunnable(new WifiRunnable() {
            @Override
            public void run() {
                Log.i(TAG,"disconnectWifiFromAP()-@Run- Begin.");

                try { Thread.sleep(1500); }
                catch (InterruptedException e) { }

                for(;;) {
                    Log.i(TAG, "disconnectWifiFromAP() - getConnectedSSID()...");
                    String currentSSID = (UtilNetwork.getConnectedSSID(mActivity));
                    if (null == currentSSID) {
                        Log.i(TAG, "disconnectWifiFromAP() - Already DIS-Connected");
                        getWifiResult().setCode(WifiResult.X_FAIL);
                        break;
                    }
                    Log.i(TAG, "disconnectWifiFromAP() - Current SSID = (" + currentSSID + ")");
                    try { Thread.sleep(1000); }
                    catch (InterruptedException e) { }

                    if (mWifiManager.disconnect()) {
                        Log.i(TAG, "disconnectWifiFromAP() - Return - SUCCESS ... 1-second");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                        Log.i(TAG, "disconnectWifiFromAP() - Return - SUCCESS");
                        getWifiResult().setCode(WifiResult.X_SUCCESS);
                        break;
                    }

                    Log.i(TAG, "disconnectWifiFromAP() - Return - FAIL");
                    getWifiResult().setCode(WifiResult.X_FAIL);
                    break;
                }
                getWifiResult().setCode(WifiResult.X_SUCCESS);
                Log.i(TAG, "disconnectWifiFromAP()-@Run- ~~~~ Return.");
                waProc.terminateSignalWifiRunnable();
            }
        });
        waProc.initiate();
        Log.i(TAG,"disconnectWifiFromAP()- Initiated. Return.");
        return true;
    }








    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////

    public boolean getWifiProfilesAsync(final AppServiceData appData) {
        Log.i(TAG, "getWifiProfilesAsync() - Entry");
        if (null == mWifiManager) {
            Log.i(TAG, "getWifiProfilesAsync() - *ERROR- WifiManager == null");
            return false;
        }
        final WifiAsyncProc waProc = new WifiAsyncProc();
////////waProc.cancel();
        waProc.setDurationTime(9900);
        waProc.setRunnable(new WifiRunnable() {
            @Override
            public void run()
            {
                Log.i(TAG,"getWifiProfilesAsync()-@Run- GetConfiguredNetworks()...");
                List<WifiConfiguration> wifiConfigs= mWifiManager.getConfiguredNetworks();

                String qSq = "\"" + appData.mCIU_ssid + "\"";
                Log.i(TAG, "getWifiProfilesAsync()-@Run- qSq = " + qSq);
                boolean bWcRemoved = false;
                for (WifiConfiguration w : wifiConfigs) {
                    if ((null != w) && (null != w.SSID)) {
                        Log.i(TAG, "getWifiProfilesAsync()-@Run- nid:" + w.networkId + ": " + w.SSID);
                        if (w.SSID.length() > 8) {
                            if (w.SSID.contains(SSID_PREFIX_MARKER)) {
                                //////// remove this network from the network manager
                                mWifiManager.removeNetwork(w.networkId);
                                bWcRemoved = true;
                                Log.i(TAG, "getWifiProfilesAsync()-@Run- **** \"" + w.SSID + "\" ********(del)****");
                            }
                        }
                    }
                }
                if (bWcRemoved) {
                    mWifiManager.saveConfiguration();
                    Log.i(TAG, "getWifiProfilesAsync() - *SAVED*CHANGES* - - - - DELAY..");
                    try{Thread.sleep(1900);} catch(InterruptedException e){}
                }
                ////////////////////////////////////////////////////////////////////////////////////
                ////for (int i=0; i<4; ++i) {
                ////    Log.i(TAG, "getWifiProfilesAsync()-@Run- POST-INF-LOOP--TICK.");
                ////    try {Thread.sleep(19000);}
                ////    catch (InterruptedException ie) {
                ////        Log.i(TAG, "getWifiProfilesAsync()-@Run- POST-INF-LOOP--INTERRUPTED.");
                ////        break;
                ////    }
                ////}
                ////////////////////////////////////////////////////////////////////////////////////
                getWifiResult().setCode(WifiResult.X_SUCCESS);
                Log.i(TAG, "getWifiProfilesAsync()-@Run- ~~~~ Return.");
                waProc.terminateSignalWifiRunnable();
            }
        });
        waProc.setWifiListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                if (result.getCode() == WifiResult.X_TIMEOUT) {
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- TIMEOUT TIMEOUT TIMEOUT i");
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- TIMEOUT TIMEOUT TIMEOUT ii");
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- TIMEOUT TIMEOUT TIMEOUT iii");
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- TIMEOUT TIMEOUT TIMEOUT iv");
                }
                else if (result.getCode() == WifiResult.X_SUCCESS) {
                    Log.i(TAG, "getWifiProfilesAsync()-@Listener- SUCCESS SUCCESS SUCCESS i");
                    Log.i(TAG, "getWifiProfilesAsync()-@Listener- SUCCESS SUCCESS SUCCESS ii");
                    Log.i(TAG, "getWifiProfilesAsync()-@Listener- SUCCESS SUCCESS SUCCESS iii");
                    Log.i(TAG, "getWifiProfilesAsync()-@Listener- SUCCESS SUCCESS SUCCESS iv");
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                    delayer(2400, new WifiResultListener() {
//                        @Override
//                        public void onWifiResult(WifiResult result) {
//                            Log.i(TAG, "getWifiProfilesAsync()-@Listener- Delay @@Listener-- OK i");
//                            Log.i(TAG, "getWifiProfilesAsync()-@Listener- Delay @@Listener-- OK ii");
//                            Log.i(TAG, "getWifiProfilesAsync()-@Listener- Delay @@Listener-- OK iii");
//                            Log.i(TAG, "getWifiProfilesAsync()-@Listener- Delay @@Listener-- OK iv");
//                        }
//                    });
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    disconnectWifiFromAP();
                }
                else if (result.getCode() == WifiResult.X_FAIL) {
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- FAIL FAIL FAIL i");
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- FAIL FAIL FAIL ii");
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- FAIL FAIL FAIL iii");
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- FAIL FAIL FAIL iv");
                }
                else {
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- UnKnown.i: " + result.getCode());
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- UnKnown.ii: " + result.getCode());
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- UnKnown.iii: " + result.getCode());
                    Log.i(TAG,"getWifiProfilesAsync()-@Listener- UnKnown.iv: " + result.getCode());
                }
            }
        });
        boolean r = waProc.initiate();
        if (r) {
            Log.i(TAG, "getWifiProfilesAsync() - Success.Initiate().");
            return true;
        } else {
            Log.i(TAG, "getWifiProfilesAsync() - *FAIL***Initiate().");
            return false;
        }
    }

















//    private WifiResultListener mWifiResultListener;
//    public void setWifiResultListener( WifiResultListener li ) {
//        mWifiResultListener = (li);
//    }

    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
/*-
    private final static long DT_WIFI_THREAD = (200);
    private WifiResultListener mWifiTimeoutListener;
    private Thread mThreadTimeout;
    private long mTimeoutExpires;
    public void setTimeoutAndResultListener( long dt, WifiResultListener li) {
        if (null == li) {
            mWifiTimeoutListener = (null);
            Log.i(TAG, "setTimeoutAndResultListener() - Listener OFF");
            return;
        }

        if (dt <= 0) {
            Log.i(TAG, "setTimeoutAndResultListener() -  ((dt <= 0)) !!!!!");
            return;
        }
        Log.i(TAG, "setTimeoutAndResultListener() - dt == " + dt);

        mWifiTimeoutListener = (li);

        mTimeoutExpires = System.currentTimeMillis() + dt;
        timeoutCancel();
        mThreadTimeout = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "setTimeoutAndResultListener() - @@Thread{} - Start.");
                long tm;
                boolean bInterrupted = false;
                while (mTimeoutExpires >= (tm = System.currentTimeMillis())) {
                    try {
                        Thread.sleep(DT_WIFI_THREAD);
                    } catch (InterruptedException ie) {
                        bInterrupted = true;
                        break;
                    }
                }
                if (bInterrupted) {
                    Log.i(TAG, "setTimeoutAndResultListener() - @@Thread{} - Timeout INTERRUPTED");
                    return;
                }
                Log.i(TAG, "setTimeoutAndResultListener() - @@Thread{} - Timeout Timeout Timeout Timeout Timeout Timeout Timeout");
                if (null != mWifiTimeoutListener) {
                    Log.i(TAG, "setTimeoutAndResultListener() - @@Thread{} - Sending RESULT=TIMEOUT");
                    mWifiTimeoutListener.onWifiResult(new WifiResult(WifiResult.X_TIMEOUT));
                    mWifiTimeoutListener = null;
                }
                Log.i(TAG, "setTimeoutAndResultListener() - @@Thread{} - End.");
            }
        });
        mThreadTimeout.start();
        Log.i(TAG, "setTimeoutAndResultListener() - Timeout START");
    }
    public void timeoutCancel() {
        if (null == mThreadTimeout) {
            Log.i(TAG, "TimeoutCancel() - Thread does NOT exist.");
        } else {
            Log.i(TAG, "TimeoutCancel() - Thread exists...");
            if (mThreadTimeout.isAlive()) {
                Log.i(TAG, "TimeoutCancel() - Thread is ALIVE...");
                Log.i(TAG, "TimeoutCancel() - INTERRUPTING...");
                mThreadTimeout.interrupt();
            }
            mThreadTimeout = (null);
        }
    }
-*/
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////





    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
/*-
    private final static long mWifiAsyncTimeout_DELTA_T = (999);
    private Thread mThreadProcAsyncRunnable;
    private Thread mThreadProcAsyncTimeout;
    private WifiResultListener mProcAsyncListener;
    public void cancel() {
        Log.i(TAG,"cancel() - Start.");
        if(null != mThreadProcAsyncTimeout) {
            if (mThreadProcAsyncTimeout.isAlive()) {
                Log.i(TAG,"cancel() - Timeout=ALIVE. Interrupt.");
                mThreadProcAsyncTimeout.interrupt();
            }
            mThreadProcAsyncTimeout = null;
        }
        if(null != mThreadProcAsyncRunnable) {
            if (mThreadProcAsyncRunnable.isAlive()) {
                Log.i(TAG,"cancel() - Runnable=ALIVE. Interrupt.");
                mThreadProcAsyncRunnable.interrupt();
            }
            mThreadProcAsyncRunnable = null;
        }
////////if (null != mProcAsyncListener) {
////////    mProcAsyncListener.onWifiResult(new WifiResult(WifiResult.X_CANCELLED));
////////    mProcAsyncListener = null;
////////}
        Log.i(TAG,"cancel() - Return.");
    }
    public void initProcAsyncAndTimout(Runnable r, final int dt, WifiResultListener li) {
        if (dt <= 0) {
            Log.i(TAG,"initProcAsyncAndTimeout() *Error- Bad (DT)");
            return;
        }
        if (null == r) {
            Log.i(TAG,"initProcAsyncAndTimeout() *Error- Missing Runnable");
            return;
        }
        if (null == r) {
            Log.i(TAG,"initProcAsyncAndTimeout() *Error- Missing Listener");
            return;
        }
        Log.i(TAG,"initProcAsyncAndTimeout() - Start.");
        cancel();
        mProcAsyncListener = (li);
        mThreadProcAsyncTimeout = new Thread(new Runnable(){
            @Override
            public void run() {
                Log.i(TAG,"initProcAsyncAndTimeout() - @Runnable.");
                long expiration = System.currentTimeMillis() + (long)dt;
                for(;;) {
                    try { Thread.sleep(mWifiAsyncTimeout_DELTA_T); }
                    catch (InterruptedException ie) {
                        Log.i(TAG,"initProcAsyncAndTimeout() - @Runnable - TIMER = INTERRUPTED.");
                        break;
                    }
                    if (System.currentTimeMillis() > expiration) {
                        Log.i(TAG,"initProcAsyncAndTimeout() - @Runnable - TIMEOUT - TIMEOUT - TIMEOUT - TIMEOUT.");
                        if (null != mThreadProcAsyncRunnable) {
                            Log.i(TAG,"initProcAsyncAndTimeout() - @Runnable - RUNNABLE = INTERRUPTED.");
                            mThreadProcAsyncRunnable.interrupt();
                        }
                        if (null != mProcAsyncListener) {
                            Log.i(TAG,"initProcAsyncAndTimeout() - @Runnable - execute Timeout CALLBACK.");
                            mProcAsyncListener.onWifiResult( new WifiResult(WifiResult.X_TIMEOUT));
                        }
                        break;
                    }
                }

            }
        });

        Log.i(TAG,"initProcAsyncAndTimeout() - EXECUTE.");
        mThreadProcAsyncRunnable = new Thread(r);
        mThreadProcAsyncTimeout.start();
        mThreadProcAsyncRunnable.start();
        Log.i(TAG,"initProcAsyncAndTimeout() - Return.");
    }
-*/
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////
    //-/////////////////////////////////////////////////////////////////////////////////////////////





/*-
    private Thread mThreadTimeOutX;
    private interface TimeOutXListener { public void onTimedOut(); }
    private TimeOutXListener mTimeOutXListener;
    private void cancelTimeOutX() {
        mTimeOutXListener = null;
        if (null == mThreadTimeOutX) {
            return;
        }
        if (mThreadTimeOutX.isAlive()) {
            mThreadTimeOutX.interrupt();
        }
        mThreadTimeOutX = null;
    }
    private void setTimeOutAndListener(final int dt, TimeOutXListener li) {
        cancelTimeOutX();
        mTimeOutXListener = li;
        mThreadTimeOutX = new Thread(new Runnable() {
            @Override
            public void run() {
                long expiration = System.currentTimeMillis() + dt;
                for(;;) {
                    if (expiration < System.currentTimeMillis()) {
                        if (null != mTimeOutXListener) {
                            mTimeOutXListener.onTimedOut();
                        }
                        break;
                    }
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ie) {
                        break;
                    }
                }
            }
        });
    }
-*/













}

package com.stanleyblackanddecker.fisx;

import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FragmReconnectWait extends FragmBaseFragment {

    private final static String TAG = "{FragmReconnectWait}";


    private Runnable      mAnimationRunnable;
    private Handler       mAnimationHandler;
    private boolean       mAnimationIsRunning = false;
    private int           mAnimationCycleCount;

    private ImageView     mIvAnimHourglass;
    private LinearLayout  mProgressIndicaContainment;
    private LinearLayout  mProgressIndica;
    private int           mProgressIndicaHeight;
    private int           mProgressIndicaWidth;



    private final static float WAIT_FOR_MINUTES = (1.0f);
    private final static int   PROG_INDIC_MAX = (int)(WAIT_FOR_MINUTES * 60.0f * 8.0f);

    private int  mFrameNdx;
    private int  mFrameDeltaT [] = new int [] {
            120,120,120,120, 120,120,120,120
    };
    private int  mFrameDrawable [] = new int [] {
            R.drawable.anim_hourglass_0,
            R.drawable.anim_hourglass_1,
            R.drawable.anim_hourglass_2,
            R.drawable.anim_hourglass_3,
            R.drawable.anim_hourglass_4,
            R.drawable.anim_hourglass_5,
            R.drawable.anim_hourglass_6,
            R.drawable.anim_hourglass_7
    };


    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_reconnect_wait, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null"));
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);

        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "@COMMS-UPDATE---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__Edit);
            }
        });


        mIvAnimHourglass = (ImageView) rootView.findViewById(R.id.iv_anim_hourglass);
        mFrameNdx = (mFrameDrawable.length);


        mProgressIndicaContainment = (LinearLayout) rootView.findViewById(R.id.llProgressContainment);
        mProgressIndica = (LinearLayout) rootView.findViewById(R.id.llProgressIndica);


        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(7);


        TextView tv = (TextView) rootView.findViewById(R.id.dialog_tv_btn_cancel);
        if (null != tv) {
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBtnCancel();
                }
            });
        }




        Utils.LogDecorated(TAG, "  @Screen: RECONNECT WAIT  ");


        return rootView;
    }






    private void setProgressIndica(int count) {
        if (null == mProgressIndica) {
            return;
        }

        if (0 == mProgressIndicaWidth) {
            if (null != mProgressIndicaContainment) {
                mProgressIndicaWidth = mProgressIndicaContainment.getWidth();
                mProgressIndicaHeight = mProgressIndicaContainment.getHeight();
            } else {
                mProgressIndicaWidth = (308);
                mProgressIndicaHeight = (32);
            }
        }


        float f;
        if (count < PROG_INDIC_MAX) {
            f = ((float)(PROG_INDIC_MAX - count)) / ((float)PROG_INDIC_MAX + 10);
        } else {
            f = 0.00f;
        }
        int w = (int)(f * (float)mProgressIndicaWidth);


        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(w, mProgressIndicaHeight);
        mProgressIndica.setLayoutParams(parms);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");


        animationRunner();
    }




    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");

        mAnimationIsRunning = false;
    }





    private void animationRunner() {
        mAnimationHandler = new Handler();
        mAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (! mAnimationIsRunning) {
                    Log.i(TAG, "animationRunner() --> Animation NOT running. Return.");
                    return;
                }
                if ((++mFrameNdx) >= mFrameDrawable.length) {
                    mFrameNdx = (0);
                }
                mIvAnimHourglass.setImageResource(mFrameDrawable[mFrameNdx]);
                mAnimationHandler.postDelayed(this, (long)(mFrameDeltaT[mFrameNdx]));
                ++mAnimationCycleCount;


                setProgressIndica(mAnimationCycleCount);
                determine_if_complete();
            }
        };

        mAnimationHandler.postDelayed(mAnimationRunnable, 100);
        mAnimationCycleCount = (0);
        mAnimationIsRunning = true;
        Log.i(TAG, "animationRunner() - Starting animation");
    }



    private void onBtnCancel() {
        mFCmdDispatcher.switchto(R.id.id_app_command__PressButton);
    }


    private void determine_if_complete() {
        if (mAnimationCycleCount < PROG_INDIC_MAX) {
            return;
        }
        mFCmdDispatcher.switchto(R.id.id_app_command__Search);
    }


}

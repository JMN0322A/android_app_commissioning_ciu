package com.stanleyblackanddecker.fisx;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FragmAbout extends FragmBaseFragment {

    private final static String TAG = "{FragmAbout}";

    private TextView mTvBtnOk;



    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_about, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null") );
        mFTitleBar.enableBtnMenu(true);

        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnBack(false);
        mFTitleBar.enableBtnMenu(true);


        mTvBtnOk = (TextView) rootView.findViewById(R.id.btn_ok);
        if (null != mTvBtnOk) {
            mTvBtnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBtnOk();
                }
            });
        }


        Utils.LogDecorated(TAG, "  @Screen: ABOUT  ");

        return rootView;
    }



    private void onBtnOk() {

        //
        //
        Log.i(TAG, "onBtnOk()");
        if ((null != mFCmdDispatcher) && (null != mFAcContainer)) {
            mFCmdDispatcher.switchto(R.id.id_app_command__Start);
        }
        else {
            Log.i(TAG, "onBtnStart() - mFCmdDispatcher, or, mFAcContainer - is NULL.");
        }
    }


}



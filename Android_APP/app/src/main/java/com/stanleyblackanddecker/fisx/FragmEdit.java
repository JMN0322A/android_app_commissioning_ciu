package com.stanleyblackanddecker.fisx;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


///////// https://accesstech.atlassian.net/secure/RapidBoard.jspa?rapidView=2&projectKey=CIU&view=planning.nodetail&selectedIssue=CIU-471

public class FragmEdit extends FragmBaseFragment {

    private final static String TAG = "{FragmEdit}";

    private EditText mEtDataSSID;
    private EditText mEtDataPassword;


    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_edit, container, false);

        Logger.i(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null"));
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);

        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Logger.i(TAG, "@EDIT---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__Status);
            }
        });


        mEtDataSSID = (EditText) rootView.findViewById(R.id.et_data_ssid);
        mEtDataPassword = (EditText) rootView.findViewById(R.id.et_data_password);

        updateUI();



        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active));

        idp.setViewActive(5);


        TextView tv;
        ImageView iv;

        iv = (ImageView) rootView.findViewById(R.id.iv_triangle_back);
        if (null != iv) {
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vw) {
                    mFCmdDispatcher.switchto(R.id.id_app_command__Status);
                }
            });
        }


        tv = (TextView) rootView.findViewById(R.id.btn_save);
        if (null != tv) {
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vw) {
                    save_to_ciu();
                }
            });
        }


        Utils.LogDecorated(TAG, "  @Screen: EDIT  ");

        return rootView;
    }







    private void updateUI() {
        //////// extract from the payload received from CIU into array of Strings
        ArrayList<String> ars = Utils.extractStringArray(mFServiceData.mCIU_bytes_rx);

        //////// FIRST String (0) ==> SSID
        if (ars.size() >= 1) {
            if (null != mEtDataSSID) {
                mEtDataSSID.setText(ars.get(0));
            }
        }

        //////// SECOND String (1) ==> PASSWORD
        if (ars.size() >= 2) {
            if (null != mEtDataPassword) {
                mEtDataPassword.setText(ars.get(1));
            }
        }
    }





    private void save_to_ciu()
    {
        if ((null == mEtDataSSID) || (null == mEtDataPassword)) {
            Logger.i(TAG, "Edit-Text NOT FOUND");
            return;
        }
        String s_ssid = mEtDataSSID.getText().toString();
        String s_pswd = mEtDataPassword.getText().toString();

        if ((null == s_ssid) || (null == s_pswd)) {
            Logger.i(TAG, "Edit-Text NULL DATA");
            return;
        }

        int dataLength = s_ssid.length() + s_pswd.length() + (2);
        final byte [] txDat = new byte[ dataLength + (6) ];
        int j = 2;

        txDat[0] = ('*');
        txDat[1] = (byte)(dataLength);

        for (int i=0; i<s_ssid.length(); i++) {
            txDat[j++] = (byte)(s_ssid.charAt(i));
        }
        txDat[j++] = (byte)(0x00);

        for (int i=0; i<s_pswd.length(); i++) {
            txDat[j++] = (byte)(s_pswd.charAt(i));
        }
        txDat[j++] = (byte)(0x00);

        txDat[j] = (byte)(Utils.calculate_checksum(txDat, j));

        txDat[++j] = (byte)(0x00);
        txDat[++j] = (byte)(0x00);
        txDat[++j] = (byte)(0x00);

        save_to_ciu_Transmitt(txDat);

//        mFZDialog.clear();
//        mFZDialog.setTitle("SAVE");
//        mFZDialog.setMessage("==> " + Utils.byteArrrayToHexString(txDat));
//        mFZDialog.setModal(true);
//        mFZDialog.setSingleBtnMode(true);
//        mFZDialog.setDialogOkListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                save_to_ciu_Transmitt(txDat);
//            }
//        });
//        mFZDialog.show();
    }




    private void save_to_ciu_Transmitt(byte [] tx) {
        mFServiceData.mCIU_bytes_tx = tx;
        Logger.i(TAG, "save_to_ciu_Transmitt()-- => " + Utils.byteArrayToHexMixedString(mFServiceData.mCIU_bytes_tx));
        mFCmdDispatcher.switchto(R.id.id_app_command__CommsUpdate);
    }



}

package com.stanleyblackanddecker.fisx;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ZDialog {


    private final static String TAG = "{ZDialog}";


    private View.OnClickListener mDialogOkListener;

    private View.OnClickListener mDialogCancelListener;

    private String  mStrTitle;
    private String  mStrMessage;
    private String  mStrOk;
    private String  mStrCancel;
    private int     mRidImageSprite;
    private boolean mIsModal;
    private boolean mIsVisible;
    private boolean mSingleBtnMode;



    private View      mVwBtnCancel;
    private View      mVwBtnOK;
    private View      mVwImgSprite;
    private View      mVwTitle;
    private View      mVwMessage;
    private View      mVwDialogDarkener;
    private View      mVwDialogContainer;

    private Activity mAc;



    public ZDialog(Activity a) {
        if (null == (mAc = a)) {
            Log.d(TAG, "*ERROR**** Constructor with NULL - Activity ***********");
            return;
        }
        mVwBtnCancel = mAc.findViewById(R.id.dialog_tv_btn_cancel);
        mVwBtnOK = mAc.findViewById(R.id.dialog_tv_btn_ok);
        mVwImgSprite = mAc.findViewById(R.id.dialog_iv_sprite);
        mVwTitle = mAc.findViewById(R.id.dialog_tv_title);
        mVwMessage = mAc.findViewById(R.id.dialog_tv_message);
        mVwDialogContainer = mAc.findViewById(R.id.llContainer_dialog);
        mVwDialogDarkener = mAc.findViewById(R.id.llContainer_darkener);

        clear();
    }

    public Context getContext() {
        return (Context) mAc;
    }

    /**
     *  Clear() resets all parameters for the Dialog. NOTHING is changed on the screen.
     *  You should setup a dialog with clear(), setXXXX(), ... and THEN invoke show().
     */
    public ZDialog clear() {
        mDialogOkListener = null;
        mDialogCancelListener = null;
        mStrTitle = null;
        mStrMessage = null;
        mStrOk = null;
        mStrCancel = null;
        mRidImageSprite = (0);
        mIsModal = false;
        mIsVisible = false;
        mSingleBtnMode = false;
        return this;
    }

    public ZDialog setTitle(String s) {
        mStrTitle = s;
        return this;
    }
    public ZDialog setMessage(String s) {
        mStrMessage = s;
        return this;
    }
    public ZDialog setStrOk(String s) {
        mStrOk = s;
        return this;
    }
    public ZDialog setStrCancel(String s) {
        mStrCancel = s;
        return this;
    }
    public ZDialog setRidSprite(int rid) {
        mRidImageSprite = rid;
        return this;
    }
    public ZDialog setModal(boolean b) {
        mIsModal = b;
        return this;
    }
    public ZDialog setDialogOkListener(View.OnClickListener li) {
        mDialogOkListener = li;
        return this;
    }
    public ZDialog setDialogCancelListener(View.OnClickListener li) {
        mDialogCancelListener = li;
        return this;
    }

    public ZDialog setSingleBtnMode(boolean singleBtnMode) {
        mSingleBtnMode = singleBtnMode;
        return this;
    }


    /**
     *  Using the parameters PREVIOUSLY SET for this Dialog, this method makes the dialog
     *  visible on the screen and awaits interaction withe User.
     *
     */
    public void show() {
        Log.d(TAG, "show()- " + mStrTitle +" "+ mStrMessage +" "+ mStrOk +" "+ mStrCancel);

        //////// setup TITLE TEXT
        if (null != mVwTitle) {
            ((TextView)mVwTitle).setText(mStrTitle);
        }

        //////// setup MESSAGE TEXT
        if (null != mVwMessage) {
            ((TextView)mVwMessage).setText(mStrMessage);
        }

        //////// setup SPRITE IMAGE. IF ZERO(0), SET TO 'GONE'
        if (null != mVwImgSprite) {
            if (0 == mRidImageSprite) {
                mVwImgSprite.setVisibility(View.GONE);
            } else {
                ((ImageView)mVwImgSprite).setImageResource(mRidImageSprite);
                mVwImgSprite.setVisibility(View.VISIBLE);
            }
        }

        //////// setup Text, and Listener. for BUTTON - OK
        if (null != mVwBtnOK) {
            if (null != mStrOk) {
                ((TextView)mVwBtnOK).setText(mStrOk);
            } else {
                ((TextView)mVwBtnOK).setText("OK");
            }
            mVwBtnOK.setOnClickListener(mBtnOkListener);
        }

        //////// setup Text, and Listener for BUTTON - CANCEL
        if (null != mVwBtnCancel) {
            if (null != mStrCancel) {
                ((TextView)mVwBtnCancel).setText(mStrCancel);
            } else {
                ((TextView)mVwBtnCancel).setText("CANCEL");
            }
            mVwBtnCancel.setOnClickListener(mBtnCancelListener);
        }


        //////// process the arrangement of buttons, according to singular-mode, and color-override
        if (! mSingleBtnMode) {
            //////////////// THIS IS NOT SINGLE-BUTTON-MODE
            //////// Therefore, there IS a CANCEL button
            mVwBtnCancel.setVisibility(View.VISIBLE);
            //////// Cancel button
            mVwBtnCancel.setBackgroundResource(R.drawable.bkg_btn_dialog_cancel_selector);
            //////// There IS a POSITIVE button
            mVwBtnOK.setVisibility(View.VISIBLE);
            mVwBtnOK.setBackgroundResource(R.drawable.bkg_btn_dialog_ok_selector);
        } else {
            //////////////// THIS IS SINGLE-BUTTON-MODE, and it is the POSITIVE Button
            //////// Therefore, there IS NO CANCEL button
            mVwBtnCancel.setVisibility(View.GONE);
            //////// there IS a POSITIVE button
            mVwBtnOK.setVisibility(View.VISIBLE);
            mVwBtnOK.setBackgroundResource(R.drawable.bkg_btn_dialog_ok_selector);
        }





        //////// Make the DARKENING-LAYER become VISIBLE
        //////// Setup a TOUCH-Listener, to "eat" all the touch events, to prevent buttons in the
        //////// layers beneith from being touchable. This also provides another mechanism to
        //////// dismiss the Dialog, when this is NOT modal.
        if (null != mVwDialogDarkener) {
            mVwDialogDarkener.setVisibility(View.VISIBLE);
            final boolean bModal = mIsModal;
            mVwDialogDarkener.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d(TAG, "onTouchListener--darkener --- TOUCH");
                    if (! bModal) {
                        dismiss();
                    }
                    return true;
                }
            });
        }

        //////// Make the DIALOG CONTAINER become VISIBLE
        //////// Setup a TOUCH-Listener, to "eat" all the touch events, to prevent any layers below
        //////// from reacting to any touches. If the Darkenning layer below were to receive an
        //////// "un-eaten" touch event, it could cause a dialog-dismiss. Thus, we have to
        //////// ensure we "eat" these touch-events
        if (null != mVwDialogContainer) {
            mVwDialogContainer.setVisibility(View.VISIBLE);
            mVwDialogContainer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d(TAG, "onTouchListener--CONTAINER --- TOUCH");
                    return true;
                }
            });
        }



        mIsVisible = true;
    }


    /**
     * dismiss() HIDES the dialog from screen, turns off the darkenning layer, and clears
     * all the registered listeners.
     */
    public void dismiss() {
        //if (null != mVwBtnX) {
        //    mVwBtnOK.setOnClickListener(null);
        //}
        if (null != mVwBtnOK) {
            mVwBtnOK.setOnClickListener(null);
        }
        if (null != mVwBtnCancel) {
            mVwBtnCancel.setOnClickListener(null);
        }
        if (null != mVwDialogContainer) {
            mVwDialogContainer.setOnClickListener(null);
            mVwDialogContainer.setVisibility(View.GONE);
            Log.d(TAG, "dismis()- dialog GONE");
        }
        if (null != mVwDialogDarkener) {
            mVwDialogDarkener.setOnClickListener(null);
            mVwDialogDarkener.setVisibility(View.GONE);
            Log.d(TAG, "dismis()- darkener GONE");
        }
        mSingleBtnMode = false;
        mIsVisible = false;
    }



    public boolean isVisible() {
        return mIsVisible;
    }
    public boolean isModal() {
        return mIsModal;
    }

    public View.OnClickListener mBtnOkListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
            if (null != mDialogOkListener) {
                Log.d(TAG,"onCLick -Ok >>Listener!");
                mDialogOkListener.onClick(v);
            }
        }
    };

    public View.OnClickListener mBtnCancelListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
            if (null != mDialogCancelListener) {
                Log.d(TAG,"onCLick -Cancel >>Listener!");
                mDialogCancelListener.onClick(v);
            }
        }
    };


}

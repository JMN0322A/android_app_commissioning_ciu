package com.stanleyblackanddecker.fisx;



import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MenuDrawer {

    private static final String TAG = "{MenuDrawer}";

    private final int MENU_SLIDE_DURATION = 250;

    private Activity     mAc;
    private int          mScreenWidth = 480;
    private boolean      mIsMenuDrawerDisplayed = false;
    private boolean      mIsAnimationInProgress = false;
    private float        mMenuPositionInitial = -360.0f;

    private View         mVwBkgDark;
    private LinearLayout mllContainerMenu;
    private ImageView    mIvCloser;


    public MenuDrawer(Activity a, int width) {
        mAc = a;
        mScreenWidth = width;

        mMenuPositionInitial = -0.7f * (float) mScreenWidth;
        mIvCloser = (ImageView) mAc.findViewById( R.id.id_btn_menu_close );
        mVwBkgDark = mAc.findViewById(R.id.llContainer_darkener);
        mllContainerMenu = (LinearLayout) mAc.findViewById(R.id.llContainer_menu);
    }

    public boolean isVisible() {
        return mIsMenuDrawerDisplayed;
    }

    public void show() {
        menuDrawerShowDismiss(true);
    }
    public void dismiss() {
        menuDrawerShowDismiss(false);
    }





    private void menuDrawerShowDismiss(boolean bShow) {
        if (null == mAc) {
            Log.d(TAG, "menuDrawerShowDismiss()- Activity NULL");
            return;
        }
        if ((null == mVwBkgDark) || (null == mllContainerMenu) || (null == mIvCloser)) {
            Log.d(TAG, "menuDrawerShowDismiss()- layout ID's not found");
            return;
        }

        if (bShow) {
            //////// set the animation in-progress flag
            mIsAnimationInProgress = true;
            //////// setup the animator
            TranslateAnimation animSlide_in = new TranslateAnimation(mMenuPositionInitial,0.0f, 0.0f,0.0f);
            animSlide_in.setDuration(MENU_SLIDE_DURATION);
            animSlide_in.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) { }
                @Override
                public void onAnimationRepeat(Animation animation) { }
                @Override
                public void onAnimationEnd(Animation animation) {
                    mIsAnimationInProgress = false;
                    mIsMenuDrawerDisplayed = true;
                }
            });
            //////// initialize the visibilities
            mVwBkgDark.setVisibility(View.VISIBLE);
            mllContainerMenu.setVisibility(View.VISIBLE);
            //////// setup the menu closer listener
            mIvCloser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuDrawerShowDismiss(false);
                }
            });
            //////// consume all touch events, outside the menu, within the darkening layer
            mVwBkgDark.setOnTouchListener(new View.OnTouchListener(){
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d(TAG,"Darkener-Layer Touch-EVENT");
                    menuDrawerShowDismiss(false);
                    return true;
                }
            });
            mllContainerMenu.startAnimation(animSlide_in);

        } else {
            //////// set the animation in-progress flag
            mIsAnimationInProgress = true;
            //////// setup the animator
            TranslateAnimation animSlide_out = new TranslateAnimation(0.0f,mMenuPositionInitial, 0.0f,0.0f);
            animSlide_out.setDuration(MENU_SLIDE_DURATION);
            animSlide_out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) { }
                @Override
                public void onAnimationRepeat(Animation animation) { }
                @Override
                public void onAnimationEnd(Animation animation) {
                    mVwBkgDark.setVisibility(View.GONE);
                    mllContainerMenu.setVisibility(View.GONE);
                    mIsAnimationInProgress = false;
                    mIsMenuDrawerDisplayed = false;

                }
            });
            //////// setup the listeners for touch events
            mVwBkgDark.setOnTouchListener(null);
            mllContainerMenu.setOnTouchListener(null);
            //////// start the animation
            mllContainerMenu.startAnimation(animSlide_out);
        }

    }


}

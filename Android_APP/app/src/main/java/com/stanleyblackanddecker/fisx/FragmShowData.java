package com.stanleyblackanddecker.fisx;


import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class FragmShowData extends FragmBaseFragment {

    private final static String TAG = "{FragmShowData}";

    private final static String STR_DT_LAST_WIFI   = "date last Wifi connect  ";
    private final static String STR_DT_LAST_CLOUD  = "date last Cloud connect  ";

    private TextView  mTvDataSSID;
    private TextView  mTvDataPSWD;
    private TextView  mTvDataDateWifi;
    private TextView  mTvDataDateCloud;
    private TextView  mTvDateDeltaWifi;
    private TextView  mTvDateDeltaCloud;
    private LinearLayout mllCheckboxIdle;
    private LinearLayout mllCheckboxActive;

    private String    mStrRX;
    private boolean   mFlagIsCompleted = false;
//    private int       mAutoReconnect   = (1);


    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_show_data, container, false);

        Logger.i(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null"));
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);

        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Logger.i(TAG, "@SHOW-DATA---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__Reconnect);
            }
        });


        mTvDataSSID       = (TextView) rootView.findViewById(R.id.tv_ap_ssid);
        mTvDataPSWD       = (TextView) rootView.findViewById(R.id.tv_ap_password);
        mTvDataDateWifi   = (TextView) rootView.findViewById(R.id.tv_tm_wifi);
        mTvDataDateCloud  = (TextView) rootView.findViewById(R.id.tv_tm_cloud);
        mTvDateDeltaWifi  = (TextView) rootView.findViewById(R.id.tv_tm_delta_wifi);
        mTvDateDeltaCloud = (TextView) rootView.findViewById(R.id.tv_tm_delta_cloud);
        mllCheckboxIdle   = (LinearLayout) rootView.findViewById(R.id.ll_checkbox_reconnect_idle);
        mllCheckboxActive = (LinearLayout) rootView.findViewById(R.id.ll_checkbox_reconnect_active);

        //////// set the Progress Indicator
        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(5);




        //////// setup ON-CLICK events for the two buttons
        TextView tv;

        //////// EDIT BUTTON
        tv = (TextView) rootView.findViewById(R.id.btn_edit);
        if (null != tv) {
            tv.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View vw) {
                    mFCmdDispatcher.switchto(R.id.id_app_command__Edit);
                }
            });
        }

        //////// DISCONNECT BUTTON
        tv = (TextView) rootView.findViewById(R.id.btn_disconnect);
        if (null != tv) {
            tv.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View vw) {
                    onBtnDisconnect();
                }
            });
        }


        //////// setup ON-CLICK events for the checkboxes
        if (null != mllCheckboxActive) {
            mllCheckboxActive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickCheckboxActive();
                }
            });
        }

        if (null != mllCheckboxIdle) {
            mllCheckboxIdle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickCheckboxIdle();
                }
            });
        }

        mFServiceData.mWaitForReconnect = (1);


        Utils.LogDecorated(TAG, "  @Screen: SHOW DATA  ");

        return rootView;
    }


    @Override
    public void onResume()  {
        super.onResume();

        updateUI();
    }



    private void onBtnDisconnect() {
        StringBuilder sb = new StringBuilder();
        if ((0) == mFServiceData.mWaitForReconnect) {
            sb.append("* APP will disconnect from CIU.\n");
            sb.append("* CIU will connect\n");
            sb.append("   to Wifi Access Point\n");
            sb.append("   and  The Cloud.\n");
            sb.append("* There are no more changes\n");
            sb.append("   to configuration.\n");
        }
        else {
            sb.append("* APP will disconnect from CIU.\n");
            sb.append("* CIU will attempt to connect\n");
            sb.append("   to Wifi Access Point.\n");
            sb.append("   and  The Cloud.\n");
            sb.append("* CIU will reconnect with APP\n");
            sb.append("   to view results.");
        }

        mFZDialog.clear();
        mFZDialog.setModal(false);
        mFZDialog.setTitle("DISCONNECT");
        mFZDialog.setMessage(sb.toString());
        mFZDialog.setDialogOkListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendExit_TcpIpComms();
////////////////mFCmdDispatcher.switchto(R.id.id_app_command__Reconnect);
            }
        });
        mFZDialog.show();
    }


    private void sendExit_TcpIpComms()
    {
        Logger.i(TAG, "sendExit_TcpIpComms()- setting up for ((((.--Exit))))");
        final byte [] txDat = new byte[ 7 ];

        txDat[0] = ('.');        //////// (byte)(0x2e) is ascii period(.)
        txDat[1] = (byte)(0x01);
        txDat[2] = (byte)(mFServiceData.mWaitForReconnect);

        txDat[3] = (byte)(Utils.calculate_checksum(txDat, 3));

        txDat[4] = (byte)(0x00);
        txDat[5] = (byte)(0x00);
        txDat[6] = (byte)(0x00);

        mFServiceData.mCIU_bytes_tx = (txDat);



        mFSpinningWait.clear();
        mFSpinningWait.setMessageText("Sending Disconnect");
        mFSpinningWait.show();



        mStrRX = " ...";
        mFWifiProcs.tcp_comms_setListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                mFlagIsCompleted = true;
                int n = result.getCode();
                Logger.i(TAG, "sendExit_TcpIpComms()- "+ n +" "+ WifiResult.codeToString(n) + " " + result.getStr());
                ////Utils.LogDecorated(TAG, " exit-TCPcomms "+result.getStr()+" ");
                if (WifiResult.X_SUCCESS == n) {
                    byte [] ar = (byte [])result.getData();
                    mStrRX = Utils.byteArrayToHexMixedString(ar);
                } else {
                    mStrRX = "*Error- " + n + " " + WifiResult.codeToString(n);
                }
                Utils.LogDecorated(TAG, " exit-TCPcomms "+mStrRX+" ");
                navigateNext();
            }
        });
        Logger.i(TAG,"sendExit_TcpIpComms()- BTX=" + Utils.byteArrrayToHexString(mFServiceData.mCIU_bytes_tx));
        mFWifiProcs.tcp_comms(mFServiceData.mCIU_bytes_tx);
    }


    private void navigateNext() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                mFSpinningWait.dismiss();
                mFCmdDispatcher.switchto(R.id.id_app_command__Reconnect);

//                Handler handler = new Handler();
//                Runnable r = new Runnable() {
//                    @Override
//                    public void run() {
//                        mFSpinningWait.dismiss();
//                        mFCmdDispatcher.switchto(R.id.id_app_command__Reconnect);
//                    }
//                };
//                handler.postDelayed(r, 3000);
//                mFSpinningWait.dismiss();
//                mFCmdDispatcher.switchto(R.id.id_app_command__Reconnect);
//                mFSpinningWait.dismiss();
////                mFSpinningWait.setMessageText("Wait...");
////                mFSpinningWait.show();
            }
        });
    }







    private void updateUI() {
        //////// extract from the payload received from CIU into array of Strings
        ArrayList<String> ars = Utils.extractStringArray(mFServiceData.mCIU_bytes_rx);

        //////// FIRST String (0) ==> SSID
        if (ars.size() >= 1) {
            if (null != mTvDataSSID) {
                mTvDataSSID.setText(ars.get(0));
            }
        }

        //////// SECOND String (1) ==> PASSWORD
        if (ars.size() >= 2) {
            if (null != mTvDataPSWD) {
                mTvDataPSWD.setText(ars.get(1));
            }
        }

        //////// THIRD String (2) ==> DATE-LAST-WIFI-CONNECTED
        if (ars.size() >= 3) {
            String s = ars.get(2);
            if ((null == s) || (s.equals("0"))) {
                if (null != mTvDataDateWifi) {
                    mTvDataDateWifi.setText(" ---");
                }
                if (null != mTvDateDeltaWifi) {
                    mTvDateDeltaWifi.setText(STR_DT_LAST_WIFI);
                }
            } else {
                if (null != mTvDataDateWifi) {
                    mTvDataDateWifi.setText(Utils.extractDateFromNumberString(s));
                }
                if (null != mTvDateDeltaWifi) {
                    mTvDateDeltaWifi.setText(
                            STR_DT_LAST_WIFI + "(" + Utils.diffTime(s) + ")");
                }
            }
        }


        //////// FOURTH String (3) ==> DATE-LAST-CLOUD-CONNECTED
        if (ars.size() >= 4) {
            String s = ars.get(3);
            if (null != mTvDataDateCloud) {
                if ((null != s) && (s.equals("0"))) {
                    if (null != mTvDataDateCloud) {
                        mTvDataDateCloud.setText(" ---");
                    }
                    if (null != mTvDateDeltaCloud) {
                        mTvDateDeltaCloud.setText(STR_DT_LAST_CLOUD);
                    }
                } else {
                    if (null != mTvDataDateCloud) {
                        mTvDataDateCloud.setText(Utils.extractDateFromNumberString(s));
                    }
                    if (null != mTvDateDeltaCloud) {
                        mTvDateDeltaCloud.setText(
                                STR_DT_LAST_CLOUD + "(" + Utils.diffTime(s) + ")");
                    }
                }
            }
        }

    }


    private void onClickCheckboxIdle() {
        if ((null != mllCheckboxIdle) && (null != mllCheckboxActive)) {
            mllCheckboxIdle.setVisibility(View.GONE);
            mllCheckboxActive.setVisibility(View.VISIBLE);
            mFServiceData.mWaitForReconnect = (1);
        }
    }
    private void onClickCheckboxActive() {
        if ((null != mllCheckboxIdle) && (null != mllCheckboxActive)) {
            mllCheckboxActive.setVisibility(View.GONE);
            mllCheckboxIdle.setVisibility(View.VISIBLE);
            mFServiceData.mWaitForReconnect = (0);
        }
    }




}

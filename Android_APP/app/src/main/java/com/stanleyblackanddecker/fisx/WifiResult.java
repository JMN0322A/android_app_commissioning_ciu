package com.stanleyblackanddecker.fisx;



public class WifiResult {


    public static final int X_NO_RESULT   = (0);
    public static final int X_FAIL        = (1);
    public static final int X_SUCCESS     = (2);
    public static final int X_CANCELLED   = (3);
    public static final int X_TIMEOUT     = (4);

    public static String codeToString(int n) {
        switch (n) {
            case X_NO_RESULT: return "NO-Result";
            case X_FAIL:      return "Result-Fail";
            case X_SUCCESS:   return "Result-Success";
            case X_CANCELLED: return "Result-Cancelled";
            case X_TIMEOUT:   return "Timed-Out-Result";
        }
        return "Unknown-Result(" + n + ")";
    }

    private int     mCode;
    private String  mStr;
    private Object  mData;


    public WifiResult () {
        mCode = (X_NO_RESULT);
        mStr  = (null);
        mData = (null);
    }

    public WifiResult (int m) {
        mCode = (m);
        mStr  = (null);
        mData = (null);
    }

    public WifiResult (int m, String s) {
        mCode = (m);
        mStr  = (s);
        mData = (null);
    }

    public WifiResult (int m, String s, Object o) {
        mCode = (m);
        mStr  = (s);
        mData = (o);
    }

    public int    getCode() { return mCode; }
    public String getStr()  { return mStr;  }
    public Object getData() { return mData; }

    public void  setCode(int n)    { mCode = (n); }
    public void  setStr(String s)  { mStr  = (s); }
    public void  setData(Object o) { mData = (o); }

}

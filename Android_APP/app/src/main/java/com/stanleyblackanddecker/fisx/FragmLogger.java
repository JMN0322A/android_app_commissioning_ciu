package com.stanleyblackanddecker.fisx;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmLogger extends FragmBaseFragment {

    private final static String TAG = "{FragmLogger}";




    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_logger, container, false);

        Logger.i(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null") );
        mFTitleBar.enableBtnMenu(true);

        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnBack(false);
        mFTitleBar.enableBtnMenu(true);


        Utils.LogDecorated(TAG, "  @Screen: LOGGER  ");

        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                mFCmdDispatcher.switchto(R.id.id_app_command__About);
            }
        });


        TextView tv = rootView.findViewById(R.id.tv_text);
        if (null != tv) {
            tv.setText(Logger.getText());
        }


        return rootView;
    }


}

package com.stanleyblackanddecker.fisx;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;


public class FragmSearch extends FragmBaseFragment {

    private final static String TAG = "{FragmSearch}";
    private final int ANIM_DURATION = 667;
    private final int CYCLE_COUNT_ANIMATION_MINIMUM = (15);

    private ImageView  mIvSearching;
    private ImageView  mIvWifiFansIdle;
    private ImageView  mIvWifiFansActive;
    private TextView   mTvStatus;
    private int        mCycleCountAnimations;


    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_search, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null"));
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);


        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "@SEARCH---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__PressButton);
            }
        });


        mIvSearching = (ImageView) rootView.findViewById(R.id.iv_searching);
        mIvWifiFansIdle = (ImageView) rootView.findViewById(R.id.iv_wifi_fans_idle);
        mIvWifiFansActive = (ImageView) rootView.findViewById(R.id.iv_wifi_fans_active);
        mTvStatus = (TextView) rootView.findViewById(R.id.tv_status);

        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(2);



        clearCIUsearchResults();
        if (null != mFWifiProcs) {
            mFWifiProcs.scanWifiForCIU_start();
            Log.i(TAG, "onCreateView()- Searching started.");
        }



        Utils.LogDecorated(TAG, "  @Screen: SEARCH  ");

        return rootView;
    }


    private void clearCIUsearchResults() {
        Log.i(TAG, "clearCIUsearchResults() - Clear CIU search results.");
        mFServiceData.mCIU_mac  = null;
        mFServiceData.mCIU_ssid = null;
        mFServiceData.mCIU_rssi = 0;
        mFServiceData.mCIU_password = null;
        mCycleCountAnimations  = (0);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");

        if (null != mIvWifiFansIdle) {
            mIvWifiFansIdle.setVisibility(View.VISIBLE);
        }
        if (null != mIvWifiFansActive) {
            mIvWifiFansActive.setVisibility(View.GONE);
        }

        if (null != mFWifiProcs) {
            setupAnimation(mIvSearching);
        }
    }


    private void inquire_didWifiScanFindCIU() {
        WifiProcs.ScanWifiItem si;
        if (null == (si = mFWifiProcs.getScanWifiItem_CIU())) {
            return;
        }

        if (null != mIvWifiFansActive) {
            mIvWifiFansActive.setVisibility(View.VISIBLE);
        }
        if (null != mIvWifiFansIdle) {
            mIvWifiFansIdle.setVisibility(View.GONE);
        }
        updateUI(si);



        Log.i(TAG,"inquire_didWifiScanFindCIU()- ("+mCycleCountAnimations+") "
                +si.mSSID+ " **** " +si.mBSSID+ " **** " +si.mFreq+ "mHz " +si.mRssi+ "dBm ****");



        if (mCycleCountAnimations < CYCLE_COUNT_ANIMATION_MINIMUM) {
            return;
        }

        mFServiceData.mCIU_mac  = si.mBSSID;
        mFServiceData.mCIU_ssid = si.mSSID;
        mFServiceData.mCIU_rssi = si.mRssi;
        mFServiceData.mCIU_password = extractPassword(si.mBSSID, si.mSSID);

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
if (si.mSSID.contains("SAT_XxxxxxxxxxZzzzzzzzzz00")) {    //////////////////////////////////////////
    mFServiceData.mCIU_password = "0000000000";           //////////////////////////////////////////
}                                                         //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////



        Log.i(TAG,"inquire_didWifiScanFindCIU() - mac  = " + mFServiceData.mCIU_mac);
        Log.i(TAG,"inquire_didWifiScanFindCIU() - ssid = " + mFServiceData.mCIU_ssid);
        Log.i(TAG,"inquire_didWifiScanFindCIU() - rssi = " + mFServiceData.mCIU_rssi);
        Log.i(TAG,"inquire_didWifiScanFindCIU() - MINIMUM ANIMATIONS COUNTED");
        Log.i(TAG,"inquire_didWifiScanFindCIU() - NEXT FRAGMENT = CONNECT");

        Log.i(TAG,"inquire_didWifiScanFindCIU() - UN-register the BCastRXer...");
        mFWifiProcs.scanWifiForCIU_Stop();

        mFCmdDispatcher.switchto(R.id.id_app_command__Connect);
    }



    private String extractPassword(String mac, String ssid) {
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************----i");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************---ii");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************--iii");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************---iv");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************----v");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************---vi");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************--vii");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************-viii");


        byte [] byte_ciuMac = Utils.hexStringToByteArray(mac);
        Log.i(TAG, "extractPassword()- byte_ciuMac[] ==> " + Utils.byteArrrayToHexString(byte_ciuMac));

        if ((null == ssid) || (ssid.length() < 20)) {
            Log.i(TAG, "extractPassword()- *Error- BAD SSID = " + ssid);
            return ("0000000000");
        }
        String str_ssid = ssid.substring(4);
        Log.i(TAG, "extractPassword()- SSID@4 ==> " + str_ssid);

        byte [] data_ssid = Utils.brascii64b_decode(str_ssid);
        Log.i(TAG, "extractPassword()- SSID@4 =======> " + Utils.byteArrrayToHexString(data_ssid));

        //////// build xor key
        byte [] byte_xorKey = new byte[16];
        byte [] byte_passwd = new byte[16];

        for (int i=0; i<6; i++) {
            byte_xorKey[i] = byte_ciuMac[i];
        }
        for (int i=6, j=0;  i<16;  i++, j++) {
            byte_xorKey[i] = Utils.mKkey[j];
        }
        Log.i(TAG, "extractPassword()- xorKey =======> " + Utils.byteArrrayToHexString(byte_xorKey));

        for (int i=0; i<16; i++) {
            byte_passwd[i] = (byte)((short)byte_xorKey[i]  ^  (short)(data_ssid[i]));
        }
        Log.i(TAG, "extractPassword()- byte-passwd ==> " + Utils.byteArrrayToHexString(byte_passwd));

        String strDecodedPasswd_withEnding = Utils.brascii64b_encode(byte_passwd);
        String strDecodedPasswd = strDecodedPasswd_withEnding.substring(0,22);
        Log.i(TAG, "extractPassword()- Password ==> " + strDecodedPasswd);
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************-ix");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************-x");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************-xi");
        Log.i(TAG,"extractPassword()- " + mac + " ** " + ssid + " ********************-xii");

        return (strDecodedPasswd);
    }



    private void setupAnimation(final ImageView iv) {
        if (null == iv) {
            Log.i(TAG, "setupAnimation() - iv_searching not found");
            return;
        }

        final Animation anim = new AnimationPathCircular(iv, 120);
        anim.setDuration(ANIM_DURATION);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ++mCycleCountAnimations;
                iv.startAnimation(anim);
                inquire_didWifiScanFindCIU();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        iv.startAnimation(anim);
        mCycleCountAnimations = (0);
    }






    private void updateUI(WifiProcs.ScanWifiItem si)  {
        if ((null == mTvStatus) || (null == si)) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("mac: ");
        sb.append(si.mBSSID);
        sb.append("  ");
        sb.append(si.mRssi);
        sb.append("dBm\n");
        sb.append(si.mSSID);
        sb.append(mFServiceData.mCIU_password);
        mTvStatus.setText(sb.toString());
    }



}































/*----
        int top = iv.getTop();
        int bottom = iv.getBottom();
        int left = iv.getLeft();
        int right = iv.getRight();
        int w = iv.getWidth();
        int h = iv.getHeight();
        Log.i(TAG, "------- " + w +" "+ h +" "+ top +" "+ bottom +" "+ left +" "+ right);
        //////// ----- 910 1763     672  2435     265 1175
----*/

/*---------------------------
        final Animation animMoveDown = AnimationUtils.loadAnimation(mFAcContainer.getApplicationContext(), R.anim.search_move_down);
        final Animation animMoveUp   = AnimationUtils.loadAnimation(mFAcContainer.getApplicationContext(), R.anim.search_move_up);
        animMoveDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i(TAG,"{animationListener}-onAnimationEnd()");
                iv.startAnimation(animMoveUp);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        animMoveUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i(TAG,"{animationListener}-onAnimationEnd()");
                iv.startAnimation(animMoveDown);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        iv.clearAnimation();
        iv.startAnimation(animMoveDown);
---------------------------*/

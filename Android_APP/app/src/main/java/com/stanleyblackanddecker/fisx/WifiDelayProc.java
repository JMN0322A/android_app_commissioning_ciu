package com.stanleyblackanddecker.fisx;

import android.util.Log;

public class WifiDelayProc {

    private final static String TAG = "{WifiDelayProc}";

    private long               mTimeoutDuration;
    private Thread             mThreadDelay;
    private WifiResultListener mWifiListener;


    public WifiDelayProc() {
        Log.i(TAG, "Constructor --- !");
        clear();
    }


    public WifiDelayProc clear() {
        mTimeoutDuration = (0);
        mThreadDelay     = (null);
        mWifiListener    = (null);

        return (this);
    }


    public WifiDelayProc setDurationTime(int dt) {
        if (dt <= 0) {
            mTimeoutDuration = (1000);
            Log.i(TAG, "setTimeDurationTime() - (dt < 0)===>1000 **** ");
        } else {
            mTimeoutDuration = (long)dt;
            Log.i(TAG, "setTimeDurationTime() - dt = " + dt);
        }

        return (this);
    }


    public WifiDelayProc setWifiListener(WifiResultListener li) {
        mWifiListener = (li);
        if (null == li) {
            Log.i(TAG, "setWifiListener() - NULL - NULL - NULL");
        } else {
            Log.i(TAG, "setWifiListener() - " + li.toString());
        }

        return (this);
    }


    public WifiDelayProc cancel()
    {
        Log.i(TAG,"cancel() - Begin.");
        if(null != mThreadDelay) {
            if (mThreadDelay.isAlive()) {
                Log.i(TAG,"cancel() - Timeout=ALIVE. Interrupt!");
                mThreadDelay.interrupt();
            }
            mThreadDelay = null;
        }
        Log.i(TAG,"cancel() - Return.");

        return (this);
    }



    public boolean initiate()
    {
        if (mTimeoutDuration <= 0) {
            Log.i(TAG,"initiate() - *Error- BAD DURATION TIME");
            return false;
        }
        if (null == mWifiListener) {
            Log.i(TAG,"initiate() - *Error- MISSING RESULT LISTENER");
            return false;
        }

        Log.i(TAG,"initiate() - Cancel ongoning operation");
        cancel();

        Log.i(TAG,"initiate() - Create THREAD for Delay");
        mThreadDelay = new Thread(new Runnable() {
            @Override
            public void run() {
                boolean bWasInterrupted = false;
                Log.i(TAG,"initiate() - @Runnable-Delay.");
                try {
                    Thread.sleep(mTimeoutDuration);
                } catch (InterruptedException ie) {
                    bWasInterrupted = true;
                    Log.i(TAG, "initiate() - @Runnable-Delay - INTERRUPTED !!!!!!!!!!");
                }
                Log.i(TAG,"initiate() - @Runnable-Delay---END.");
                if (! bWasInterrupted) {
                    if (null != mWifiListener) {
                        mWifiListener.onWifiResult(new WifiResult(WifiResult.X_SUCCESS));
                    }
                }
                return;
            }
        });

        if (null == mThreadDelay) {
            Log.i(TAG, "initiate() - *Error- Can't create timeout Thread");
            return false;
        }

        Log.i(TAG,"initiate() - EXECUTE.");
        mThreadDelay.start();
        Log.i(TAG,"initiate() - Return.");

        return true;
    }






}

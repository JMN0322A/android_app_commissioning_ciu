package com.stanleyblackanddecker.fisx;


import android.app.Fragment;

public class FragmBaseFragment extends Fragment {

    protected ActivityContainer mFAcContainer;
    protected TitleBar          mFTitleBar;
    protected MenuDrawer        mFMenuDrawer;
    protected SpinningWait      mFSpinningWait;
    protected ZDialog           mFZDialog;
    protected CommandDispatcher mFCmdDispatcher;
    protected WifiProcs         mFWifiProcs;
    protected AppServiceData    mFServiceData;



    protected void initializeBase() {
        mFAcContainer = (ActivityContainer) getActivity();

        mFTitleBar      = mFAcContainer.mTitleBar;
        mFMenuDrawer    = mFAcContainer.mMenuDrawer;
        mFSpinningWait  = mFAcContainer.mSpinningWait;
        mFZDialog       = mFAcContainer.mZDialog;
        mFCmdDispatcher = mFAcContainer.mCommandDispatcher;
        mFWifiProcs     = mFAcContainer.mServiceConnector.getWifiProcs();
        mFServiceData   = mFAcContainer.mServiceConnector.getData();

        mFAcContainer.mTitleBar.setOnClickBtnBack(null);
        mFAcContainer.mTitleBar.enableBtnBack(false);
        mFAcContainer.mTitleBar.enableBtnSettings(false);
        mFAcContainer.mTitleBar.enableBtnMenu(false);
    }

}

package com.stanleyblackanddecker.fisx;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.List;

public class UtilNetwork {


    private static final String TAG = "{UtilNetwork}";

    public static final int NOT_CONNECTED = 0;
    public static final int WIFI = 1;
    public static final int MOBILE = 2;

    public static int getConnectionStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return WIFI;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return MOBILE;
        }
        return NOT_CONNECTED;
    }




    public static int getConnectedState(Context context) {

        if (context == null)
            return (-1);
        int networkState = getConnectionStatus(context);
////////Log.i(TAG, "getConnectedState()- networkState:" + networkState);////////////////////////////

        return networkState;
    }




    public static String getConnectedSSID(Context context) {

        if (context == null)
            return null;
        String networkName = null;
        int networkState = getConnectionStatus(context);
////////Log.i(TAG, "getConnectedSSID()- Network State:" + networkState);////////////////////////////
        if (networkState == UtilNetwork.WIFI) { //no wifi connection and alert dialog allowed
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null) {
                    networkName = wifiInfo.getSSID().replaceAll("\"", "");
////////////////////Log.i(TAG, "getConnectedSSID()- networkName == " + networkName);////////////////
                }
            }
        }
        if (networkName == null || networkName.equals("<unknown ssid>") || networkName.equals("0x") || networkName.equals("")) {
            networkName = null;
        }

        return networkName;
    }



    public static String getConnectionStatusString (Context context) {
        int connectionStatus = UtilNetwork.getConnectionStatus(context);
        if (connectionStatus == UtilNetwork.WIFI)
            return "Connected to Wifi";
        if (connectionStatus == UtilNetwork.MOBILE)
            return "Connected to Mobile Data";
        return "No internet connection";
    }




    public static String getWifiName (Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String wifiName = wifiManager.getConnectionInfo().getSSID();
        if (wifiName != null){
            if (!wifiName.contains("unknown ssid") && wifiName.length() > 2){
                if (wifiName.startsWith("\"") && wifiName.endsWith("\""))
                    wifiName = wifiName.subSequence(1, wifiName.length() - 1).toString();
                return wifiName;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }



    public static String getGateway (Context context) {
        WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return UtilNetwork.intToIp(wm.getDhcpInfo().gateway);
    }



    public static String intToIp(int i) {
        return ((i & 0xFF ) + "." +
                ((i >> 8 ) & 0xFF) + "." +
                ((i >> 16 ) & 0xFF) + "." +
                ((i >> 24 ) & 0xFF) ) ;
    }



    public static void startScan(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.startScan();
    }




    public static WifiManager getWifiManager(Context context) {
        return (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }




    public static void connectToKnownWifi(Context context, String ssid) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            if (i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
            }
        }
    }





    public static Boolean connectToWifiAfterDisconnecting(Context context, String ssid) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wc = new WifiConfiguration();
        wc = new WifiConfiguration();
        wc.SSID = "\"" + ssid + "\"";
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        wifiManager.addNetwork(wc);

        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            if (i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                wifiManager.enableNetwork(i.networkId, true);
                Boolean flag = wifiManager.reconnect();
                return flag;
            }
        }

        return false;
    }



}

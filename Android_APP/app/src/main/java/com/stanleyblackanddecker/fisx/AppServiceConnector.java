package com.stanleyblackanddecker.fisx;



import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public class AppServiceConnector implements IAppServiceConnector {

    private final static String TAG = "{AppServiceConnector}";

    private Context            mAppContext;
    private ReadinessListener  mServiceReadinessListener;
    private AppService         mAppService;



    public interface ReadinessListener {
        public void onReady();
    }

    public void setServiceReadinessListener(ReadinessListener li) {
        mServiceReadinessListener = li;
    }


    public AppServiceConnector(Context ctx) {
        mAppContext = ctx.getApplicationContext();
        mServiceReadinessListener = null;
        mAppService = null;

        //////// start the service
        Intent intent = new Intent(mAppContext, AppService.class);
        mAppContext.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG,"ServceConnection - onServiceConnected() - \""+name.toString()+"\"");
            if (null == service) {
                Log.e(TAG,"ServceConnection - onServiceConnected() - NULL == IBINDER-SERVICE");
                return;
            }

            mAppService = ((AppService.LocalBinder)service).getService();

            Log.d(TAG,"ServceConnection - onServiceConnected() - Service is Running.");
            if (null != mServiceReadinessListener) {
                mServiceReadinessListener.onReady();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mAppService = null;
            Log.e(TAG,"ServceConnection - onServiceDisconnected() - \""+name.toString()+"\"");
        }
    };


    public boolean initialize(Activity a, ZDialog dlg) {
        if (null == mAppService) {
            Log.i(TAG,"initialize() - Service is *NOT* Ready");
            return false;
        }
        mAppService.initialize(a, dlg);
        Log.i(TAG,"initialize() - Service Ready");

        return true;
    }


    public WifiProcs getWifiProcs() {
        if (null == mAppService)  {
            Log.i(TAG,"getWifiProcs() - Service is *NOT* Ready");
            return null;
        }
        return mAppService.getWifiProcs();
    }


    public AppServiceData getData() {
        if (null == mAppService)  {
            Log.i(TAG,"getData() - Service is *NOT* Ready");
            return null;
        }
        return mAppService.getData();
    }


    public void setThreadProcOperationWithListener(int nOperation, AppService.ThreadProcListener li) {
        if (null == mAppService)  {
            Log.i(TAG,"setThreadProcOperationWithListener() - Service is *NOT* Ready");
            return;
        }
        mAppService.setThreadProcOperationWithListener(nOperation, li);
        return;
    }


}

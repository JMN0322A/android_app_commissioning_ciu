package com.stanleyblackanddecker.fisx;


public class AppServiceData {


    public AppServiceData() {
        ;
    }


    public static final int X_IDLE              = (1);
    public static final int X_INITIALIZING      = (2);
    public static final int X_INQUIRY_CONNECTED = (3);
    public static final int X_WAITING_SCAN      = (4);
    public static final int X_DISCONNECTING     = (5);
    public static final int X_CONNECTING        = (6);


    public String  mRouter_ssid;         // router ssid, can be NULL if not originally connected
    //public String  mRouter_mac;          // mac, for quicker compares
    //public boolean mRouter_isConnected;  // if Router is connected

    public String  mCIU_ssid;            // CIU ssid
    public String  mCIU_mac;             // mac
    public int     mCIU_rssi;            // rssi
    //public boolean mCIU_isConnected;     // if CIU is connected
    public String  mCIU_password;        // password

    public long    mTimeExpireOperation; // time when current operation will expire



    public byte [] mCIU_bytes_rx; ////// the most recent BYTES RECEIVED from the CIU

    public byte [] mCIU_bytes_tx; ////// data WAITING to be TRANSMITTED TO the CIU

    public int   mWaitForReconnect = (0);

//    public String  mCIU_data_rx;  ////// the most recent data RECEIVED from the CIU
//    public String  mCIU_data_tx;  ////// data WAITING to be SENT to the CIU
}

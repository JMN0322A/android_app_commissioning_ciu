package com.stanleyblackanddecker.fisx;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmPressButton extends FragmBaseFragment {

    private final static String TAG = "{FragmPressButton}";

    TextView mTvBtnOk;

    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_press_button, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null") );
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);

        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "@PRESS-BUTTON---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__Start);
            }
        });





        mTvBtnOk = (TextView) rootView.findViewById(R.id.btn_ok);
        if (null != mTvBtnOk) {
            mTvBtnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onOkPressed();
                }
            });
        }

        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(1);

        Utils.LogDecorated(TAG, "  @Screen: PRESS BUTTON  ");

        return rootView;
    }




    private void onOkPressed() {
        Log.i(TAG, "----Press-Button----> (OK)!");
        mFCmdDispatcher.switchto(R.id.id_app_command__Search);
    }

}



package com.stanleyblackanddecker.fisx;




import android.os.Bundle;

public class ActivityMain extends ActivityContainer {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTitleBar.enableBtnMenu(true);
        mTitleBar.show();

        //////// activate the first fragment, fragm-zero
        mCommandDispatcher.switchto(R.id.id_app_command__About);

    }


}

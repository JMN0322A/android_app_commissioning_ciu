package com.stanleyblackanddecker.fisx;


public interface SimpleUpdateListener {

    public void onUpdate();

}

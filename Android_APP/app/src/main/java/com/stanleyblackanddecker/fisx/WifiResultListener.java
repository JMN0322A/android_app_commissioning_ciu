package com.stanleyblackanddecker.fisx;


public interface WifiResultListener {


    public void onWifiResult(WifiResult result);

}

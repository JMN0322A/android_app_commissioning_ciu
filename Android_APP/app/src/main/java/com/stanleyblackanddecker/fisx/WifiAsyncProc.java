package com.stanleyblackanddecker.fisx;





public class WifiAsyncProc {


    private final static String TAG = "{WifiAsyncProc}";


    private final static long DELTA_T = (999);

    private long               mTimeoutExpires;
    private Thread             mThreadWifiRunnable;
    private Thread             mThreadTimeout;
    private WifiResultListener mWifiListener;
    private WifiRunnable       mWifiRunnable;
    private int                mCount;


    public WifiAsyncProc() {
        Logger.i(TAG, "Constructor --- !");
        clear();
    }


    public WifiAsyncProc clear() {
        mTimeoutExpires     = (0);
        mThreadWifiRunnable = (null);
        mThreadTimeout      = (null);
        mWifiRunnable       = (null);
        mWifiListener       = (null);
        mCount              = (0);

        return (this);
    }


    public WifiAsyncProc setDurationTime(int dt) {
        if (dt <= 0) {
            mTimeoutExpires = (1000) + System.currentTimeMillis();
            Logger.i(TAG, "setTimeDuration() - (dt < 0)===>1000 **** Expiration = " + mTimeoutExpires);
        } else {
            mTimeoutExpires = (long)dt + System.currentTimeMillis();
            Logger.i(TAG, "setTimeDuration() - dt = " + dt + " Expiration = " + mTimeoutExpires);
        }

        return (this);
    }


    public WifiAsyncProc setWifiListener(WifiResultListener li) {
        mWifiListener = (li);
        if (null == li) {
            Logger.i(TAG, "setWifiListener() - NULL - NULL - NULL");
        } else {
            Logger.i(TAG, "setWifiListener() - " + li.toString());
        }

        return (this);
    }


    public WifiAsyncProc setRunnable(WifiRunnable r) {
        mWifiRunnable = (r);
        if (null == r) {
            Logger.i(TAG, "setRunnable() - NULL - NULL - NULL");
        } else {
            Logger.i(TAG, "setRunnable() - " + r.toString());
        }

        return (this);
    }


    public WifiAsyncProc cancel()
    {
        Logger.i(TAG,"cancel() - Begin.");
        if(null != mThreadTimeout) {
            if (mThreadTimeout.isAlive()) {
                Logger.i(TAG,"cancel() - Timeout=ALIVE. Interrupt!");
                mThreadTimeout.interrupt();
            }
            mThreadTimeout = null;
        }
        if(null != mThreadWifiRunnable) {
            if (mThreadWifiRunnable.isAlive()) {
                Logger.i(TAG,"cancel() - WifiRunnable=ALIVE. Interrupt!");
                mThreadWifiRunnable.interrupt();
            }
            mThreadWifiRunnable = null;
        }
////////if (null != mWifiListener) {
////////    mWifiListener.onWifiResult(new WifiResult(WifiResult.X_CANCELLED));
////////    mWifiListener = null;
////////}
        Logger.i(TAG,"cancel() - Return.");

        return (this);
    }



    /*-
    public void setResultAndQuit(WifiResult res) {
        if (null == res) {
            Log.i(TAG,"setResultAndQuit() - *Error- MISSING ARG WifiResult");
            return;
        }
        if (null == mWifiRunnable) {
            Log.i(TAG,"setResultAndQuit() - *Error- MISSING Wifi-Runnable");
            return;
        }
        WifiResult dest = mWifiRunnable.getWifiResult();
        if (null == dest) {
            Log.i(TAG,"setResultAndQuit() - *Error- NULL Wifi-Result, in the Wifi-Runnable");
            return;
        }

        dest.setCode(res.getCode());
        dest.setStr(res.getStr());
        dest.setData(res.getData());
        Log.i(TAG,"setResultAndQuit() - Resultant is SET.");

        if (null == mThreadTimeout) {
            Log.i(TAG,"setResultAndQuit() - Timeout-THREAD is NULL .. (abort)");
            return;
        }
        if (! mThreadTimeout.isAlive()) {
            Log.i(TAG,"setResultAndQuit() - Timeout-THREAD is *DEAD* .. (abort)");
            return;
        }
        Log.i(TAG,"setResultAndQuit() - Timeout-THREAD is Alive. (INTERRUPTING...)");
        mThreadTimeout.interrupt();
    }
    -*/

    public void terminateSignalWifiRunnable() {
        if (null == mWifiRunnable) {
            Logger.i(TAG,"terminateSignalWifiRunnable() - *Error- MISSING Wifi-Runnable");
            return;
        }
        if (null == mThreadTimeout) {
            Logger.i(TAG,"terminateSignalWifiRunnable() - Timeout-THREAD is NULL .. (abort)");
            return;
        }
        if (! mThreadTimeout.isAlive()) {
            Logger.i(TAG,"terminateSignalWifiRunnable() - Timeout-THREAD is *DEAD* .. (abort)");
            return;
        }
        Logger.i(TAG,"terminateSignalWifiRunnable() - Timeout-THREAD is Alive. (INTERRUPTING...)");
        mThreadTimeout.interrupt();
    }


    public boolean initiate()
    {
        if (System.currentTimeMillis() >= mTimeoutExpires) {
            Logger.i(TAG,"initiate() - *Error- BAD DURATION TIME");
            return false;
        }
        if (null == mWifiRunnable) {
            Logger.i(TAG,"initiate() - *Error- MISSING RUNNABLE");
            return false;
        }
        if (null == mWifiListener) {
            Logger.i(TAG,"initiate() - *Error- MISSING RESULT LISTENER");
            return false;
        }

        Logger.i(TAG,"initiate() - Cancel ongoning operation");
        cancel();

        Logger.i(TAG,"initiate() - Create THREAD for Runnable");
        mThreadWifiRunnable = new Thread(mWifiRunnable);
        if (null == mThreadWifiRunnable) {
            Logger.i(TAG, "initiate() - *Error- Can't create runnable Thread");
            return false;
        }

        Logger.i(TAG,"initiate() - Create THREAD for Timeout");
        mThreadTimeout = new Thread(new Runnable() {
            @Override
            public void run() {
                Logger.i(TAG,"initiate() - @Runnable-Timeout.");
                for(;;) {
                    try { Thread.sleep(DELTA_T); }
                    catch (InterruptedException ie) {
                        Logger.i(TAG, "initiate() - @Runnable-Timeout - INTERRUPTED !!!!!!!!!!");
                        mTimeoutExpires = (System.currentTimeMillis() - 1);
                        Logger.i(TAG, "initiate() - @Runnable-Timeout - Cleared to expire !!!!");
////////////////////////break;
                    }
                    Logger.i(TAG,"initiate() - @Runnable-Timeout.("+this.toString()+").TICK# " + mCount++);


                    //////// Determine if the RUNNABLE has ended...................................
                    if (mWifiRunnable.getWifiResult().getCode() != WifiResult.X_NO_RESULT) {
                        Logger.i(TAG, "initiate() - @Runnable-Runner - RESULT EXISTS !!!!!!!!!");
                        Logger.i(TAG, "initiate() - @Runnable-Runner - DISABLE this TIMER");

                        if (null != mWifiListener) {
                            Logger.i(TAG, "initiate() - @Runnable-Runner - CALLBACK");
                            mWifiListener.onWifiResult(mWifiRunnable.getWifiResult());
                        }
                        break; //////////// EXIT THIS ASYNC LOOPER
                    }

                    //////// Determine if a TIME-OUT has occurred..................................
                    if (System.currentTimeMillis() >= mTimeoutExpires) {
                        Logger.i(TAG, "initiate() - @Runnable-Timeout - EXPIRED !!!!!!!!!!!!!!");

                        if ((null != mThreadWifiRunnable) && (mThreadWifiRunnable.isAlive())) {
                            Logger.i(TAG, "initiate() - @Runnable-Timeout - KILL RUNNABLE !!!!!!!!!!!!");
                            mThreadWifiRunnable.interrupt();
                        }

                        mWifiRunnable.getWifiResult().setCode(WifiResult.X_TIMEOUT);
                        mWifiRunnable.getWifiResult().setStr(WifiResult.codeToString(WifiResult.X_TIMEOUT));
                        if (null != mWifiListener) {
                            Logger.i(TAG,"initProcAsyncAndTimeout() - @Runnable-Timeout - CALLBACK.");
                            ////mWifiListener.onWifiResult(new WifiResult(WifiResult.X_TIMEOUT));
                            mWifiListener.onWifiResult(mWifiRunnable.getWifiResult());
                            //////////////////////////////
                            //////////////////////////////
                            //////////////////////////////---TODO: CRASH IF NOTHING IS RECEIVED at Utils.byteArrayToHexMixedSAtring()
                            //////////////////////////////
                        }
                        break; //////////// EXIT THIS ASYNC LOOPER
                    }
                }
                Logger.i(TAG, "initiate() - @Runnable-Timeout - end of timer thread ~~~~RETURN.");
                return;
            }
        });
        if (null == mThreadTimeout) {
            Logger.i(TAG, "initiate() - *Error- Can't create timeout Thread");
            return false;
        }

        Logger.i(TAG,"initiate() - EXECUTE.");
        mThreadTimeout.start();
        mThreadWifiRunnable.start();
        Logger.i(TAG,"initiate() - Return.");

        return true;
    }






}

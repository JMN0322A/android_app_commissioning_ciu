package com.stanleyblackanddecker.fisx;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Utils {


    private static final String TAG = "{Utils}";

    private static final String mHX = "0123456789ABCDEF";


    public static String byteArrrayToHexString( byte [] ar ) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<ar.length; i++) {
            byte bb = ar[i];
            if (0 != i) {
                sb.append('-');
            }
            sb.append(mHX.charAt((0xf0 & (int)bb) >> 4));
            sb.append(mHX.charAt((0x0f & (int)bb)     ));
        }

        return sb.toString();
    }




    public static String byteArrrayToStringOfNumbers( byte [] ar ) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<ar.length; i++) {
            int n = (0x0ff & (int)(ar[i]));
            if (0 != i) {
                sb.append('~');
            }
            sb.append(n);
        }

        return sb.toString();
    }




    public static int calculate_checksum( byte [] ar) {
        return calculate_checksum(ar, 0);
    }

    public static int calculate_checksum( byte [] ar, int len ) {
        int csum = 0;
        if ((null != ar) && (0 != ar.length))
        {
            if (len <= 0 || ar.length < len) {
                len = ar.length;
            }

            for (int i = 0; i < len; i++) {
                csum += ((0x0ff) & ((int) (ar[i])));
            }
        }
        csum %= (0x0fe);
        csum ^= (0x0e5);

        return csum;
    }




    public static String byteArrayToHexMixedString(byte [] ar) {
        if (null == ar) {
            return ("-null-");
        }
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<ar.length; i++) {
            byte bb = ar[i];
            if (bb < 32 || 127 < bb) {
                sb.append("\\x");
                sb.append(mHX.charAt((0xf0 & (int)bb) >> 4));
                sb.append(mHX.charAt((0x0f & (int)bb)     ));
            }
            else {
                sb.append((char)bb);
            }
        }
        return sb.toString();
    }




    public static ArrayList<String> extractStringArray(byte[] ar) {
        ArrayList<String> ars = new ArrayList<String>();

        int len = (((int)(ar[1]) & 0x0ff) + 2);
        int offs = (2);

        StringBuilder sb = new StringBuilder();
        while (offs < len) {
            byte ch = ar[offs++];
            if ((0 == ch) || (offs == len)) {
                ars.add(sb.toString());
                sb.setLength(0);
                continue;
            }
            sb.append((char)ch);
        }
        int n = 0;
        for (String x : ars) {
            String xx = ars.get(n);
            Logger.i(TAG, "extractStringArray()- [" + n + "] => \"" + xx + "\", ("+xx.length()+")");
            ++n;
        }
        if (0 == n) {
            Logger.i(TAG, "extractStringArray()- [EMPTY]");
        }
        return ars;
    }




    public static String extractStringFromCIUresponse(String str) {
        int i, len;
        if (null == str) {
            return null;
        }
        if ((len = str.length()) < 4) {
            return null;
        }
        if ((i = str.indexOf('-')) < 0) {
            return null;
        }
        if ((++i) >= len) {
            return null;
        }
        if (('-') != str.charAt(i)) {
            return null;
        }
        if (len <= (++i)) {
            return null;
        }
        return str.substring(i);
    }



    public static String extractDateFromNumberString(String numStr)
    {
        if (null == numStr) {
            Logger.i(TAG, "extractDateFromNumberString()- null param");
            return "---";
        }

        //////// trim away any leading NON-DIGIT's
        while (numStr.length() >= 1) {
            Logger.i(TAG, "extractDateFromNumberString()- param @0: " + numStr);
            if (Character.isDigit( numStr.charAt(0) )) {
                Logger.i(TAG, "extractDateFromNumberString()- param NUMERIC " + numStr);
                break;
            }
            numStr = numStr.substring(1);
        }

        //////// attempt the conversion
        String rs = null;
        long num = 0;
        try {
            //////// attempt to convert the numeric string into an INTEGER
            num = (1000 * (long) Integer.parseInt(numStr));
            Logger.i(TAG, "extractDateFromNumberString()- NUMBER*1000 ==  " + num);

            //////// attempt to interpret the number into formatted date-string
            DateFormat simple = new SimpleDateFormat("EEE, dd MMM yyyy - HH:mm:ss  Z");
            Date result = new Date (num);
            rs = simple.format(result);
            Logger.i(TAG, "extractDateFromNumberString()- Result == " + rs);
        }
        catch (NumberFormatException nfe) {
            Logger.i(TAG, "extractDateFromNumberString()- EXCEPTION == " + nfe.toString());
            rs = "---";
        }

        return rs;
    }










    public static int hexCharToInt(char hex) {
        if ('0'<=hex && hex<='9') {
            return (hex - '0');
        }
        if ('a'<=hex && hex<='f') {
            return ((hex - 'a') + 10);
        }
        if ('A'<=hex && hex<='F') {
            return ((hex - 'A') + 10);
        }
        return (-1);
    }



    public static byte[] hexStringToByteArray( String str ) {
        if (null == str) {
            return null;
        }
        int len = str.length();
        if (len < 1) {
            return null;
        }
        byte [] x = new byte [len];
        int  ndx = 0;
        int  hexa = -1;
        for (int i=0; i < len; i++) {
            boolean bFlag = false;
            int n = hexCharToInt( str.charAt(i) );
            if (-1 == n) {
                if (hexa >= 0) {
                    x[ndx++] = (byte)hexa;
                    hexa = -1;
                }
                continue;
            }
            if (hexa < 0) {
                hexa = (byte)n;
                continue;
            }
            else {
                hexa *= 16;
                hexa += n;

                x[ndx++] = (byte)hexa;
                hexa = -1;
                continue;
            }
        }
        if (0 == ndx) {
            return null;
        }

        byte [] rtn = new byte [ndx];
        for (int i=0; i<ndx; i++) {
            rtn[i] = x[i];
        }
        return rtn;
    }





    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    //////// K-Key  length = 26-bytes  (208-bits)
    //////// Here, it is a 32-byte sequence of random-ish bytes
    //////// ***** THIS ARRAY IS SHARED WITH THE MOBILE APP ********
    //////// *****
    //////// ***** DO * NOT * EDIT * THIS ********
    //////// ***** DO * NOT * EDIT * THIS ********
    //////// ***** DO * NOT * EDIT * THIS ********
    ////////
    public final static byte [] mKkey = {
            (byte)0x1f, (byte)0x68, (byte)0x48, (byte)0xcc,
            (byte)0x59, (byte)0xcf, (byte)0x8a, (byte)0x9c,
            (byte)0x4d, (byte)0xe7, (byte)0xc4, (byte)0xf2,
            (byte)0x0d, (byte)0x5a, (byte)0xa0, (byte)0x93,
            (byte)0x3d, (byte)0xd0, (byte)0x56, (byte)0x8f,
            (byte)0x6c, (byte)0x06, (byte)0xd3, (byte)0x37,
            (byte)0x0d, (byte)0x6b, (byte)0x50, (byte)0x91,
            (byte)0x35, (byte)0x9b, (byte)0xbd, (byte)0xfd
    };
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////






    public static String diffTime(String numStr) {
        int dt;
        try {
            //////// attempt to convert the numeric string into an INTEGER
            dt = Integer.parseInt(numStr);
        }
        catch (NumberFormatException nfe) {
            return "---";
        }
        return diffTime(dt);
    }

    private static final int DT_SEC   = (1);
    private static final int DT_MIN   = (60 * DT_SEC);
    private static final int DT_HOUR  = (60 * DT_MIN);
    private static final int DT_DAY   = (24 * DT_HOUR);
    private static final int DT_WEEK  = (7 * DT_DAY);
    private static final int DT_YEAR  = ((365 * DT_DAY) + ((DT_DAY + 2) / 4));  // 365.25 days/year
    private static final int DT_MONTH = ((DT_YEAR + 6) / 12);

    public static String diffTime(int dt) {
        dt = (int)(System.currentTimeMillis() / 1000) - (dt);
        int nYears  = (dt / DT_YEAR);
        int nMonths = (dt / DT_MONTH);
        int nWeeks  = (dt / DT_WEEK);
        int nDays   = (dt / DT_DAY);
        int nHours  = (dt / DT_HOUR);
        int nMins   = (dt / DT_MIN);
        int nSecs   = (dt / DT_SEC);

        if (nYears == 1) {
            return ("1-year ago");
        }
        if (nYears > 0) {
            return "" + (nYears) + "-years ago";
        }
        if (nMonths == 1) {
            return ("1-month ago");
        }
        if (nMonths > 0) {
            return "" + (nMonths) + "-months ago";
        }
        if (nWeeks == 1) {
            return ("1-week ago");
        }
        if (nWeeks > 0) {
            return "" + (nWeeks) + "-weeks ago";
        }
        if (nDays == 1) {
            return ("1-day ago");
        }
        if (nDays > 0) {
            return "" + (nDays) + "-days ago";
        }
        if (nHours == 1) {
            return ("1-hour ago");
        }
        if (nHours > 0) {
            return "" + (nHours) + "-hours ago";
        }
        if (nMins == 1) {
            return ("1-minute ago");
        }
        if (nMins > 0) {
            return "" + (nMins) + "-minutes ago";
        }
        if (nSecs == 1) {
            return ("1-second ago");
        }
        if (nSecs > 0) {
            return "" + (nSecs) + "-seconds ago";
        }
        return "(now)" + dt;
    }








    private static char brascii64b_byte_to_char( byte x ) {
        char r;

        if( x < (26) )  {
            r = (char)((x) + ('A'));
        }
        else
        if( x < (26+26) )  {
            r = (char)((x - 26) + ('a'));
        }
        else
        if( x < (26+26+10) )  {
            r = (char)((x - 26 - 26) + ('0'));
        }
        else
        if( (0x3e) == x )  {
            r = ('-');
        }
        else
        if( (0x3f) == x )  {
            r = ('~');
        }
        else  {
            r = (char)(0xff);
        }
        return (r);
    }


    private static byte brascii64b_char_to_byte( char x ) {
        byte r;

        if( ('A') <= x  &&  x <= 'Z' )  {
            r = (byte)(x - ('A'));
        }
        else
        if( ('a') <= x  &&  x <= 'z' )  {
            r  = (byte)(x - ('a'));
            r += (26);
        }
        else
        if( ('0') <= x  &&  x <= '9' )  {
            r = (byte)(x - ('0'));
            r += (26+26);
        }
        else
        if( ('-') == x )  {
            r = (0x3e);
        }
        else
        if( ('~') == x )  {
            r = (0x3f);
        }
        else
        if( ('_') == x )  {
            r = (0x00);
        }
        else  {
            r = (0x7f);
        }
        return (r);
    }



    public static String brascii64b_encode( byte [] input_bytes )
    {
        if ((null == input_bytes) || (0 == input_bytes.length)) {
            return null;
        }

        int  input_offs = 0;
        int  [] bin = new int  [4];
        int  [] num = new int  [4];
        char [] b64 = new char [4];
        byte xByte;
        int i, len;
        StringBuilder sb = new StringBuilder();

        for(;;)
        {
            //// prepare
            b64[0] = b64[1] = b64[2] = b64[3] = ('\0');
            bin[0] = bin[1] = bin[2]          = (byte)(0);
            num[0] = num[1] = num[2]          = (byte)(0);
            len = 0;

            //// process the next three input bytes
            for( i=0; i<3; i++ )
            {
                //// if no more, stop
                if (input_offs >= input_bytes.length) {
                    break;
                }

                //// get the next data byte
                xByte = (input_bytes[ input_offs++ ]);

                //// save it into array of three bytes
                bin[i] = (int)xByte;
                //// count it
                ++len;
            }
            if ((0)==len) {
                return sb.toString();
            }


            //// process into four values(0-63)
            num[0] = ((bin[0] >> 2) & 0x3f);
            num[1] = ((bin[0] << 4) & 0x30) | ((bin[1] >> 4) & 0x0f);
            num[2] = ((bin[1] << 2) & 0x3c) | ((bin[2] >> 6) & 0x03);
            num[3] = ((bin[2]     ) & 0x3f);

            //// translate four values into characters
            b64[0] = brascii64b_byte_to_char( (byte)num[0] );
            b64[1] = brascii64b_byte_to_char( (byte)num[1] );
            b64[2] = brascii64b_byte_to_char( (byte)num[2] );
            b64[3] = brascii64b_byte_to_char( (byte)num[3] );

            //// output it
            if( len == 1 )  {
                sb.append( b64[0] );
                sb.append( b64[1] );
                sb.append( '_' );
                sb.append( '_' );
            }
            else
            if( len == 2 )  {
                sb.append( b64[0] );
                sb.append( b64[1] );
                sb.append( b64[2] );
                sb.append( '_' );
            }
            else
            if( len == 3 )  {
                sb.append( b64[0] );
                sb.append( b64[1] );
                sb.append( b64[2] );
                sb.append( b64[3] );
            }

        }
    }



    public static byte [] brascii64b_decode( String input_str )
    {
        if (null == input_str) {
            return null;
        }
        int  input_length = input_str.length();
        if (0 == input_length) {
            return null;
        }

        byte [] output = new byte [input_length];
        int     output_count = 0;

        char [] b64 = new char [4];
        int  [] bin = new int [4];
        int  xbin;
        char x;
        int i, len;
        int  input_offs = 0;

        while( input_offs < input_length )
        {
            //// prepare
            b64[0] = b64[1] = b64[2] = b64[3] = (char)(0);
            bin[0] = bin[1] = bin[2]          =       (0);
            len = 0;

            //// process the next four input characters
            for( i=0; i<4; i++ )
            {
                //// if no more, stop
                if (input_offs >= input_str.length()) {
                    break;
                }

                //// get the next char
                x = input_str.charAt(input_offs++);
                //// ignore white space
                //// printf( "%c", x );
                if( (' ')==x || ('\r')==x || ('\n')==x )
                    break;
                //// if padding-space, stop
                if( ('_') == x )
                    break;

                //// convert ascii-char to the binary 0..63
                xbin = (int)brascii64b_char_to_byte( x );
                //// if error, stop
                if(0 != (0xc0 & xbin))  {
                    //printf( "*=(%02x(%02x))=*\n", xbin, x );
                    i=999;
                    break;
                }
                ////////printf("*b64_decode(): \'%c\' : %02x => %02x\n", x, x, xbin);

                //// save the binary-ified binary 0..63
                b64[i] = (char)((byte)(xbin));
                //// count the piece
                ++len;
            }
            if (i>=999)
                break;

            //// process into three binary data bytes
            bin[0] = ((b64[0] << 2) & 0xfc) | ((b64[1] >> 4) & 0x03);
            bin[1] = ((b64[1] << 4) & 0xf0) | ((b64[2] >> 2) & 0x0f);
            bin[2] = ((b64[2] << 6) & 0xc0) | ((b64[3])      & 0x3f);

            ////      --> three Bytes  len==4
            ////   =  --> two   Bytes  len==3
            ////  ==  --> one   Byte   len==2
            if( len >= 2 )  {
			output[ output_count++ ] = (byte)bin[0];
                if( len >= 3 )  {
                    output[ output_count++ ] = (byte)bin[1];
                    if( len >= 4 )  {
                        output[ output_count++ ] = (byte)bin[2];
                    }
                }
            }
        }

        byte [] outputBytes = new byte [output_count];
        for (i=0; i<output_count; i++) {
            outputBytes[i] = output[i];
        }
        return outputBytes;
    }


    /*-
    public static String byteToHexString( byte nByte ) {
        StringBuilder sb = new StringBuilder();
        sb.append(mHX.charAt((0xf0 & (int)nByte) >> 4));
        sb.append(mHX.charAt((0x0f & (int)nByte)     ));

        return sb.toString();
    }
    -*/


    /*-
    public static String bytesToUuidString(byte[] bar) {
        if ((null == bar) || (bar.length != 16)) {
            return "-invalid-uuid-";
        }
        StringBuilder sb = new StringBuilder();
        sb.append (byteToHexString(bar[0]));
        sb.append (byteToHexString(bar[1]));
        sb.append (byteToHexString(bar[2]));
        sb.append (byteToHexString(bar[3]));
        sb.append ('-');
        sb.append (byteToHexString(bar[4]));
        sb.append (byteToHexString(bar[5]));
        sb.append ('-');
        sb.append (byteToHexString(bar[6]));
        sb.append (byteToHexString(bar[7]));
        sb.append ('-');
        sb.append (byteToHexString(bar[8]));
        sb.append (byteToHexString(bar[9]));
        sb.append ('-');
        sb.append (byteToHexString(bar[10]));
        sb.append (byteToHexString(bar[11]));
        sb.append (byteToHexString(bar[12]));
        sb.append (byteToHexString(bar[13]));
        sb.append (byteToHexString(bar[14]));
        sb.append (byteToHexString(bar[15]));
        return sb.toString();
    }
    -*/


    /*-
    private static int hexCharToInt(char ch) {
        if ((('0') <= ch) && (ch <= ('9'))) {
            return (int) (ch) - (int) ('0');
        } else if ((('A') <= ch) && (ch <= ('F'))) {
            return (int) (ch) - (int) ('A') + (10);
        } else if ((('a') <= ch) && (ch <= ('f'))) {
            return (int) (ch) - (int) ('a') + (10);
        } else {
            return (-1);
        }
    }
    public static byte[] hexStringToByteArray(String s) {
        if ((null == s) || (0 == s.length())) {
            return null;
        }
        byte[] bar = new byte[s.length()];
        int i = 0;
        int nValue = -1;

        for (int j=0; j<s.length(); j++) {
            int n = hexCharToInt(s.charAt(j));
            if (n < 0) {
                nValue = (-1);
                continue;
            }
            if (nValue < 0) {
                nValue = ((16) * n);
                continue;
            } else {
                bar[i++] = (byte)((nValue + n) & 0x0ff);
                nValue = (-1);
            }
        }
        byte[] rbar = new byte[i];
        for (int j=0; j<i; j++) {
            rbar[j] = bar[j];
        }
        return rbar;
    }
    -*/

    /*-
    public static int numStringToInteger(String s) {
        if ((null == s) || (0 == s.length())) {
            return (0);
        }

        boolean bPreviousMinusDetected = false;
        int nNumber = (0);

        for (int i=0; i<s.length(); i++) {
            char ch = s.charAt(i);
            if (('-') == ch) {
                bPreviousMinusDetected = true;
                continue;
            }
            if ((('0') <= ch) && (ch <= ('9'))) {
                for (int j = i; j<s.length(); j++) {
                    ch = s.charAt(j);
                    if ((('0') <= ch) && (ch <= ('9'))) {
                        int n = (int) (ch) - (int) ('0');
                        nNumber = (10 * nNumber) + n;
                        continue;
                    } else {
                        break;
                    }
                }
                break;
            }
            bPreviousMinusDetected = false;
            continue;
        }

        if (bPreviousMinusDetected) {
            nNumber = (0 - nNumber);
        }
        return nNumber;
    }
    -*/


    /*-
    public static byte[] int32tobyteArray(int num) {
        byte[] bar = new byte[4];

        bar[0] = (byte)((num      ) & 0xff);
        bar[1] = (byte)((num >>  8) & 0xff);
        bar[2] = (byte)((num >> 16) & 0xff);
        bar[3] = (byte)((num >> 24) & 0xff);

        return bar;
    }
    -*/


    /*-
    public static byte[] int32tobyteArrayReversed(int num) {
        byte[] bar = new byte[4];

        bar[3] = (byte)((num      ) & 0xff);
        bar[2] = (byte)((num >>  8) & 0xff);
        bar[1] = (byte)((num >> 16) & 0xff);
        bar[0] = (byte)((num >> 24) & 0xff);

        return bar;
    }
    -*/


    /*-
    public static byte[] stringToByteArray(String s) {
        if ((null == s) || (0 == s.length())) {
            return null;
        }
        byte[] bar = new byte[s.length()];

        for (int i=0; i<s.length(); i++) {
            char ch = s.charAt(i);
            bar[i] = (byte)ch;
        }
        return bar;
    }
    -*/



    public static void LogDecorated(String tag, String s) {
        if (null == s) {
            return;
        }
        char fillChar = ('\\');
        StringBuilder sb = new StringBuilder("_ _ _ _\n");
        int len = s.length();

        for (int j=(2); j>0; --j) {
            for (int i = (len + 32); i > 0; --i) {
                sb.append(fillChar);
            }
            sb.append('\n');
        }
        ///////////////////////////////////////
        for (int i=16+1; i>0; --i) {
            sb.append(fillChar);
        }
        for (int i=(len - 2); i>0; --i) {
            sb.append(' ');
        }
        for (int i=16+1; i>0; --i) {
            sb.append(fillChar);
        }
        sb.append('\n');
        ///////////////////////////////////////
        for (int i=16; i>0; --i) {
            sb.append(fillChar);
        }
        sb.append(s);
        for (int i=16; i>0; --i) {
            sb.append(fillChar);
        }
        sb.append('\n');
        ///////////////////////////////////////
        for (int i=16+1; i>0; --i) {
            sb.append(fillChar);
        }
        for (int i=(len - 2); i>0; --i) {
            sb.append(' ');
        }
        for (int i=16+1; i>0; --i) {
            sb.append(fillChar);
        }
        sb.append('\n');
        ///////////////////////////////////////
        for (int j=(2); j>0; --j) {
            for (int i = (len + 32); i > 0; --i) {
                sb.append(fillChar);
            }
            sb.append('\n');
        }
        Logger.i(tag, sb.toString());
    }



}

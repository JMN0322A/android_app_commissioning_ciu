package com.stanleyblackanddecker.fisx;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class AnimationPathCircular extends Animation
{
    private View  mView;
    private float mCenterX, mCenterY;
    private float mRadius;

    private float mPriorX,  mPriorY;
    private float mPriorDx, mPriorDy;


    public AnimationPathCircular (View vw, float radius) {
        mView = vw;
        mRadius = radius;
    }


    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight ) {
        mCenterX = mView.getLeft() + (float)((width  + 1) / 2);
        mCenterY = mView.getTop()  + (float)((height + 1) / 2);

        mPriorX = mCenterX;
        mPriorY = mCenterY;
    }


    @Override
    public void applyTransformation(float tmInterpolated, Transformation transf) {
        if (null == transf) {
            return;
        }
        if (0.0F == tmInterpolated) {
            transf.getMatrix().setTranslate(mPriorDx, mPriorDy);
            return;
        }

        double ang = ((double)tmInterpolated * (2.0*Math.PI)) + (0.5*Math.PI);
        float x = mCenterX + (mRadius * ((float)Math.cos(ang)));
        float y = mCenterY + (mRadius * ((float)Math.sin(ang)));
        float dx = (mPriorX - x);
        float dy = (mPriorY - y);

        mPriorDx = dx;
        mPriorDy = dy;
        mPriorX = x;
        mPriorY = y;

        transf.getMatrix().setTranslate(mPriorDx, mPriorDy);
    }


//    @Override
//    public boolean willChangeBounds() {
//        return true;
//    }

}

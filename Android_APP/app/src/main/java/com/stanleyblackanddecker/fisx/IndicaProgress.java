package com.stanleyblackanddecker.fisx;


import android.util.Log;
import android.view.View;


public class IndicaProgress {

    private final static String TAG = "{IndicaProgress}";
    private final static int  MAX_VIEWS = (8);

    private View [] mView = new View[MAX_VIEWS];
    private int  mNumViews;
    private int  mColorIdle;
    private int  mColorActive;

    public IndicaProgress(View vwContainment, int n, int colorIdle, int colorActive) {
        if (null == vwContainment) {
            Log.i(TAG,"Constructor- NULL Containment - IdicaProgress = Disabled.");
            mNumViews = (0);
            return;
        }
        if (n <= 0) {
            mNumViews = (3);
        }
        else if (MAX_VIEWS <= n) {
            mNumViews = (MAX_VIEWS -1 );
        }
        else {
            mNumViews = (n);
        }
        Log.i(TAG,"Constructor- n == " + n);
        Log.i(TAG,"Constructor- #views = " + mNumViews);
        mColorIdle = colorIdle;
        mColorActive = colorActive;

        mView[0] = vwContainment.findViewById(R.id.id_progres_p0);
        mView[1] = vwContainment.findViewById(R.id.id_progres_p1);
        mView[2] = vwContainment.findViewById(R.id.id_progres_p2);
        mView[3] = vwContainment.findViewById(R.id.id_progres_p3);
        mView[4] = vwContainment.findViewById(R.id.id_progres_p4);
        mView[5] = vwContainment.findViewById(R.id.id_progres_p5);
        mView[6] = vwContainment.findViewById(R.id.id_progres_p6);
        mView[7] = vwContainment.findViewById(R.id.id_progres_p7);

        for (int i=0; i<MAX_VIEWS; i++) {
            if (null == mView[i]) {
                Log.i(TAG,"Constructor- NULL in findViewById() - IdicaProgress = Disabled.");
                mNumViews = (0);
                break;
            }
        }
    }


    public void setViewActive(int n) {
        for (int i=0; i<mNumViews; i++) {
            if (i < n) {
                mView[i].setBackgroundColor(mColorActive);
            } else {
                mView[i].setBackgroundColor(mColorIdle);
            }
            Log.i(TAG,"setViewActive()- ndx == " + n);
        }
    }


}

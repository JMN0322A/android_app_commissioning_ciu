package com.stanleyblackanddecker.fisx;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FragmFlowStart extends FragmBaseFragment {

    private final static String TAG = "{FragmFlowStart}";

    private TextView mTvBtnStart;




    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_flow_start, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null"));
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnBack(false);
        mFTitleBar.enableBtnMenu(true);


        mTvBtnStart = (TextView) rootView.findViewById(R.id.btn_start);
        if (null != mTvBtnStart) {
            mTvBtnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBtnStart();
                }
            });
        }



        Utils.LogDecorated(TAG, "  @Screen: FLOW START  ");

        return rootView;
    }




    private void onBtnStart()
    {
        Log.i(TAG, "onBtnStart()");
        if ((null != mFCmdDispatcher) && (null != mFAcContainer))
        {
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////---Somehow, this isn't set in WifiProcs
            ///////////////////////////////////////////////// - TODO - WifiProcs initialize: Dlg,Ac
            mFWifiProcs.set_ZDialog(mFZDialog);
            mFWifiProcs.set_Activity(getActivity());
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////

            if (mFWifiProcs.isReadySystemDependencies()) {
                mFCmdDispatcher.switchto(R.id.id_app_command__PressButton);
            }
            else {
                Log.i(TAG, "onBtnStart() - NOT Ready for dependencies");
            }
        }
        else {
            Log.i(TAG, "onBtnStart() - mFCmdDispatcher, or, mFAcContainer - is NULL.");
        }
    }









}

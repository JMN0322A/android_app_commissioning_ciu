package com.stanleyblackanddecker.fisx;


import android.util.Log;

public class Logger {


    private static StringBuilder  m_sb = new StringBuilder();


    public static void clear() {
        m_sb.setLength(0);
        m_sb .append("APP Logs\n===========================\n\n");
    }

    public static void i (String tag, String s) {
        Log.i(tag, s);

        m_sb.append(System.currentTimeMillis());
        m_sb.append(" * ");
        m_sb.append(tag);
        m_sb.append(" * ");
        m_sb.append(s);
        m_sb.append('\n');
    }


    public static String getText() {
        return m_sb.toString();
    }

}

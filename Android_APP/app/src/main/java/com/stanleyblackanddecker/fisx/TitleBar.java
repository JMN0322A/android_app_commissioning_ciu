package com.stanleyblackanddecker.fisx;


import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;



public class TitleBar {

    private View mVwMenu;
    private View mVwBack;
    private ImageView mIvLogo;
    private View mVwSettings;
    private ViewGroup mVgContainer;


    public interface TitleBarBtnMenuListener {
        public void onClick();
    }
    public interface TitleBarBtnBackListener {
        public void onClick();
    }
    public interface TitleBarBtnSettingsListener {
        public void onClick();
    }

    private TitleBarBtnMenuListener mIClickMenu;
    private TitleBarBtnBackListener mIClickBack;
    private TitleBarBtnSettingsListener mIClickSettings;

    public void setOnClickBtnMenu(TitleBarBtnMenuListener i) {
        mIClickMenu = i;
    }
    public void setOnClickBtnBack(TitleBarBtnBackListener i) {
        mIClickBack = i;
    }
    public void setOnClickBtnSettings(TitleBarBtnSettingsListener i) {
        mIClickSettings = i;
    }


    public boolean isSetBackButtonListener() {
        return (null != mIClickBack);
    }
    public void alternateBackButtonTriggered() {
        if (null != mIClickBack) {
            mIClickBack.onClick();
        }
    }



    public TitleBar(Context c) {
        if (null == c) {
            return;
        }
        mVgContainer = ((Activity)c).findViewById(R.id.titleBar_container);

        if (null != mVgContainer) {
            mVwMenu = mVgContainer.findViewById(R.id.titleBar_menu);
            mVwBack = mVgContainer.findViewById(R.id.titleBar_back);
            mIvLogo = (ImageView) mVgContainer.findViewById(R.id.titleBar_logo);
            mVwSettings = mVgContainer.findViewById(R.id.titleBar_settings);

            if (null != mVwMenu) {
                mVwMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mIClickMenu) {
                            mIClickMenu.onClick();
                        }
                    }
                });
                enableBtnMenu(false);
            }

            if (null != mVwBack) {
                mVwBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mIClickBack) {
                            mIClickBack.onClick();
                        }
                    }
                });
                enableBtnBack(false);
            }

            if (null != mVwSettings) {
                mVwSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mIClickSettings) {
                            mIClickSettings.onClick();
                        }
                    }
                });
                enableBtnSettings(false);
            }

        }

    }


    public void show() {
        if (null != mVgContainer) {
            mVgContainer.setVisibility(View.VISIBLE);
        }
    }

    public void hide() {
        if (null != mVgContainer) {
            mVgContainer.setVisibility(View.INVISIBLE);
        }
    }

    public void enableBtnMenu(boolean b) {
        if ((null == mVwMenu) || (null == mVwBack)) {
            return;
        }

        if (b) {
            mVwMenu.setVisibility(View.VISIBLE);
            mVwBack.setVisibility(View.GONE);
        } else {
            mVwMenu.setVisibility(View.INVISIBLE);
        }
    }

    public void enableBtnBack(boolean b) {
        if ((null == mVwMenu) || (null == mVwBack)) {
            return;
        }

        if (b) {
            mVwBack.setVisibility(View.VISIBLE);
            mVwMenu.setVisibility(View.GONE);
        } else {
            mVwBack.setVisibility(View.GONE);
        }
    }

    public void enableBtnSettings(boolean b) {
        if (null != mVwSettings) {
            mVwSettings.setVisibility(b ? View.VISIBLE : View.INVISIBLE);
        }
    }



/*-
    public static final int LOGO_ORANGE = (1);
    public static final int LOGO_SILVER = (2);

    public void setLogo( int nLogo ) {
        if (null != mIvLogo) {
            if (LOGO_ORANGE ==  nLogo) {
                mIvLogo.setImageResource(R.drawable.black_decker_logo);
            } else if (LOGO_SILVER == nLogo) {
                mIvLogo.setImageResource(R.drawable.black_decker_logo_silver);
            } else {
                mIvLogo.setImageResource(R.drawable.ic_cat_default);
            }
        }
    }
-*/

}

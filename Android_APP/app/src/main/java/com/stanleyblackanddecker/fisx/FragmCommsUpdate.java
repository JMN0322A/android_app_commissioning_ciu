package com.stanleyblackanddecker.fisx;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmCommsUpdate extends FragmBaseFragment {

    private final static String TAG = "{FragmCommsUpdate}";
    private final static int    CYCLE_THRESHHOLD_NEXT = (24);

    private Runnable mAnimationRunnable;
    private Handler  mAnimationHandler;
    private boolean  mAnimationIsRunning = false;
    private int      mAnimationCycleCount;
    private String   mStrTX;
    private String   mStrRX;
    private boolean  mFlagIsCompleted = false;

    private TextView mTvStatus;

    private ImageView mIvTransmission;
    private int  mFrameNdx;
    private int  mFrameDeltaT [] = new int [] {
            100,100,100,100, 100,100,100,100
    };
    private int  mFrameDrawable [] = new int [] {
            R.drawable.transmission_0,
            R.drawable.transmission_1,
            R.drawable.transmission_2,
            R.drawable.transmission_3,
            R.drawable.transmission_4,
            R.drawable.transmission_5,
            R.drawable.transmission_6,
            R.drawable.transmission_7
    };



    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_comms_update, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null"));
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);

        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "@COMMS-UPDATE---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__Edit);
            }
        });


        mTvStatus = (TextView) rootView.findViewById(R.id.tv_status);
        mIvTransmission = (ImageView) rootView.findViewById(R.id.iv_transmissions);
        mFrameNdx = (mFrameDrawable.length);


        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(4);


        Utils.LogDecorated(TAG, "  @Screen: COMMS UPDATE  ");


        initiate_TcpIpComms();


        return rootView;
    }



    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");

        animationRunner();
    }




    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");

        mAnimationIsRunning = false;
    }




    private void determine_if_complete() {
        updateUI();
        if (mFlagIsCompleted) {
            if (mAnimationCycleCount >= CYCLE_THRESHHOLD_NEXT) {
                mFCmdDispatcher.switchto(R.id.id_app_command__Status);
            }
        }
    }




    /*-
    private void initiate_TcpIpComms() {
        Log.i(TAG, "initiate_TcpIpComms()- setting up for ((((-Update-))))");
        mStrTX = mFServiceData.mCIU_data_tx;
        mStrRX = " ... ... ...";
        mFWifiProcs.tcp_comms_setListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                int n = result.getCode();
                Log.i(TAG, "initiate_TcpIpComms()- "+ n +" "+ WifiResult.codeToString(n) + " " + result.getStr());
                Utils.LogDecorated(TAG, " TCPcomms "+result.getStr()+" ");
                if (WifiResult.X_SUCCESS == n) {
                    mStrRX = result.getStr();
                    mFlagIsCompleted = true;
                } else {
                    mStrTX = "*Error- " + n + " " + WifiResult.codeToString(n);
                }
            }
        });
        mFWifiProcs.tcp_comms(mStrTX.getBytes());///////////////////////////////////////////////////////
    }
    -*/
    private void initiate_TcpIpComms() {
        Log.i(TAG, "initiate_TcpIpComms()- setting up for ((((?--Query))))");
        //byte[] btx = new byte[] {'?', 0x01, 0x00, (byte)0xa5, 0x00, 0x00, 0x00, 0x00};
        mStrRX = " ...";
        mFWifiProcs.tcp_comms_setListener(new WifiResultListener() {
            @Override
            public void onWifiResult(WifiResult result) {
                int n = result.getCode();
                Log.i(TAG, "initiate_TcpIpComms()- "+ n +" "+ WifiResult.codeToString(n) + " " + result.getStr());
                Utils.LogDecorated(TAG, " Update-TCPcomms "+result.getStr()+" ");
                if (WifiResult.X_SUCCESS == n) {
                    byte [] ar = (byte [])result.getData();
                    mStrRX = Utils.byteArrayToHexMixedString(ar);
                    mFlagIsCompleted = true;
                } else {
                    mStrRX = "*Error- " + n + " " + WifiResult.codeToString(n);
                }
            }
        });
        Log.i(TAG,"initiate_TcpIpComms()- BTX=" + Utils.byteArrrayToHexString(mFServiceData.mCIU_bytes_tx));
        mFWifiProcs.tcp_comms(mFServiceData.mCIU_bytes_tx);
    }




    private void animationRunner() {
        mAnimationHandler = new Handler();
        mAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (! mAnimationIsRunning) {
                    Log.i(TAG, "animationRunner() --> Animation NOT running. Return.");
                    return;
                }
                if ((++mFrameNdx) >= mFrameDrawable.length) {
                    mFrameNdx = (0);
                }
                mIvTransmission.setImageResource(mFrameDrawable[mFrameNdx]);
                mAnimationHandler.postDelayed(this, (long)(mFrameDeltaT[mFrameNdx]));
                ++mAnimationCycleCount;
                //
                //
                //
                determine_if_complete();
            }
        };

        mAnimationHandler.postDelayed(mAnimationRunnable, 100);
        mAnimationCycleCount = (0);
        mAnimationIsRunning = true;
        Log.i(TAG, "animationRunner() - Starting animation");
    }



    private void updateUI() {
        if (null != mTvStatus) {
            StringBuilder sb = new StringBuilder();
            sb.append("TX: ");
            sb.append(mStrTX);
            sb.append("..  RX: ");
            sb.append(mStrRX);
            mTvStatus.setText(sb.toString());
        }
    }


}

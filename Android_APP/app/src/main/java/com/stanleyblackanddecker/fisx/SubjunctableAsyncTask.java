package com.stanleyblackanddecker.fisx;


import android.os.AsyncTask;





public class SubjunctableAsyncTask  extends AsyncTask<Void, Void, Boolean>
{
    private final static String TAG = "{SubjunctableAsTa}";


    public SubjunctableAsyncTask initialize() {
        mResultCode = (-1);
        mOnResultListener = (null);
        mSubjunctableTask = (null);

        return this;
    }


    @Override
    public void onPreExecute() {
        mResultCode = (-1);
    }

    @Override
    public void onPostExecute(Boolean b) {
        if (null != mOnResultListener) {
            mOnResultListener.onResult(mResultCode, null);
        }
    }

    @Override
    public Boolean doInBackground(Void... params) {
        if (null == mSubjunctableTask) {
            return false;
        }

        mResultCode = mSubjunctableTask.execute();

        return true;
    }







    private int mResultCode;

    public int getResultCode() {
        return mResultCode;
    }



    public interface SubjunctableTask {
        int execute();
    }

    private SubjunctableTask mSubjunctableTask = null;

    public SubjunctableAsyncTask setSubjTask( SubjunctableTask li ) {
        mSubjunctableTask = li;

        return this;
    }



    public interface OnResultListener {
        public void onResult(int code, String result);
    }

    private OnResultListener mOnResultListener;

    public SubjunctableAsyncTask setOnResultListener(OnResultListener li) {
        mOnResultListener = li;

        return this;
    }


}

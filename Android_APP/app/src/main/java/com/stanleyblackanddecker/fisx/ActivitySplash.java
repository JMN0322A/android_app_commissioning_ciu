package com.stanleyblackanddecker.fisx;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;


public class ActivitySplash extends ActivityContainer {


    private Handler mHandler;
    private Runnable mRunnable;
    boolean mIsContinuingToNextTask = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //////// if the application SPALSH was already shown, go to home ZERO screen
        if (ApplicationFIS.getInstance().isAppSplashed()) {
            mTitleBar.enableBtnMenu(true);
            mTitleBar.show();
            mCommandDispatcher.switchto(R.id.id_app_command__Start);
            return;
        }

        //////// the APP has been SPLASH'ed
        ApplicationFIS.getInstance().setAppSplashed();

        setContentView(R.layout.activity_splash);


        final Context ctx = this;
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                continueWithNextTask(ctx);
            }
        };
        mHandler.postDelayed(mRunnable, ApplicationFIS.SPLASH_TIME);


        ImageView iv = findViewById(R.id.splash_img);
        if (null != iv) {
            iv.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    continueWithNextTask(ctx);
                }
            });
        }

    }


    private void continueWithNextTask(Context ctx) {
        if (mIsContinuingToNextTask) {
            return;
        }
        mIsContinuingToNextTask = true;

        Intent intent = new Intent(ctx, ActivityMain.class);
        intent.addFlags(android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


}

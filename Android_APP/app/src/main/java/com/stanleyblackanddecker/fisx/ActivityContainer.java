package com.stanleyblackanddecker.fisx;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


public class ActivityContainer extends Activity {

    private final static String TAG = "{ActivityContainer}";

    protected AppServiceConnector  mServiceConnector;
    protected SpinningWait mSpinningWait;
    protected TitleBar mTitleBar;
    protected MenuDrawer mMenuDrawer;
    protected CommandDispatcher mCommandDispatcher;
    protected ZDialog mZDialog;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //////// Be careful! Don't forget: "super.xxxxxxx()" * * * * *
        super.setContentView(R.layout.activity_container);

        ApplicationFIS.getInstance(getApplicationContext());

        //////// get the screen width
        DisplayMetrics m = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(m);
        int w = m.widthPixels;
        int h = m.heightPixels;
        ApplicationFIS.getInstance().setScreenWidthHeight(w, h);

        mSpinningWait = new SpinningWait(this);
        mZDialog = new ZDialog(this);
        mZDialog.clear();
        ViewGroup vg = findViewById(R.id.titleBar_container);
        mTitleBar = new TitleBar(this);
        mMenuDrawer = new MenuDrawer(this, w);
        mCommandDispatcher = new CommandDispatcher(this);


        mTitleBar.setOnClickBtnMenu(new TitleBar.TitleBarBtnMenuListener(){
            @Override
            public void onClick() {
                mMenuDrawer.show();
            }
        });
        mTitleBar.setOnClickBtnSettings(null);
////////////////////////////////////////////////////////////////////////////////////////
//        final Activity a = this;
//        mTitleBar.setOnClickBtnSettings(new TitleBar.TitleBarBtnSettingsListener(){
//            @Override
//            public void onClick() {
//                Toast.makeText(a,"SETTINGS",Toast.LENGTH_SHORT).show();
//            }
//        });
////////////////////////////////////////////////////////////////////////////////////////


        final Activity a = this;
        mServiceConnector = ApplicationFIS.getInstance().getServiceConnector();
        mServiceConnector.setServiceReadinessListener(new AppServiceConnector.ReadinessListener() {
            @Override
            public void onReady() {
                Logger.i(TAG, "onReady() - service-connector -> initialize()");
                mServiceConnector.initialize(a, mZDialog);
            }
        });
    }










    @Override
    public void setContentView(int nRid) {
///////////////////// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/////////////////////        super.setContentView(nRid); ///////////////////// NOT NEEDED
///////////////////// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Logger.i(TAG, "setContentView( int=" + nRid + " )- get Container ");
        LinearLayout ll = (LinearLayout) findViewById(R.id.llContainer_content);

        Logger.i(TAG, "setContentView()- obtain inflater");
        LayoutInflater inflater = LayoutInflater.from(this);

        if (null == ll) {
            Logger.i(TAG, "setContentView()- LL is null");
            return;
        }

        Logger.i(TAG, "setContentView()- removeAllViews");
        ll.removeAllViews();

        Logger.i(TAG, "setContentView()- inflate layout");
        inflater.inflate(nRid, ll, true);
    }




    @Override
    public void onBackPressed() {
        Logger.i(TAG, "onBackPressed() - Entry ----");
        if (mZDialog.isVisible()) {
            Logger.i(TAG, "onBackPressed() - Dialog VISIBLE");
            if (! mZDialog.isModal()) {
                mZDialog.dismiss();
            }
            return;
        }
        Logger.i(TAG, "onBackPressed() - (NO)=DIALOG");

        if (mMenuDrawer.isVisible()) {
            Logger.i(TAG, "onBackPressed() - Menu VISIBLE");
            Logger.i(TAG, "onBackPressed() - Menu VISIBLE ... DISMISS");
            mMenuDrawer.dismiss();
            return;
        }
        Logger.i(TAG, "onBackPressed() - (NO)=MENU");

        if (null != mTitleBar) {
            Logger.i(TAG, "onBackPressed() - Titlebar EXISTS");
            if (mTitleBar.isSetBackButtonListener()) {
                Logger.i(TAG, "onBackPressed() - Titlebar - Back-Button LISTENER is set");
                mTitleBar.alternateBackButtonTriggered();
                return;
            }
        }
        Logger.i(TAG, "onBackPressed() - (NO)=TITLE-BAR");

        Logger.i(TAG, "onBackPressed() - Invoke default handler - - - - - - - - - - - - -");
        super.onBackPressed();
    }



    public void onClick_MenuOption(View v) {
        if ((null != mMenuDrawer) && mMenuDrawer.isVisible()) {
            mMenuDrawer.dismiss();
        }
        mCommandDispatcher.switchto(v.getId());
        /*-
        switch (v.getId()) {
            case R.id.id_app_command__MenuOption1:
                Log.d(TAG, "Menu-Click ===> (menu option 1)" );
                break;
            case R.id.id_app_command__MenuOption2:
                Log.d(TAG, "Menu-Click ===> (menu option 2)" );
                break;
        }
        -*/
    }






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.i(TAG,"TURN ON SYSTEM LOCATIONS  RESULT IGNORED");
    }

    @Override
    public void onRequestPermissionsResult( int nRequestCode, String[] permissions, int [] granted) {
        if (WifiProcs.LOCA_PERMI_REQUEST_CODE_IGNORED == nRequestCode) {
            Logger.i(TAG,"LOCATION PERMISSIONS CHANGED  RESULT IGNORED");
        }
    }




}

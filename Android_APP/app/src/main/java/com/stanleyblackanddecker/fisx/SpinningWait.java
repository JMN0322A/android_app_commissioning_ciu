package com.stanleyblackanddecker.fisx;


import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SpinningWait {

    private final static String TAG = "{SpinningWait}";

    private Activity mAc;

    private String  mMessageText = null;


    public SpinningWait(Activity a) {
        mAc = a;
    }


    public void clear() {
        mMessageText = null;
    }

    public void dismiss()  {
        Log.d(TAG,"dismiss()");

        if (null == mAc) {
            return;
        }

        LinearLayout llDarkener = (LinearLayout) mAc.findViewById(R.id.llContainer_darkener);
        if (null == llDarkener) {
            return;
        }
        FrameLayout fl = (FrameLayout) mAc.findViewById(R.id.llContainer_spinningWait);
        if (null == fl) {
            return;
        }

        ImageView ivSpin = (ImageView) mAc.findViewById(R.id.iv_spinningWait);
        if (null != ivSpin) {
            ivSpin.clearAnimation();
        }

        llDarkener.setVisibility(View.GONE);
        fl.setVisibility(View.GONE);
    }

    public void setMessageText(String s) {
        mMessageText = s;
    }

    public void show() {
        Log.d(TAG,"show()");

        if (null == mAc) {
            return;
        }

        LinearLayout llDarkener = (LinearLayout) mAc.findViewById(R.id.llContainer_darkener);
        if (null == llDarkener) {
            return;
        }
        FrameLayout fl = (FrameLayout) mAc.findViewById( R.id.llContainer_spinningWait);
        if (null == fl) {
            return;
        }
        LinearLayout llCtext = (LinearLayout) mAc.findViewById(R.id.ll_spinningWait_container_text);
        if (null == llCtext) {
            return;
        }

        llDarkener.setVisibility(View.VISIBLE);
        fl.setVisibility(View.VISIBLE);


        if (null != mMessageText) {
            llCtext.setVisibility(View.VISIBLE);
            TextView tv = (TextView) mAc.findViewById(R.id.tv_spinningWait_text);
            if (null != tv) {
                tv.setText(mMessageText);
            }
        } else {
            llCtext.setVisibility(View.GONE);
        }

        ImageView ivSpin = (ImageView) mAc.findViewById( R.id.iv_spinningWait);
        if( null == ivSpin )
            return;

        //// consume all touch events in this Container
        llDarkener.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG,"Darkener-layer (on-touch'ed)");
                return true;
            }
        });

        RotateAnimation animR = new RotateAnimation( 0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f );
        animR.setDuration( 667 );
        animR.setRepeatCount(Animation.INFINITE);
        animR.setInterpolator( new LinearInterpolator() );
        ivSpin.clearAnimation();
        ivSpin.startAnimation( animR );
    }


}

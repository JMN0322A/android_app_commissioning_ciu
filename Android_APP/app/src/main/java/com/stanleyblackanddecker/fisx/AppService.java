package com.stanleyblackanddecker.fisx;


import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;



import static java.lang.Thread.sleep;


public class AppService extends Service {


    private final static String TAG = "{AppService}";
    private Thread  mThreadTheService;



    private IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public AppService getService() {
            return AppService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind()");
        stopSelf();
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate() - ********");
        Log.d(TAG, "onCreate() - **********");
        Log.d(TAG, "onCreate() - ************");
        Log.d(TAG, "onCreate() - ************** *");

        /*--------------------------------------------------------------------
        testSequ();
        --------------------------------------------------------------------*/

        mThreadTheService = new Thread(new Runnable() {
            @Override
            public void run() {
                threadProc_service();
            }
        });
        mThreadTheService.start();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
    }





//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-//////////                  /////////////////////////////////////////////////////////////////////
//-//////////  THE   SERVICE   /////////////////////////////////////////////////////////////////////
//-//////////                  /////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////


    private WifiProcs       mWifiProcs;
    private Activity        mActivity;
    private ZDialog         mZDialog;
    private AppServiceData  mData;
    private int             mState = (-1);

    public boolean isReadyForOperations() {
        //if prerequisites are present to perform operations
        return false;
    }

    public void initialize(Activity a, ZDialog dlg) {
        mActivity = a;
        mZDialog  = dlg;
        mData = new AppServiceData();
        mWifiProcs = new WifiProcs(mData, mActivity, mZDialog);
    }



    public  WifiProcs getWifiProcs() {
        return mWifiProcs;
    }

    public  AppServiceData getData() { return mData; }

//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-//////////                           ////////////////////////////////////////////////////////////
//-//////////  THE   SERVICE  THREAD    ////////////////////////////////////////////////////////////
//-//////////                           ////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////


    public final static int SVC_OP__ZERO_0                       = 0;
    public final static int SVC_OP_IDLE                          = 1;
    public final static int SVC_OP_GET_CONNECTION_DATA           = 2;
    public final static int SVC_OP_GET_ROUTER_CONNECTION         = 3;
    public final static int SVC_OP_GET_WIFI_PROFILES             = 4;
    public final static int SVC_OP_WIFI_DISCONNECT               = 5;
    public final static int SVC_OP_WIFI_CONNECT_CIU              = 6;
    //                                                           //
    private int mThreadOperation                                 = (SVC_OP__ZERO_0);

    public interface ThreadProcListener {
        public void onComplete(boolean bRetcode, int nRetvalue, String retStr);
    }
    private ThreadProcListener mThreadProcListener = null;
    public void setThreadProcOperationWithListener (int nOp, ThreadProcListener li) {
        mThreadOperation    = (nOp);
        mThreadProcListener = (li);
    }


    private void threadProc_service() {
        long tm0 = System.currentTimeMillis();
        try {
            for (;;) {
                long tm = System.currentTimeMillis();
                int dt = (int) ((tm - tm0)/1000);
                Log.i(TAG, "threadProc_service() - " + dt + " - - - - - - - - - - - - -----");

                boolean bRetcode        = (false);
                int     nRetVal         = (0);
                String  strResult       = (null);
                boolean bInvokeListener = (false);

                switch (mThreadOperation)
                {
                    case SVC_OP_GET_CONNECTION_DATA:
                        Log.i(TAG,"{threadProc_service()}- GET-CONNECTION-DATA ~ ~ ~ ~ ~ ~");
                        bRetcode = true;
                        strResult = mWifiProcs.getWifiConnectionData();
                        bInvokeListener = true;
                        break;

                    case SVC_OP_GET_ROUTER_CONNECTION:
                        Log.i(TAG,"{threadProc_service()}- GET-ROUTER-CONNECTION ~ ~ ~ ~ ~");
                        bRetcode = true;
                        strResult = mWifiProcs.getWifiConnection_router(mData);
                        bInvokeListener = true;
                        break;

                    case SVC_OP_GET_WIFI_PROFILES:
                        Log.i(TAG,"{threadProc_service()}- GET-WIFI-PROFILES ~ ~ ~ ~ ~ ~ ~");
                        bRetcode = mWifiProcs.getWifiProfiles(mData);
                        bInvokeListener = true;
                        break;

                    case SVC_OP_WIFI_DISCONNECT:
                        Log.i(TAG,"{threadProc_service()}- WIFI-DISCONNECT ~ ~ ~ ~ ~ ~ ~ ~");
                        bRetcode = mWifiProcs.wifiDisconnect(mData);
                        bInvokeListener = true;
                        break;


                    case SVC_OP_WIFI_CONNECT_CIU:
                        Log.i(TAG,"{threadProc_service()}- WIFI-CONNECT-CIU ~ ~ ~ ~ ~ ~ ~ ~");
                        bRetcode = mWifiProcs.wifiConnectCIU(mData);
                        bInvokeListener = true;
                        break;


                    case SVC_OP_IDLE:
                        Log.i(TAG,"{threadProc_service()}- (IDLE)");
                        break;
                    default:
                        Log.i(TAG,"{threadProc_service()}- *UN-KNOWN*==== " + mThreadOperation);
                        mThreadOperation = (SVC_OP_IDLE);
                        break;
                }

                if (bInvokeListener) {
                    mThreadOperation = (SVC_OP_IDLE);
                    if (null != mThreadProcListener) {
                        ThreadProcListener li = mThreadProcListener;
                        mThreadProcListener = (null);
                        li.onComplete(bRetcode, nRetVal, strResult);
                    }
                    bInvokeListener = false;
                }



                sleep(10*1000);
            }
        } catch (InterruptedException e) {
            Log.i(TAG, "threadProc_service() - INTERRUPTED EXCEPTION");
        }
        return;
    }






///
/*-
--- Sequence :: Connect :
--- * At any time, if dropped, reconnect to desired ssid
--- * Get current wifi connection. Save as (router-ssid).  // null means not-connected
--- * Get LIST of Networks. If DESIRED is SAT_xxxx, remove old-DESIRED, and add new-DESIRED
--- * IF CONNECTED to (other than Desired)
--- * ** Disconnect from current wifi connection
--- * IF NOT CONNECTED to Desired
--- * ** Connect to Desired, with auto-reconnect turned "on"
---
-*/
///





//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-//////////                                  /////////////////////////////////////////////////////
//-//////////  IDEAS                           /////////////////////////////////////////////////////
//-//////////                                  /////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////
//-/////////////////////////////////////////////////////////////////////////////////////////////////

    /*--------------------------------------------------------------------

    public class Operation {
        public int ndx;
        public Operation(int n) {ndx = n;}
        public boolean run() { return false; }
    }

    public class OpAlpha extends Operation {
        public OpAlpha(int n) {super(n);}
        @Override
        public boolean run() {
            Log.i("OpAlpha","Alpha(" + ndx + ")");
            ndx += 1;
            return true;
        }
    }

    public class OpBravo extends Operation {
        public OpBravo(int n) {super(n);}
        @Override
        public boolean run() {
            Log.i("OpBravo","Bravo(" + ndx + ")");
            ndx += 10;
            return true;
        }
    }

    public void testSequ() {
        Log.i(TAG,"testSequ() ***********************************************************");
        Operation [] sequ = new Operation[4];
        sequ[0] = new OpAlpha(1000);
        sequ[1] = new OpBravo(2000);
        sequ[2] = new OpBravo(3000);
        sequ[3] = new OpAlpha(4000);

        for(int j=0; j<5; j++) {
            for (int i = 0; i < sequ.length; i++) {
                sequ[i].run();
            }
        }
        Log.i(TAG,"testSequ() ***********************************************************");
    }
    --------------------------------------------------------------------*/

}

package com.stanleyblackanddecker.fisx;



import android.app.Application;
import android.content.Context;
import android.util.Log;


public class ApplicationFIS extends Application {

    private final static String TAG = "{ApplicationCJS}";

    private int mScreenWidth;
    private int mScreenHeight;

    private static volatile ApplicationFIS mInstance;
    private static Context mAppContext;

    private static AppServiceConnector mAppServiceConnector;


    public ApplicationFIS(Context ctx) {
        mAppContext = ctx;

        Log.d(TAG, "ApplicationFIS() - Constructor - creating Service-Connector");
        mAppServiceConnector = new AppServiceConnector(mAppContext);
    }

    public static ApplicationFIS getInstance(Context ctx) {
        if (null == mInstance) {
            synchronized (ApplicationFIS.class) {
                mInstance = new ApplicationFIS(ctx);
            }
        }
        return mInstance;
    }
    public static ApplicationFIS getInstance() {
        return mInstance;
    }

    public AppServiceConnector getServiceConnector() {
        return mAppServiceConnector;
    }

    public static Context getAppContext() {
        return mAppContext;
    }




    public void setScreenWidthHeight(int w, int h) {
        mScreenWidth = w;
        mScreenHeight = h;
    }
    public int getScreenWidth() {
        return mScreenWidth;
    }
    public int getScreenHeight() {
        return mScreenHeight;
    }



    private static boolean mIsAppSplashed = false;
    public static boolean isAppSplashed() {
        return mIsAppSplashed;
    }
    public void setAppSplashed() {
        mIsAppSplashed = true;
    }

    public final static int INDICA_PROGRESS_SIZE = (7);
    public final static long SPLASH_TIME = (2500);
}

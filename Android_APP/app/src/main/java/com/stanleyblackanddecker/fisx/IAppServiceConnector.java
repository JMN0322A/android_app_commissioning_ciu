package com.stanleyblackanddecker.fisx;


import android.app.Activity;


public interface IAppServiceConnector {


    public void    setServiceReadinessListener(AppServiceConnector.ReadinessListener li);


    public boolean initialize(Activity a, ZDialog dlg);


    public WifiProcs getWifiProcs();

    public AppServiceData getData();


    public void setThreadProcOperationWithListener(int nOperation, AppService.ThreadProcListener li);

}

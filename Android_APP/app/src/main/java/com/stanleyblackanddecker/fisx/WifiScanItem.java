package com.stanleyblackanddecker.fisx;

public class WifiScanItem {

    public int    mID;
    public String mSSID;
    public String mBSSID;
    public int    mFreq;
    public int    mRssi;


    public WifiScanItem(int id, String ssid, String bssid, int freq, int rssi) {
        mID    = id;
        mSSID  = ssid;
        mBSSID = bssid;
        mFreq  = freq;
        mRssi  = rssi;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ID=");
        sb.append(mID);
        sb.append(", ssid:");
        sb.append(mSSID);
        sb.append(",bssid:");
        sb.append(mBSSID);
        sb.append(",");
        sb.append(mFreq);
        sb.append("mhz,");
        sb.append(mRssi);
        sb.append("dBm");

        return sb.toString();
    }

}

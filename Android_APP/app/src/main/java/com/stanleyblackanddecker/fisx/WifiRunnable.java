package com.stanleyblackanddecker.fisx;


public class WifiRunnable extends Thread {

    private WifiResult  mResult;

    public WifiRunnable() {
        mResult = new WifiResult();
    }

    public WifiRunnable( WifiResult res) {
        mResult = (res);
    }

    public WifiResult getWifiResult() {
        return mResult;
    }

}

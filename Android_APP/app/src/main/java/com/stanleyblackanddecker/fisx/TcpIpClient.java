package com.stanleyblackanddecker.fisx;


import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;


public class TcpIpClient  {


    private final static String  TAG       = ("{TcpIpClient}");
    public  final static String  CIU_IP    = ("172.29.188.1");
    public  final static int     CIU_PORT  = (9090);


    private InetAddress    mInetAddress;
    private Socket         mSocket;
//    private PrintWriter    mBufTx;
//    private BufferedReader mBufRx;
//    private StringBuffer   mSBrx = new StringBuffer();
//    private String         mStrResult;
    private byte []        mBytesResult;



    public TcpIpClient() {
        ;//
    }


//    public String getRx() {
//        return mSBrx.toString();
//        return mStrResult;
//    }
//    public byte [] getRx() {
//        return mBytesResult;
//    }



    public byte [] comms(byte [] msgTX) {
        if ((null == msgTX) || (0 == msgTX.length)) {
            Log.i(TAG, "comms()- ****-NO-message-TX-*********************");
            return null;
        }
//        mMessageTx = (msgTX);
        Log.i(TAG, "comms()- Entry.");
        Log.i(TAG, "comms()- TX==" + Utils.byteArrrayToHexString(msgTX));


//        String strResult = null;
        try {
            Log.i(TAG, "comms()- setup InetAddress...");
            mInetAddress = InetAddress.getByName(CIU_IP);
            if (null == mInetAddress) {
                Log.i(TAG, "comms()- setup InetAddress === ERROR");
                return null;
            }

            Log.i(TAG, "comms()- setup Socket...");
            mSocket = new Socket (mInetAddress, CIU_PORT);
            if (null == mSocket) {
                Log.i(TAG, "comms()- setup Socket === ERROR");
                return null;
            }



            try {
                Log.i(TAG, "comms()- setup DataOutputStream...");
                DataOutputStream dOut = new DataOutputStream(mSocket.getOutputStream());
                if (null == dOut) {
                    Log.i(TAG, "comms()- setup DataOutputStream === ERROR");
                    return null;
                }

                Log.i(TAG, "comms()- setup DataInputStream...");
                DataInputStream dIn = new DataInputStream(mSocket.getInputStream());
                if (null == dIn) {
                    Log.i(TAG, "comms()- setup DataInputStream === ERROR");
                    return null;
                }


                Log.i(TAG, "comms()- Tx ==> " + Utils.byteArrrayToHexString(msgTX) + " ...");
                dOut.write(msgTX);

                Log.i(TAG, "comms()- RECEIVE RESPONSE ...");
                byte [] rxTmp = new byte[512];
                long tmExpires = System.currentTimeMillis() + 3*1000;
                while (System.currentTimeMillis()  <  tmExpires)
                {
                    //////////////////////////////////// Attempt to RECEIVE data
                    int nread = dIn.read(rxTmp);
                    if (nread < 4)  {
                        Log.i(TAG, "comms()- Rx----NOTHING-RECEIVED--looping...");
                        continue;
                        ////////----- just keep on looping until time expires
                    }

                    //////// Copy data received into a properly-lengthed byte array
                    mBytesResult = new byte [nread];
                    for (int i=0; i<nread; i++) {
                        mBytesResult[i] = rxTmp[i];
                    }
                    rxTmp = (null); ////////////////////////////////////////////////////-G.C. !!!!!


                    Log.i(TAG, "comms()- Rx("+nread+")-- " + Utils.byteArrrayToHexString(mBytesResult));
                    Log.i(TAG, "comms()- Rx("+nread+")~~ " + Utils.byteArrrayToStringOfNumbers(mBytesResult));


                    //////// examine the CHECKSUM
                    int nDlen = (2 + (int)mBytesResult[1]);
                    int csum = Utils.calculate_checksum(mBytesResult, nDlen);
                    if (mBytesResult[nDlen] != (byte)csum) {
                        Log.i(TAG,"comms()- Rx--ERROR--CHECKSUM");
                    } else {
                        Log.i(TAG,"comms()- Rx--CHECKSUM----OK---");
                    }

                    rxTmp = null; /////////////// G.C. !!!!!!!!


//                    StringBuilder sbRx = new StringBuilder();
//                    for (int i=2; i<nDlen; i++)
//                        sbRx.append((char)rxBytes[i]);
//                    mStrResult = sbRx.toString();


                    ApplicationFIS.getInstance().getServiceConnector().getData().mCIU_bytes_rx = mBytesResult;
                    Log.i(TAG, "comms()- Rx-- ==> " + Utils.byteArrayToHexMixedString(mBytesResult));
                    break;


                    /*-
                    if ((null != mMessageRx) && (0 != mMessageRx.length())) {
                        mSBrx.append(mMessageRx);
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====0");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====1");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====2");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====3");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====4");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====5");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====6");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====7");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====8");
                        Log.i(TAG, "comms()- Rx-- .readLine() ==> " + mMessageRx + " ====9");


                        ApplicationFIS.getInstance().getServiceConnector().getData().mCIU_data_rx = mMessageRx;
                        strResult = mMessageRx;


                        if (strResult.length() >= 8) {
                            Log.i(TAG, "comms()- (length reached)(readLine detected)(OK)---(BREAK)");
                            break;
                        }
                    }
////////////////////else {
////////////////////    Log.i(TAG, "comms()- Rx-- .readline() ------ NULL"); //--CONFIRMED: NON-Blocking-call
////////////////////}
                    -*/
                }
                Log.i(TAG, "comms()- (receive response) DONE LISTENING.");

            }
            catch (Exception e) {
                Log.e(TAG, "comms()- exception === " + e.toString());
            }
            finally {
                if (null != mSocket) {
                    Log.i(TAG, "comms()-  socket-close-()");
                    mSocket.close();
                }
            }


        } catch (Exception e) {
            Log.e(TAG, "comms()- exception === " + e.toString());
        }
        Log.i(TAG, "comms()-  Return.");
        return mBytesResult;

    }




}

package com.stanleyblackanddecker.fisx;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmConnect extends FragmBaseFragment implements WifiResultListener {


    private final String TAG = "{FragmConnect}";
    private final static int    CYCLE_THRESHHOLD_NEXT = (32);

    private Runnable mAnimationRunnable;
    private Handler  mAnimationHandler;
    private boolean  mAnimationIsRunning = false;
    private int      mAnimationCycleCount;
    private boolean  mFlagIsConnecting = false;

    private TextView mTvStatus;

    private ImageView mIvConnecting;
    private int  mFrameNdx;
    private int  mFrameDeltaT [] = new int [] {
            100,100,100, 100
    };
    private int  mFrameDrawable [] = new int [] {
            R.drawable.connecting_1,
            R.drawable.connecting_2,
            R.drawable.connecting_3,
            R.drawable.connecting_4,
    };


    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_connect, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null") );
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);


        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "@CONNECT---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__PressButton);
            }
        });



        mTvStatus = (TextView) rootView.findViewById(R.id.tv_status);
        mIvConnecting = (ImageView) rootView.findViewById(R.id.iv_connecting);
        mFrameNdx = (mFrameDrawable.length);


        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(3);


        Log.i(TAG, "onCreateView() - GET CONNECTION INFO");
        mFAcContainer.mServiceConnector.getWifiProcs().connectCIU_start(this);


        Utils.LogDecorated(TAG, "  @Screen: CONNECT  ");

        return rootView;
    }



    private void determine_if_Connected()  {
        if (!mFlagIsConnecting) {
            return;
        }
        ////Log.i(TAG,"determine_if_Connected()- c.b. has fired.");

        String ssid = UtilNetwork.getConnectedSSID(ApplicationFIS.getAppContext());
        ////Log.i(TAG,"determine_if_Connected()- ssid == " + ssid);

        if (null != ssid) {
            if (ssid.length() >= 20) {
                if (ssid.contains("SAT_")) {
                    Log.i(TAG,"determine_if_Connected()- ssid == " + ssid + " #" + mAnimationCycleCount);
                    if (mAnimationCycleCount >= CYCLE_THRESHHOLD_NEXT) {
                        ////Log.i(TAG,"determine_if_Connected()- DISPATCHER !!!!!!!!!!!!!!!!!!!!!");
                        mFCmdDispatcher.switchto(R.id.id_app_command__CommsQuery);
                    }
                }
            }
        }
        ////else {
        ////    Log.i(TAG,"determine_if_Connected()- ssid == NULL... #" + mAnimationCycleCount);
        ////}
    }




    public void onWifiResult(final WifiResult res) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                mFZDialog.clear();
                switch(res.getCode()) {
                    case WifiResult.X_TIMEOUT:
                    case WifiResult.X_FAIL:
                        mFZDialog.setTitle("ERROR");
                        mFZDialog.setMessage(res.getStr());
                        mFZDialog.setModal(true);
                        mFZDialog.setSingleBtnMode(true);
                        mFZDialog.setDialogOkListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.i(TAG,"onClick() - DISMISS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                            }
                        });
                        mFZDialog.show();
                        break;

                    case WifiResult.X_SUCCESS:
                        mFlagIsConnecting = true;
                        Utils.LogDecorated(TAG,"  (@CALLBACK)--CONNECTED  ");
                        break;
                }
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");

        animationRunner();

        updateUI();

//////////mFAcContainer.mServiceConnector.getData().mCIU_password = "0000000000";

        Log.i(TAG, "onResume() - CIU-ssid:"
                + mFAcContainer.mServiceConnector.getData().mCIU_ssid
                + " CIU-password:"
                + mFAcContainer.mServiceConnector.getData().mCIU_password
                );


//////////mFWifiProcs.connectToWifiCIU(mFServiceData.mCIU_ssid, "0000000000");
//////////mFWifiProcs.connectToWifiCIU("the-router", "Passwert1!");
    }





/*
        mTvStatus.setText("EXPERIMENTAL ASYNC GET PROFILES");
        mFWifiProcs.getWifiProfilesAsync(mFAcContainer.mServiceConnector.getData());
        if (false) {
            mTvStatus.setText("Get connection data");
            mFAcContainer.mServiceConnector.setThreadProcOperationWithListener(
                    AppService.SVC_OP_GET_CONNECTION_DATA,
                    new AppService.ThreadProcListener() {
                        @Override
                        public void onComplete(boolean b, int n, String s) {
                            Log.i(TAG, "procConnect() /\\_ ssid == " + s + " ********");
                            //
                            if (null == s) {
                                Log.i(TAG, "procConnect_start() /\\_ *NOT**CONNECTED* ********1");
                                Log.i(TAG, "procConnect_start() /\\_ *NOT**CONNECTED* ********2");
                                Log.i(TAG, "procConnect_start() /\\_ *NOT**CONNECTED* ********3");
                                Log.i(TAG, "procConnect_start() /\\_ *NOT**CONNECTED* ********4");
                                //mTvStatus.setText("*NOT* Connected to Wifi");
                            } else if (WifiProcs.isSSID_of_CIU(s)) {
                                Log.i(TAG, "procConnect_start() /\\_ CONNECTED-TO-CIU ==  " + s + " ********1");
                                Log.i(TAG, "procConnect_start() /\\_ CONNECTED-TO-CIU ==  " + s + " ********2");
                                Log.i(TAG, "procConnect_start() /\\_ CONNECTED-TO-CIU ==  " + s + " ********3");
                                Log.i(TAG, "procConnect_start() /\\_ CONNECTED-TO-CIU ==  " + s + " ********4");
                                //mTvStatus.setText("Connected to CIU " + s);
                            } else {
                                //////// if this is too short -or- doesn't contain marker(SAT_)
                                //////// then this is THE ROUTER
                                mFServiceData.mRouter_ssid = (s);
                                Log.i(TAG, "procConnect_start() /\\_ ROUTER ==  " + s + " ********1");
                                Log.i(TAG, "procConnect_start() /\\_ ROUTER ==  " + s + " ********2");
                                Log.i(TAG, "procConnect_start() /\\_ ROUTER ==  " + s + " ********3");
                                Log.i(TAG, "procConnect_start() /\\_ ROUTER ==  " + s + " ********4");
                                //mTvStatus.setText("Connected to Wifi Router " + s);
                            }

                            procConnect_profiles();
                        }
                    }
            );
        }




    private void procConnect_profiles() {
        //mTvStatus.setText("Collecting Profiles");
        mFAcContainer.mServiceConnector.setThreadProcOperationWithListener(
                AppService.SVC_OP_GET_WIFI_PROFILES,
                new AppService.ThreadProcListener(){
                    @Override
                    public void onComplete(boolean b, int n, String s) {
                        Log.i(TAG, "procConnect() /\\_ wifiProfiles == " + b + " ********1");
                        Log.i(TAG, "procConnect() /\\_ wifiProfiles == " + b + " ********2");
                        Log.i(TAG, "procConnect() /\\_ wifiProfiles == " + b + " ********3");
                        Log.i(TAG, "procConnect() /\\_ wifiProfiles == " + b + " ********4");
                        //
                        procConnect_disconnectRouter();
                    }
                }
        );
    }




    private void procConnect_disconnectRouter() {
        //mTvStatus.setText("Disconnecting");
        mFAcContainer.mServiceConnector.setThreadProcOperationWithListener(
                AppService.SVC_OP_WIFI_DISCONNECT,
                new AppService.ThreadProcListener(){
                    @Override
                    public void onComplete(boolean b, int n, String s) {
                        Log.i(TAG, "procConnect() /\\_ wifiDisconnectRouter == " + b + " ********1");
                        Log.i(TAG, "procConnect() /\\_ wifiDisconnectRouter == " + b + " ********2");
                        Log.i(TAG, "procConnect() /\\_ wifiDisconnectRouter == " + b + " ********3");
                        Log.i(TAG, "procConnect() /\\_ wifiDisconnectRouter == " + b + " ********4");
                        //
                        procConnect_connectCIU();
                    }
                }
        );
    }




    private void procConnect_connectCIU() {
        //mTvStatus.setText("Connect-CIU");
        mFAcContainer.mServiceConnector.setThreadProcOperationWithListener(
                AppService.SVC_OP_WIFI_CONNECT_CIU,
                new AppService.ThreadProcListener(){
                    @Override
                    public void onComplete(boolean b, int n, String s) {
                        Log.i(TAG, "procConnect() /\\_ wifiConnectCIU == " + b + " ********1");
                        Log.i(TAG, "procConnect() /\\_ wifiConnectCIU == " + b + " ********2");
                        Log.i(TAG, "procConnect() /\\_ wifiConnectCIU == " + b + " ********3");
                        Log.i(TAG, "procConnect() /\\_ wifiConnectCIU == " + b + " ********4");
                        //
                        procConnect_connectedCIU();
                    }
                }
        );
    }




    private void procConnect_connectedCIU() {
        mAnimationIsRunning = false;

        mFCmdDispatcher.switchto(R.id.id_app_command__Communications);
    }
*/


    private void animationRunner() {
        mAnimationHandler = new Handler();
        mAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (! mAnimationIsRunning) {
                    Log.i(TAG, "animationRunner() --> Animation NOT running. Return.");
                    return;
                }
                if ((++mFrameNdx) >= mFrameDrawable.length) {
                    mFrameNdx = (0);
                }
                mIvConnecting.setImageResource(mFrameDrawable[mFrameNdx]);
                mAnimationHandler.postDelayed(this, (long)(mFrameDeltaT[mFrameNdx]));
                ++mAnimationCycleCount;
                //Log.i(TAG, "animationRunner() - Count: " + mAnimationCycleCount);
                //
                //
                //
                determine_if_Connected();
            }
        };

        mAnimationHandler.postDelayed(mAnimationRunnable, 100);
        mAnimationCycleCount = (0);
        mAnimationIsRunning = true;
        Log.i(TAG, "animationRunner() - Starting animation");
    }



    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");

        mAnimationIsRunning = false;
        Log.i(TAG, "onPause() - UN-Flagging animation");
    }






    private void updateUI() {
        if (null != mTvStatus) {
            StringBuilder sb = new StringBuilder();
            sb.append(mFServiceData.mCIU_ssid);
            sb.append("  ");
            sb.append(mFServiceData.mCIU_mac);
            sb.append("  ");
            sb.append(mFServiceData.mCIU_rssi);
            sb.append("dBm.  Router-");
            sb.append(mFServiceData.mRouter_ssid);
            mTvStatus.setText(sb.toString());
        }
    }


    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////








}

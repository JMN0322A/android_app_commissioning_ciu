package com.stanleyblackanddecker.fisx;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;



public class FragmReconnect extends FragmBaseFragment implements WifiResultListener {


    private final String TAG = "{FragmReconnect}";

    private final static int    CYCLE_THRESHHOLD_NEXT = (64);
    private final static int    CYCLE_BEFORE_START    = (40);

    private int mFrameCycles_before_start = (0);


    private Runnable mAnimationRunnable;
    private Handler  mAnimationHandler;
    private boolean  mAnimationIsRunning = false;
    private int      mAnimationCycleCount;
    private boolean  mIsReconnected = false;

    private ImageView mIvReconnecting;
    private int       mFrameNdx;

    private int       mFrameDeltaT_disconnecting [] = new int [] {
            125,125,125,125
    };
    private int       mFrameDrawable_disconnecting [] = new int [] {
            R.drawable.disconnect_0,
            R.drawable.disconnect_1,
            R.drawable.disconnect_2,
            R.drawable.disconnect_3
    };

    private int       mFrameDeltaT_connected [] = new int [] {
            100,100,100,100, 100
    };
    private int       mFrameDrawable_connected [] = new int [] {
            R.drawable.disconnect_3,
            R.drawable.disconnect_4,
            R.drawable.disconnect_5,
            R.drawable.disconnect_6,
            R.drawable.disconnect_7
    };




    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstance) {

        initializeBase();


        View rootView = inflator.inflate(R.layout.fragm_reconnect, container, false);

        Log.d(TAG, "inflated: " + (rootView == null ? "NULL" : "not-null") );
        mFTitleBar.enableBtnSettings(false);
        mFTitleBar.enableBtnMenu(false);
        mFTitleBar.enableBtnBack(true);


        mFTitleBar.setOnClickBtnBack(new TitleBar.TitleBarBtnBackListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "@RECONNECT---BACK");
                mFCmdDispatcher.switchto(R.id.id_app_command__PressButton);
            }
        });

        mIvReconnecting = (ImageView) rootView.findViewById(R.id.iv_reconnect);
        mFrameNdx = (9999999);


        IndicaProgress idp = new IndicaProgress(
                rootView,
                ApplicationFIS.INDICA_PROGRESS_SIZE,
                getActivity().getResources().getColor(R.color.bkg_indica_progress_idle),
                getActivity().getResources().getColor(R.color.bkg_indica_progress_active) );

        idp.setViewActive(6);



        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**1");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**2");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**3");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**4");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**5");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**6");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**7");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFServiceData.mRouter_ssid + " <<<<<<**8");

        Log.i(TAG, "onCreateView()-  **********************************************");

        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**1");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**2");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**3");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**4");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**5");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**6");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**7");
        Log.i(TAG, "onCreateView()-  ******>>>>>> " + mFAcContainer.mServiceConnector.getData().mRouter_ssid + " <<<<<<**8");




        Utils.LogDecorated(TAG, "  @Screen: RE-CONNECT  ");


        return rootView;
    }



    private void delay_before_start_reconnect() {
        if ((++mFrameCycles_before_start) == CYCLE_BEFORE_START) {
            mFAcContainer.mServiceConnector.getWifiProcs().connectRouter_start(this);
        }
    }



    private void determine_if_reconnected()  {
        int    nwkstate = UtilNetwork.getConnectedState(ApplicationFIS.getAppContext());
        String ssid = UtilNetwork.getConnectedSSID(ApplicationFIS.getAppContext());
        Log.i(TAG, "testIfConnected()-  state == " + nwkstate + ", ssid == " + ssid );

        if ((1) != nwkstate) {
            mAnimationCycleCount = (0);
            mIsReconnected = false;
            return;
        }
        mIsReconnected = true;
        if (mAnimationCycleCount >= CYCLE_THRESHHOLD_NEXT) {
            if ((0) == mFServiceData.mWaitForReconnect) {
                mFCmdDispatcher.switchto(R.id.id_app_command__About);
            }
            else {
                mFCmdDispatcher.switchto(R.id.id_app_command__ReconnectWait);
            }
        }
    }




    public void onWifiResult(final WifiResult res) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                mFZDialog.clear();
                switch(res.getCode()) {
                    case WifiResult.X_TIMEOUT:
                    case WifiResult.X_FAIL:
                        mFZDialog.setTitle("ERROR");
                        mFZDialog.setMessage(res.getStr());
                        mFZDialog.setModal(true);
                        mFZDialog.setSingleBtnMode(true);
                        mFZDialog.setDialogOkListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.i(TAG,"onClick() - DISMISS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                            }
                        });
                        mFZDialog.show();
                        break;

                    case WifiResult.X_SUCCESS:
////////////////////////Toast.makeText(mFAcContainer,res.getStr(),Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");

        animationRunner();

        updateUI();
    }



    private void animationRunner() {
        mAnimationHandler = new Handler();
        mAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (! mAnimationIsRunning) {
                    Log.i(TAG, "animationRunner() --> Animation NOT running. Return.");
                    return;
                }


                delay_before_start_reconnect();


                if (! mIsReconnected) {
                    if ((++mFrameNdx) >= mFrameDrawable_disconnecting.length) {
                        mFrameNdx = (0);
                    }
                    mIvReconnecting.setImageResource(mFrameDrawable_disconnecting[mFrameNdx]);
                    mAnimationHandler.postDelayed(this, (long)(mFrameDeltaT_disconnecting[mFrameNdx]));
                }
                else {
                    if ((++mFrameNdx) >= mFrameDrawable_connected.length) {
                        mFrameNdx = (0);
                    }
                    mIvReconnecting.setImageResource(mFrameDrawable_connected[mFrameNdx]);
                    mAnimationHandler.postDelayed(this, (long)(mFrameDeltaT_connected[mFrameNdx]));
                }

                ++mAnimationCycleCount;
////////////////Log.i(TAG, "animationRunner() - Count: " + mAnimationCycleCount);
                //
                //
                //
                determine_if_reconnected();
            }
        };

        mAnimationHandler.postDelayed(mAnimationRunnable, 100);
        mAnimationCycleCount = (0);
        mAnimationIsRunning = true;
        Log.i(TAG, "animationRunner() - Starting animation");
    }



    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");

        mAnimationIsRunning = false;
        Log.i(TAG, "onPause() - UN-Flagging animation");
    }






    private void updateUI() {
//        if (null != mTvStatus) {
//            mTvStatus.setText("Router: " + mFAcContainer.mServiceConnector.getData().mRouter_ssid
//                    + "\nConnect-to:  " + mFAcContainer.mServiceConnector.getData().mCIU_ssid
//                    + "\nMAC-Address: " + mFAcContainer.mServiceConnector.getData().mCIU_mac
//                    + "  (" + mFAcContainer.mServiceConnector.getData().mCIU_rssi
//                    + " dBm)");
//        }
    }


    //////// /////////////////////////////////////////////////////////////////////////////////////
    //////// /////////////////////////////////////////////////////////////////////////////////////


}

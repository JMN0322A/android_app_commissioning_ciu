package com.stanleyblackanddecker.fisx;




import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.Log;

public class CommandDispatcher {

    private final static String TAG = "{CommandDispatcher}";

    private Activity mAc;


    public CommandDispatcher(Activity ac) {
        mAc = ac;
    }

    public void switchto(int nId ) {

        Fragment fragmNew = null;

        Log.d(TAG,"switchto()- " + nId );
        switch( nId ) {
            case R.id.id_app_command__Start:
                fragmNew = new FragmFlowStart();
                Log.d(TAG, "switchto()- new fragment start");
                break;

            case R.id.id_app_command__About:
                fragmNew = new FragmAbout();
                Log.d(TAG,"switchto()- new fragment about" );
                break;
            case R.id.id_app_command__PressButton:
                fragmNew = new FragmPressButton();
                Log.d(TAG,"switchto()- new fragment press-button" );
                break;
            case R.id.id_app_command__Search:
                fragmNew = new FragmSearch();
                Log.d(TAG,"switchto()- new fragment search" );
                break;
            case R.id.id_app_command__Connect:
                fragmNew = new FragmConnect();
                Log.d(TAG,"switchto()- new fragment connect" );
                break;
            case R.id.id_app_command__Reconnect:
                fragmNew = new FragmReconnect();
                Log.d(TAG,"switchto()- new fragment reconnect" );
                break;
            case R.id.id_app_command__ReconnectWait:
                fragmNew = new FragmReconnectWait();
                Log.d(TAG,"switchto()- new fragment reconnect wait" );
                break;
            case R.id.id_app_command__CommsQuery:
                fragmNew = new FragmCommsQuery();
                Log.d(TAG,"switchto()- new fragment comms-query" );
                break;
            case R.id.id_app_command__CommsUpdate:
                fragmNew = new FragmCommsUpdate();
                Log.d(TAG,"switchto()- new fragment comms-update" );
                break;
            case R.id.id_app_command__Status:
                fragmNew = new FragmShowData();
                Log.d(TAG,"switchto()- new fragment status" );
                break;
            case R.id.id_app_command__Edit:
                fragmNew = new FragmEdit();
                Log.d(TAG,"switchto()- new fragment edit" );
                break;
            case R.id.id_app_command__Logger:
                fragmNew = new FragmLogger();
                Log.d(TAG,"switchto()- new fragment logger" );
                break;
        }



        if (null != fragmNew) {
            Log.d(TAG,"switchto()- new fragment XXX");

            FragmentManager fm = mAc.getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace( R.id.llContainer_content, fragmNew);
            ft.commit();
        }

    }


}

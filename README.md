# README #

Android APP, supporting SAT connected door. APP automates complicated process of commissioning a CIU to access its local area Wifi network. (created by Digital Accelerator)





### Contents ###

* Android APP source, for Android Studio
* Pseudo-CIU Source, for Atmel development boards


### Author ###

* John Novosel
* John.Novosel@sbdinc.com
* Digital Accelerator
* 10 10th Street
* Suite 400
* Atlanta, GA 30309
